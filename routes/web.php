<?php

use App\Notifications\Notify;
use Carbon\Carbon;
/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('home');
// });

Auth::routes();

Route::get('fire', function(){
    App\User::find(1)->notify(new Notify());
});

Route::get('/', 'HomeController@index')->name('home')->middleware('ceklogin');

Route::get('pinjam_ruangan/create', 'PinjamRuanganController@create')->name('pinjam_ruangan.create')->middleware('ceklogin');

Route::post('pinjam_ruangan/store', 'PinjamRuanganController@store')->name('pinjam_ruangan.store')->middleware('ceklogin');

Route::get('pinjam_ruangan/show/{order_id}', 'PinjamRuanganController@show')->name('pinjam_ruangan.show')->middleware('ceklogin');

Route::get('pinjam_ruangan/kelola/{order_id}', 'PinjamRuanganController@kelola')->name('pinjam_ruangan.kelola')->middleware('ceklogin', 'role:Administrator|Pengelola Urdal');

Route::get('pinjam_ruangan/download_nodin/{order_id}', 'PinjamRuanganController@download_nodin')->name('pinjam_ruangan.download_nodin')->middleware('ceklogin');

// Route::post('pinjam_ruangan/upload_nodin/{order_id}', 'PinjamRuanganController@upload_nodin')->name('pinjam_ruangan.upload_nodin')->middleware('ceklogin');

Route::get('pinjam_ruangan/edit/{order_id}', 'PinjamRuanganController@edit')->name('pinjam_ruangan.edit')->middleware('ceklogin');

Route::get('pinjam_ruangan/download_lampiran/{order_id}', 'PinjamRuanganController@download_lampiran')->name('pinjam_ruangan.download_lampiran')->middleware('ceklogin');

Route::put('pinjam_ruangan/update/{order_id}','PinjamRuanganController@update')->name('pinjam_ruangan.update')->middleware('ceklogin');

Route::delete('pinjam_ruangan/delete/{order_id}','PinjamRuanganController@destroy')->name('pinjam_ruangan.delete')->middleware('ceklogin');

Route::get('ruangan/data_ruangan', 'RuanganController@index')->name('ruangan.index')->middleware('ceklogin');

Route::post('ruangan/data_ruangan', 'RuanganController@filter')->name('ruangan.filter')->middleware('ceklogin');

Route::get('ruangan/kelola', 'RuanganController@kelola')->name('ruangan.kelola')->middleware('ceklogin', 'role:Administrator|Pengelola Urdal');

Route::get('ruangan/create', 'RuanganController@create')->name('ruangan.create')->middleware('ceklogin', 'role:Administrator|Pengelola Urdal');

Route::post('ruangan/store', 'RuanganController@store')->name('ruangan.store')->middleware('ceklogin', 'role:Administrator|Pengelola Urdal');

Route::get('ruangan/edit/{id}', 'RuanganController@edit')->name('ruangan.edit')->middleware('ceklogin', 'role:Administrator|Pengelola Urdal');

Route::put('ruangan/update/{id}', 'RuanganController@update')->name('ruangan.update')->middleware('ceklogin', 'role:Administrator|Pengelola Urdal');

Route::delete('ruangan/delete/{id}', 'RuanganController@destroy')->name('ruangan.delete')->middleware('ceklogin', 'role:Administrator|Pengelola Urdal');

Route::get('pinjam_kendaraan/create', 'PinjamKendaraanController@create')->name('pinjam_kendaraan.create')->middleware('ceklogin');

Route::post('pinjam_kendaraan/store', 'PinjamKendaraanController@store')->name('pinjam_kendaraan.store')->middleware('ceklogin');

Route::get('pinjam_kendaraan/show/{order_id}', 'PinjamKendaraanController@show')->name('pinjam_kendaraan.show')->middleware('ceklogin');

Route::get('pinjam_kendaraan/kelola/{order_id}', 'PinjamKendaraanController@kelola')->name('pinjam_kendaraan.kelola')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan');

// Route::post('pinjam_kendaraan/upload_nodin/{order_id}', 'PinjamKendaraanController@upload_nodin')->name('pinjam_kendaraan.upload_nodin')->middleware('ceklogin');

Route::get('pinjam_kendaraan/download_nodin/{order_id}', 'PinjamKendaraanController@download_nodin')->name('pinjam_kendaraan.download_nodin')->middleware('ceklogin');

Route::get('pinjam_kendaraan/edit/{order_id}', 'PinjamKendaraanController@edit')->name('pinjam_kendaraan.edit')->middleware('ceklogin');

Route::put('pinjam_kendaraan/update/{order_id}','PinjamKendaraanController@update')->name('pinjam_kendaraan.update')->middleware('ceklogin');

Route::delete('pinjam_kendaraan/delete/{order_id}','PinjamKendaraanController@destroy')->name('pinjam_kendaraan.delete')->middleware('ceklogin');

Route::get('kendaraan/data_kendaraan', 'KendaraanController@index')->name('kendaraan.index')->middleware('ceklogin');

Route::get('kendaraan/kelola', 'KendaraanController@kelola')->name('kendaraan.kelola')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan');

Route::get('kendaraan/create', 'KendaraanController@create')->name('kendaraan.create')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan');

Route::post('kendaraan/store', 'KendaraanController@store')->name('kendaraan.store')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan');

Route::get('kendaraan/edit/{id}', 'KendaraanController@edit')->name('kendaraan.edit')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan');

Route::put('kendaraan/update/{id}', 'KendaraanController@update')->name('kendaraan.update')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan');

Route::delete('kendaraan/delete/{id}', 'KendaraanController@destroy')->name('kendaraan.delete')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan');

Route::get('piket_pengemudi', 'PiketPengemudiController@index')->name('piket_pengemudi.index')->middleware('ceklogin');

Route::post('piket_pengemudi/filter', 'PiketPengemudiController@filter')->name('piket_pengemudi.filter')->middleware('ceklogin');

Route::post('piket_pengemudi/store', 'PiketPengemudiController@store')->name('piket_pengemudi.store')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan');

Route::get('perbaikan_barang/create', 'PerbaikanBarangController@create')->name('perbaikan_barang.create')->middleware('ceklogin');

Route::post('perbaikan_barang/store', 'PerbaikanBarangController@store')->name('perbaikan_barang.store')->middleware('ceklogin');

Route::get('perbaikan_barang/show/{order_id}', 'PerbaikanBarangController@show')->name('perbaikan_barang.show')->middleware('ceklogin');

Route::get('perbaikan_barang/kelola/{order_id}', 'PerbaikanBarangController@kelola')->name('perbaikan_barang.kelola')->middleware('ceklogin', 'role:Administrator|Pengelola Urdal');

// Route::post('perbaikan_barang/upload_nodin/{order_id}', 'PerbaikanBarangController@upload_nodin')->name('perbaikan_barang.upload_nodin')->middleware('ceklogin');

Route::get('perbaikan_barang/download_nodin/{order_id}', 'PerbaikanBarangController@download_nodin')->name('perbaikan_barang.download_nodin')->middleware('ceklogin');

Route::get('perbaikan_barang/edit/{order_id}', 'PerbaikanBarangController@edit')->name('perbaikan_barang.edit')->middleware('ceklogin');

Route::put('perbaikan_barang/update/{order_id}', 'PerbaikanBarangController@update')->name('perbaikan_barang.update')->middleware('ceklogin');

Route::delete('perbaikan_barang/delete/{order_id}', 'PerbaikanBarangController@destroy')->name('perbaikan_barang.delete')->middleware('ceklogin');

Route::get('pinjam_barang/create', 'PinjamBarangController@create')->name('pinjam_barang.create')->middleware('ceklogin');

Route::post('pinjam_barang/store', 'PinjamBarangController@store')->name('pinjam_barang.store')->middleware('ceklogin');

Route::get('pinjam_barang/show/{order_id}', 'PinjamBarangController@show')->name('pinjam_barang.show')->middleware('ceklogin');

Route::get('pinjam_barang/kelola/{order_id}', 'PinjamBarangController@kelola')->name('pinjam_barang.kelola')->middleware('ceklogin', 'role:Administrator|Pengelola Urdal');

// Route::post('pinjam_barang/upload_nodin/{order_id}', 'PinjamBarangController@upload_nodin')->name('pinjam_barang.upload_nodin')->middleware('ceklogin');

Route::get('pinjam_barang/download_nodin/{order_id}', 'PinjamBarangController@download_nodin')->name('pinjam_barang.download_nodin')->middleware('ceklogin');

Route::get('pinjam_barang/edit/{order_id}', 'PinjamBarangController@edit')->name('pinjam_barang.edit')->middleware('ceklogin');

Route::put('pinjam_barang/update/{order_id}', 'PinjamBarangController@update')->name('pinjam_barang.update')->middleware('ceklogin');

Route::delete('pinjam_barang/delete/{order_id}', 'PinjamBarangController@destroy')->name('pinjam_barang.delete')->middleware('ceklogin');

Route::get('penggandaan/create', 'PenggandaanController@create')->name('penggandaan.create')->middleware('ceklogin');

Route::post('penggandaan/store', 'PenggandaanController@store')->name('penggandaan.store')->middleware('ceklogin');

Route::get('penggandaan/show/{order_id}', 'PenggandaanController@show')->name('penggandaan.show')->middleware('ceklogin');

Route::get('penggandaan/kelola/{order_id}', 'PenggandaanController@kelola')->name('penggandaan.kelola')->middleware('ceklogin', 'role:Administrator|Pengelola Penggandaan');

Route::get('penggandaan/edit/{order_id}', 'PenggandaanController@edit')->name('penggandaan.edit')->middleware('ceklogin');

Route::put('penggandaan/update/{order_id}', 'PenggandaanController@update')->name('penggandaan.update')->middleware('ceklogin');

Route::delete('penggandaan/delete/{order_id}', 'PenggandaanController@destroy')->name('penggandaan.delete')->middleware('ceklogin');

Route::get('bensin/create', 'BensinController@create')->name('bensin.create')->middleware('ceklogin');

Route::post('bensin/store', 'BensinController@store')->name('bensin.store')->middleware('ceklogin');

Route::get('bensin/show/{order_id}', 'BensinController@show')->name('bensin.show')->middleware('ceklogin');

Route::get('bensin/kelola/{order_id}', 'BensinController@kelola')->name('bensin.kelola')->middleware('ceklogin', 'role:Administrator|Pengelola Urdal');

Route::get('bensin/edit/{order_id}', 'BensinController@edit')->name('bensin.edit')->middleware('ceklogin');

Route::put('bensin/put/{order_id}', 'BensinController@update')->name('bensin.update')->middleware('ceklogin');

Route::delete('bensin/delete/{order_id}', 'BensinController@destroy')->name('bensin.delete')->middleware('ceklogin');

Route::post('bensin/upload_kuitansi/{order_id}', 'BensinController@upload_kuitansi')->name('bensin.upload_kuitansi')->middleware('ceklogin');

Route::get('bensin/download_kuitansi/{order_id}', 'BensinController@download_kuitansi')->name('bensin.download_kuitansi')->middleware('ceklogin');

Route::get('saranaduan/create', 'SaranAduanController@create')->name('saranaduan.create')->middleware('ceklogin');

Route::post('saranaduan/store', 'SaranAduanController@store')->name('saranaduan.store')->middleware('ceklogin');

Route::get('saranaduan/show/{order_id}', 'SaranAduanController@show')->name('saranaduan.show')->middleware('ceklogin');

Route::get('saranaduan/kelola/{order_id}', 'SaranAduanController@kelola')->name('saranaduan.kelola')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::post('saranaduan/upload_lampiran/{order_id}', 'SaranAduanController@upload_lampiran')->name('saranaduan.upload_lampiran')->middleware('ceklogin');

Route::get('saranaduan/download_lampiran/{order_id}', 'SaranAduanController@download_lampiran')->name('saranaduan.download_lampiran')->middleware('ceklogin');

Route::get('saranaduan/edit/{order_id}', 'SaranAduanController@edit')->name('saranaduan.edit')->middleware('ceklogin');

Route::put('saranaduan/update/{order_id}', 'SaranAduanController@update')->name('saranaduan.update')->middleware('ceklogin');

Route::delete('saranaduan/delete/{order_id}', 'SaranAduanController@destroy')->name('saranaduan.delete')->middleware('ceklogin');

Route::get('dinas/create', 'DinasController@create')->name('dinas.create')->middleware('ceklogin');

Route::post('dinas/store', 'DinasController@store')->name('dinas.store')->middleware('ceklogin');

Route::get('dinas/show/{order_id}', 'DinasController@show')->name('dinas.show')->middleware('ceklogin');

Route::get('dinas/kelola/{order_id}', 'DinasController@kelola')->name('dinas.kelola')->middleware('ceklogin');

Route::get('dinas/download_nodin/{order_id}', 'DinasController@download_nodin')->name('dinas.download_nodin')->middleware('ceklogin');

Route::get('dinas/edit/{order_id}', 'DinasController@edit')->name('dinas.edit')->middleware('ceklogin');

Route::put('dinas/update/{order_id}', 'DinasController@update')->name('dinas.update')->middleware('ceklogin');

Route::delete('dinas/delete/{order_id}', 'DinasController@delete')->name('dinas.delete')->middleware('ceklogin');

Route::get('permohonan', 'PermohonanController@index')->name('permohonan.index')->middleware('ceklogin');

Route::get('kelola_permohonan', 'PermohonanController@kelola')->name('permohonan.kelola')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::put('permohonan/setuju/{order_id}', 'PermohonanController@setuju')->name('permohonan.setuju')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::get('permohonan/tolak/{order_id}', 'PermohonanController@tolak')->name('permohonan.tolak')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::put('permohonan/tolak_store/{order_id}', 'PermohonanController@tolak_store')->name('permohonan.tolak_store')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::get('pengumuman/edit', 'PengumumanController@edit')->name('pengumuman.edit')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::put('pengumuman/update', 'PengumumanController@update')->name('pengumuman.update')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::get('runtext.index', 'RuntextController@index')->name('runtext.index')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::get('runtext.create', 'RuntextController@create')->name('runtext.create')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::post('runtext.store', 'RuntextController@store')->name('runtext.store')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::get('runtext/edit/{id}', 'RuntextController@edit')->name('runtext.edit')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::put('runtext/update/{id}', 'RuntextController@update')->name('runtext.update')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::delete('runtext/delete/{id}', 'RuntextController@destroy')->name('runtext.delete')->middleware('ceklogin', 'role:Administrator|Pengelola Kendaraan|Pengelola Urdal|Pengelola Penggandaan');

Route::get('user/index', 'UserController@index')->name('user.index')->middleware('ceklogin', 'role:Administrator');

Route::get('user/edit/{id}', 'UserController@edit')->name('user.edit')->middleware('ceklogin', 'role:Administrator');

Route::put('user/update/{id}', 'UserController@update')->name('user.update')->middleware('ceklogin', 'role:Administrator');

Route::get('rekap', 'RekapController@index')->name('rekap.index')->middleware('ceklogin');

Route::post('rekap/filter', 'RekapController@filter')->name('rekap.filter')->middleware('ceklogin');

Route::get('rekap/download/{tahun}', 'RekapController@download')->name('rekap.download')->middleware('ceklogin');

Route::get('rekap/pdf', function(){ $tanggal = Carbon::now(); return view('rekap/pdf', ['tanggal' => $tanggal, 'tahun' => $tanggal->year, 'total_permohonan' => 200, 'total_bensin' => 200, 'total_pengguna' => 5]); });

Route::get('mail', function() { return view('email'); });

Route::get('fire', function() { event(new \App\Events\SocketEvent('masuk', 'ruangan', '123456', '2')); });