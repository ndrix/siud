<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PiketPengemudi extends Model
{
    protected $table = 'piket_pengemudis';

    protected $fillable = [
        'nama', 'tanggal', 'kontak'
    ];
}
