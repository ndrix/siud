<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PinjamKendaraan extends Model
{
    protected $table = 'pinjam_kendaraans';

    protected $fillable = [
        'order_id', 'user_id', 'satker_id', 'kendaraan_id', 'kegiatan', 'tujuan', 'jml_penumpang', 'waktu_mulai', 'waktu_selesai', 'dukungan_pengemudi', 
        'nodin', 'status_id', 'alasan'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function kendaraan()
    {
        return $this->belongsTo('App\Kendaraan', 'kendaraan_id');
    }

    public function status()
    {
        return $this->belongsTo('App\StatusFlags', 'status_id');
    }

    public function satker()
    {
        return $this->belongsTo('App\Satker', 'satker_id');
    }
}
