<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Bensin extends Model
{
    protected $table = 'bensins';

    protected $fillable = [
        'order_id', 'user_id', 'satker_id', 'waktu', 'jabatan', 'total_penggunaan', 'kuitansi',
        'status_id', 'alasan'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\StatusFlags', 'status_id');
    }

    public function satker()
    {
        return $this->belongsTo('App\Satker', 'satker_id');
    }
}
