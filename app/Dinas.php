<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Dinas extends Model
{
    protected $table = 'dinas';

    protected $fillable = [
        'order_id', 'user_id', 'kegiatan', 'lokasi', 'peserta', 'ktp', 'nodin', 
        'status_id', 'alasan'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function status()
    {
        return $this->belongsTo('App\StatusFlags', 'status_id');
    }
}
