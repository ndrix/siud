<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Penggandaan extends Model
{
    protected $table = 'penggandaans';

    protected $fillable = [
        'order_id', 'user_id', 'satker_id', 'judul', 'kertas', 'hlm_warna', 'jml_warna', 'hlm_hitam_putih',
        'jml_hitam_putih', 'keterangan', 'status_id', 'alasan'
    ];

    public function status()
    {
        return $this->belongsTo('App\StatusFlags', 'status_id');
    }

    public function satker()
    {
        return $this->belongsTo('App\Satker', 'satker_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
