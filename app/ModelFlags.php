<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ModelFlags extends Model
{
    protected $table = 'model_flags';

    protected $fillable = [
        'nama',
    ];

    public function history()
    {
        return $this->hasMany('App\StatusHistories', 'models_id');
    }
}
