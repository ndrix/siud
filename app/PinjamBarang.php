<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PinjamBarang extends Model
{
    protected $table = 'pinjam_barangs';

    protected $fillable = [
        'order_id', 'user_id', 'satker_id', 'barang_id', 'jumlah', 'waktu_mulai', 'waktu_selesai',
        'kegiatan', 'keterangan', 'nodin', 'status_id', 'alasan'
    ];

    public function status()
    {
        return $this->belongsTo('App\StatusFlags', 'status_id');
    }

    public function barang()
    {
        return $this->belongsTo('App\Barang', 'barang_id');
    }

    public function satker()
    {
        return $this->belongsTo('App\Satker', 'satker_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
