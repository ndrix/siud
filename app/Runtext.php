<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Runtext extends Model
{
    protected $table = 'runtexts';
    
    protected $fillable = [
        'isian', 'tampil'
    ];
}
