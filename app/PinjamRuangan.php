<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PinjamRuangan extends Model
{
    protected $table = 'pinjam_ruangans';

    protected $fillable = [
        'order_id', 'user_id', 'satker_id', 'ruangan_id', 'waktu_mulai', 'waktu_selesai', 'kegiatan', 'jml_pengguna', 'nodin', 'layout', 'lampiran', 'status_id', 'alasan' 
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function ruangan()
    {
        return $this->belongsTo('App\Ruangan', 'ruangan_id');
    }

    public function satker()
    {
        return $this->belongsTo('App\Satker', 'satker_id');
    }

    public function status()
    {
        return $this->belongsTo('App\StatusFlags', 'status_id');
    }
}
