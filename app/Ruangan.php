<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Ruangan extends Model
{
    protected $table = 'ruangans';
    
    protected $fillable = [
        'nama', 'lokasi', 'fasilitas', 'kapasitas', 'keterangan', 'image'
    ];

    public function pinjamRuangan()
    {
        return $this->hasMany('App\PinjamRuangan', 'ruangan_id');
    }
}
