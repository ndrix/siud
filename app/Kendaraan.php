<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Kendaraan extends Model
{
    protected $table = 'kendaraans';

    protected $fillable = [
        'nama', 'nomor_polisi', 'kapasitas', 
    ];

    public function pinjamKendaraan()
    {
        return $this->hasMany('App\PinjamKendaraan', 'kendaraan_id');
    }
}
