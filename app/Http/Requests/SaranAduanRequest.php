<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class SaranAduanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'isian'             => 'required',
            'jenis'             => 'required',
            'lampiran'          => 'mimes:jpg,jpeg,png,pdf|max:4086',
        ];
    }

    public function messages()
    {
        return [
            'required'                      => 'Input :attribute tidak sesuai.',
            'integer'                       => 'Input :attribute tidak sesuai.'
        ];
    }
}
