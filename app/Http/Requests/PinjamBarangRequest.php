<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class PinjamBarangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $now        = Carbon::now();
        return [
            'barang.*.barang_id'    => 'required|integer|distinct',
            'barang.*.jumlah'       => 'required|integer',
            'waktu_mulai'           => 'required|date_format:Y-m-d H:i:s|after:'.$now,
            'waktu_selesai'         => 'required|date_format:Y-m-d H:i:s|after:waktu_mulai',
            'satker_id'             => 'required|integer',
            'kegiatan'              => 'required',
            'nodin'                 => 'mimes:jpg,jpeg,png,pdf|max:2048'
        ];
    }

    protected function prepareForValidation()
    {
        if($this->has('waktu_mulai')){
            $this->merge(['waktu_mulai'=>date('Y-m-d H:i:s', strtotime(explode(" - ", $this->input('waktu_mulai'))[0].' '.explode(" - ", $this->input('waktu_mulai'))[1]))]);
        }
        if($this->has('waktu_selesai')){
            $this->merge(['waktu_selesai'=>date('Y-m-d H:i:s', strtotime(explode(" - ", $this->input('waktu_selesai'))[0].' '.explode(" - ", $this->input('waktu_selesai'))[1]))]);
        }
    }

    public function messages()
    {
        return [
            'barang.*.jumlah.required'      => 'Input jumlah tidak sesuai.',
            'barang.*.barang_id.required'   => 'Input barang tidak sesuai.',
            'barang.*.barang_id.distinct'   => 'Input barang terdapat duplikasi.',
            'required'                      => 'Input :attribute tidak sesuai.',
            'integer'                       => 'Input :attribute tidak sesuai.'
        ];
    }
}
