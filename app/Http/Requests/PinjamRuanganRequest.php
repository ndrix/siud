<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;

class PinjamRuanganRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $now        = Carbon::now();
        $limit_date = Carbon::parse(Request::get('waktu_mulai'))->addDays(7);
        return [
            'ruangan_id'    => 'required|integer',
            'waktu_mulai'   => 'required|date_format:Y-m-d H:i:s|after:'.$now,
            'waktu_selesai' => 'required|date_format:Y-m-d H:i:s|after:waktu_mulai|before:'.$limit_date,
            'kegiatan'      => 'required|string',
            'jml_pengguna'  => 'required|integer',
            'layout'        => 'required',
            'lampiran'      => 'mimes:jpeg,png,jpg,pdf|max:2048',
            'nodin'         => 'mimes:jpeg,jpg,png,pdf|max:2048'
        ];
    }

    protected function prepareForValidation()
    {
        if($this->has('waktu_mulai')){
            $this->merge(['waktu_mulai'=>date('Y-m-d H:i:s', strtotime(explode(" - ", $this->input('waktu_mulai'))[0].' '.explode(" - ", $this->input('waktu_mulai'))[1]))]);
            $this->merge(['waktu_selesai'=>date('Y-m-d H:i:s', strtotime(explode(" - ", $this->input('waktu_selesai'))[0].' '.explode(" - ", $this->input('waktu_selesai'))[1]))]);
        }
        if($this->has('ketersediaan')){
            $this->merge(['ruangan_id'=>'99', 'kegiatan'=>'abcd', 'jml_pengguna'=>'99', 'layout'=>'letter u']);
        }
    }

    public function messages()
    {
        return [
            'required'      => 'Input :attribute tidak sesuai.',
            'date_format'   => 'Format waktu salah.',
            'after'         => 'Input :attribute tidak sesuai.',
            'string'        => 'Input :attribute tidak sesuai.',
            'integer'       => 'Input :attribute tidak sesuai',
            'mimes'         => 'Format file tidak sesuai.',
            'max'           => 'File melebihi batas maksimal (2 MB).',
        ];
    }

    public function response(array $errors)
    {
        
        
        $transformed = [];
 
        foreach ($errors as $field => $message) {
            $transformed[] = [
                'field' => $field,
                'message' => $message[0]
            ];
        }
 
        return response()->json([
            'errors' => $transformed
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
