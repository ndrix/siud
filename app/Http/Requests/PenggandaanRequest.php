<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PenggandaanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'satker_id'                  => 'required|integer',
            'barang.*.judul'             => 'required',
            'barang.*.kertas'            => 'required',
            'barang.*.hlm_warna'         => 'required|integer',
            'barang.*.jml_warna'         => 'required|integer',
            'barang.*.hlm_hitam_putih'   => 'required|integer',
            'barang.*.jml_hitam_putih'   => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'required'                      => 'Input :attribute tidak sesuai.',
            'integer'                       => 'Input :attribute tidak sesuai.'
        ];
    }
}
