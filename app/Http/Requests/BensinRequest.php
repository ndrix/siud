<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Carbon\Carbon;

class BensinRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $current = Carbon::now();
        $current = new Carbon;
        return [
            'waktu'             => 'required|date_format:Y-m-d|before:'.$current,
            'jabatan'           => 'required',
            'total_penggunaan'  => 'required|integer',
            'kuitansi'          => 'mimes:jpeg,png,jpg,pdf|max:2048',
        ];
    }
}
