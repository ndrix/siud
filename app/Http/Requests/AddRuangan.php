<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class AddRuangan extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    public function wantsJson()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'nama'      => 'required',
            'lokasi'    => 'required',
            'fasilitas' => 'required',
            'kapasitas' => 'required|integer',
        ];
    }

    public function messages()
    {
        return [
            'required'      => 'Variabel :attribute tidak terisi',
        ];
    }

    public function response(array $errors)
    {
        
        
        $transformed = [];
 
        foreach ($errors as $field => $message) {
            $transformed[] = [
                'field' => $field,
                'message' => $message[0]
            ];
        }
 
        return response()->json([
            'errors' => $transformed
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
