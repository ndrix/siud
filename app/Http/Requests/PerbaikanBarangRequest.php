<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class PerbaikanBarangRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'barang.*.barang_id'    => 'required|integer|distinct',
            'barang.*.jumlah'       => 'required|integer',
            'satker_id'             => 'required|integer',
            'lokasi'                => 'required',
            'nodin'                 => 'mimes:jpg,jpeg,png,pdf|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'barang.*.jumlah.required'      => 'Input jumlah tidak sesuai.',
            'barang.*.barang_id.required'   => 'Input barang tidak sesuai.',
            'barang.*.barang_id.distinct'   => 'Input barang terdapat duplikasi.',
            'required'                      => 'Input :attribute tidak sesuai.',
            'integer'                       => 'Input :attribute tidak sesuai.'
        ];
    }
}
