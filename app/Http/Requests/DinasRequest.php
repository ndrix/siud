<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class DinasRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'peserta.*.nama'        => 'required|distinct',
            'peserta.*.ktp'         => 'required|integer',
            'kegiatan'              => 'required',
            'lokasi'                => 'required',
            'nodin'                 => 'mimes:jpg,jpeg,png,pdf|max:2048'
        ];
    }

    public function messages()
    {
        return [
            'peserta.*.nama.required'       => 'Input nama tidak sesuai.',
            'peserta.*.ktp.required'        => 'Input ktp tidak sesuai.',
            'peserta.*.nama.distinct'       => 'Input nama terdapat duplikasi.',
            'required'                      => 'Input :attribute tidak sesuai.',
            'integer'                       => 'Input :attribute tidak sesuai.'
        ];
    }
}
