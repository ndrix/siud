<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;
use Symfony\Component\HttpFoundation\Request;
use Carbon\Carbon;

class PinjamKendaraanRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $current    = Carbon::now();
        $limit_date = Carbon::parse(Request::get('waktu_mulai'))->toDateString();
        $limit      = $limit_date." 23:59:59";
        return [
            'kendaraan_id'      => 'required|integer',
            'kegiatan'          => 'required|string',
            'waktu_mulai'       => 'required|date_format:Y-m-d H:i:s|after:'.$current,
            'waktu_selesai'     => 'required|date_format:Y-m-d H:i:s|after:waktu_mulai|before:'.$limit,        
            'tujuan'            => 'required|string',
            'jml_penumpang'     => 'required|integer',
            'dukungan_pengemudi'=> 'in:ya,tidak',
            'nodin'             => 'mimes:jpg,jpeg,png,pdf|max:2048'
        ];
    }

    protected function prepareForValidation()
    {
        if($this->has('waktu_mulai')){
            $this->merge(['waktu_mulai' => date('Y-m-d H:i:s', strtotime(explode(" - ", $this->input('waktu_mulai'))[0].' '.explode(" - ", $this->input('waktu_mulai'))[1]))]);
            $this->merge(['waktu_selesai' => date('Y-m-d H:i:s', strtotime(explode(" - ", $this->input('waktu_selesai'))[0].' '.explode(" - ", $this->input('waktu_selesai'))[1]))]);
        }
        if($this->has('ketersediaan')){
            $this->merge(['kendaraan_id' => '999', 'kegiatan' => 'abcd', 'tujuan' => 'abcd', 'jml_penumpang' => '99', 'dukungan_supir' => 'on']);
        }
    }

    public function messages()
    {
        return [
            'required'      => 'Input :attribute tidak sesuai.',
            'date_format'   => 'Format waktu salah.',
            'after'         => 'Input :attribute tidak sesuai.',
            'string'        => 'Input :attribute tidak sesuai.',
            'integer'       => 'Input :attribute tidak sesuai',
            'in'            => 'Input :attribute tidak sesuai',
        ];
    }

    public function response(array $errors)
    {
        
        
        $transformed = [];
 
        foreach ($errors as $field => $message) {
            $transformed[] = [
                'field' => $field,
                'message' => $message[0]
            ];
        }
 
        return response()->json([
            'errors' => $transformed
        ], JsonResponse::HTTP_UNPROCESSABLE_ENTITY);
    }
}
