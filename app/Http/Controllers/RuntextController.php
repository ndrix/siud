<?php

namespace App\Http\Controllers;

use App\Runtext;
use Illuminate\Http\Request;
use App\Http\Requests\RuntextRequest;
use Illuminate\Support\Facades\Crypt;

class RuntextController extends Controller
{
    public function index()
    {
        $runtext                    = Runtext::all();

        return view('runtext.index', ['runtext' => $runtext]);
    }

    public function create()
    {
        $runtext                    = Runtext::all();

        return view('runtext.create', ['runtext' => $runtext]);
    }

    public function store(RuntextRequest $request)
    {
        $runtext_all                = Runtext::all();
        $runtext['isian']           = $request->get('isian');
        Runtext::create($runtext);

        return redirect()->route('runtext.index', ['runtext' => $runtext_all])->with('success', 'Berhasil menambah teks berjalan');
    }

    public function edit($id)
    {
        $decrypt_id                 = Crypt::decryptString($id);
        $runtext                    = Runtext::where('id', $decrypt_id)->first();
        $runtext_all                = Runtext::all();

        return view('runtext.edit', ['runtext' => $runtext, 'runtext_all' => $runtext_all]);
    }

    public function update($id, RuntextRequest $request)
    {
        $runtext_all                = Runtext::all();
        $decrypt_id                 = Crypt::decryptString($id);
        $runtext                    = Runtext::where('id', $decrypt_id)->first();
        $runtext->isian             = $request->get('isian');
        $runtext->tampil            = $request->get('tampil');
        $runtext->save();

        return redirect()->route('runtext.index', ['runtext' => $runtext_all]);
    }

    public function destroy($id)
    {
        $decrypt_id                 = Crypt::decryptString($id);
        $runtext                    = Runtext::where('id', $decrypt_id)->first();
        $runtext->delete();

        return redirect()->back()->with('success', 'Berhasil menghapus teks berjalan.');
    }
}
