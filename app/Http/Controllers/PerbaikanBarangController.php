<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Barang;
use App\Satker;
use App\PerbaikanBarang;
use App\Runtext;
use App\User;
use App\Http\Requests\PerbaikanBarangRequest;
use App\Http\Requests\UploadNodinRequest;
use App\Notifications\Notify;

class PerbaikanBarangController extends Controller
{
    public function create()
    {
        $barang                 = Barang::where('kategori', 'perbaikan')->orWhere('kategori', 'perbaikan & peminjaman')->get();
        $satker                 = Satker::all();
        $runtext                = Runtext::all();

        return view('perbaikan_barang.create', ['barang' => $barang, 'satker' => $satker, 'runtext' => $runtext]);
    }

    public function store(PerbaikanBarangRequest $request)
    {
        $bulan_romawi                   = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
        $angka_bulan                    = date('n', strtotime(Carbon::now()));
        $tahun                          = date('Y', strtotime(Carbon::now()));
        $order_id_sebelumnya            = PerbaikanBarang::all()->sortByDesc('id')->first()['order_id'];
        // generate order_id
        if($order_id_sebelumnya != null){
            $substr                     = substr($order_id_sebelumnya, strpos($order_id_sebelumnya,"SIUD.3/")+strlen("SIUD.3/"),strlen($order_id_sebelumnya));
            $urutan_sebelumnya          = substr($substr,0,strpos($substr,"/"));
            $urutan                     = $urutan_sebelumnya+1;
            $order_id                   = "SIUD.3/".$urutan."/".$bulan_romawi[$angka_bulan]."/".$tahun; 
        } else{
            $order_id                   = "SIUD.3/1/".$bulan_romawi[$angka_bulan]."/".$tahun;
        }
        // cek duplikasi
        foreach($request->get('barang') as $n)
        {
            $cek_duplikasi              = PerbaikanBarang::
                                        where('barang_id', $n['barang_id'])
                                        ->where('lokasi', 'like', '%'.$request->get('lokasi').'%')
                                        ->where('user_id', Auth::user()->id)
                                        ->whereMonth('created_at', Carbon::now()->month)
                                        ->whereYear('created_at', Carbon::now()->year)
                                        ->first();
            //jika ada duplikasi
            if($cek_duplikasi != null){
                return redirect()->back()->with('error', 'Anda telah mengajukan permohonan untuk barang yang sama di lokasi tersebut.');
            }
        }
        //validate nodin
        Validator::make($request->all(), [
            'nodin' => 'required',
        ], [
            'required' => 'Kolom :attribute harus diisi.',
        ])->validate();
        //file nodin
        $replace_slash_order_id     = str_replace("/", "-", $order_id);
        $order_id_dash              = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
        $get_nodin                  = $request->nodin;
        $ext                        = $get_nodin->getClientOriginalExtension();
        $rename_nodin               = "NODIN_".$order_id_dash.".".$ext;
        //jika tidak ada duplikasi
        foreach($request->get('barang') as $n){
            $data['order_id']           = $order_id;
            $data['user_id']            = Auth::user()->id;
            $data['barang_id']          = $n['barang_id'];
            $data['satker_id']          = $request->get('satker_id');
            $data['jumlah']             = $n['jumlah'];
            $data['lokasi']             = $request->get('lokasi');
            $data['keterangan']         = $request->get('keterangan');
            $data['status_id']          = "2";
            $data['nodin']              = $rename_nodin;
            PerbaikanBarang::create($data);
        }
        //simpan file nodin
        Storage::disk('nodin')->put($rename_nodin, File::get($get_nodin));
        //notify
        User::find(2)->notify(new Notify($order_id_dash, 'perbaikan', ''));
        //send email
        $perbaikan_barang           = PerbaikanBarang::where('order_id', $order_id)->get();
        Mail::send('email', ['permohonan' => $perbaikan_barang], function ($message) use($perbaikan_barang)
        {
            $message->subject(strtoupper($perbaikan_barang->first()->status->status));
            $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
            $message->to($perbaikan_barang->first()->user->email);
        });

        return back()->with('success', 'Permohonan Anda akan diproses, sebelumnya lengkapi dulu Nota Dinas Permohonan di halaman Permohonan Saya.');
    }

    public function show($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $perbaikan_barang               = PerbaikanBarang::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('perbaikan_barang.show', ['perbaikan_barang' => $perbaikan_barang, 'kelola' => 'no', 'runtext' => $runtext]);
    }

    public function kelola($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $perbaikan_barang               = PerbaikanBarang::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('perbaikan_barang.show', ['perbaikan_barang' => $perbaikan_barang, 'kelola' => 'yes', 'runtext' => $runtext]);
    }

    public function upload_nodin($order_id, UploadNodinRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $status_id                      = PerbaikanBarang::where('order_id', $order_id)->first()['status_id'];
        //validasi apakah status masih belum diverifikasi
        if($status_id != 3 OR $status_id != 4){
            //operasi file
            $get_file                   = $request->nodin;
            $ext                        = $get_file->getClientOriginalExtension();
            $rename_file                = "Nodin_".$decrypt_order_id.".".$ext;
            //simpan update
            $perbaikan_barang           = PerbaikanBarang::where('order_id', $order_id)->get();
            foreach($perbaikan_barang as $p){
                $p->nodin               = $rename_file;
                $p->status_id           = "2";
                $p->save();
            }
            Storage::disk('nodin')->delete($rename_file);
            Storage::disk('nodin')->put($rename_file, File::get($get_file));
            //send email
            Mail::send('email', ['permohonan' => $perbaikan_barang], function ($message) use($perbaikan_barang)
            {
                $message->subject(strtoupper($perbaikan_barang->first()->status->status));
                $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                $message->to($perbaikan_barang->first()->user->email);
            });

            return redirect()->back()->with('success', 'Berhasil mengunggah file nota dinas, silakan menunggu Admin untuk memverifikasi.');
        }
        //jika sudah diverifikasi
        else {
            return redirect()->back()->with('error', 'Tidak dapat mengunggah file nota dinas.');
        }
    }

    public function download_nodin($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $nodin                          = PerbaikanBarang::where('order_id', $order_id)->first()['nodin'];

        return Storage::disk('nodin')->download($nodin);
    }

    public function edit($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $perbaikan_barang               = PerbaikanBarang::where('order_id', $order_id)->get();
        $barang                         = Barang::all();
        $satker                         = Satker::all();
        $runtext                        = Runtext::all();

        return view('perbaikan_barang.edit', ['perbaikan_barang' => $perbaikan_barang, 'barang' => $barang, 'satker' => $satker, 'runtext' => $runtext]);
    }

    public function update($order_id, PerbaikanBarangRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $perbaikan_barang               = PerbaikanBarang::where('order_id', $order_id)->first();
        $delete_dot_order_id            = str_replace(".", "", $order_id); 
        $replace_slash_order_id         = str_replace("/", "-", $delete_dot_order_id); 
        $encrypt_order_id               = Crypt::encryptString($replace_slash_order_id);
        // validasi status masih belum diverifikasi admin
        if($perbaikan_barang->status_id != 3 OR $perbaikan_barang->status_id != 4){
            // jika statusnya belum diverifikasi admin maka permohonan bisa diubah
            if(PerbaikanBarang::where('order_id', $order_id)->first()->nodin != null){
                //delete file nodin
                Storage::disk('nodin')->delete(PerbaikanBarang::where('order_id', $order_id)->first()['nodin']);
            }
            $delete_entry               = PerbaikanBarang::where('order_id', $order_id)->delete();
            //file nodin baru
            if($request->nodin != null){
                $replace_slash_order_id     = str_replace("/", "-", $order_id);
                $order_id_dash              = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
                $get_nodin                  = $request->nodin;
                $ext                        = $get_nodin->getClientOriginalExtension();
                $rename_nodin               = "NODIN_".$order_id_dash.".".$ext;
            }
            foreach($request->get('barang') as $n){
                $data['order_id']       = $order_id;
                $data['user_id']        = Auth::user()->id;
                $data['barang_id']      = $n['barang_id'];
                $data['satker_id']      = $request->get('satker_id');
                $data['jumlah']         = $n['jumlah'];
                $data['lokasi']         = $request->get('lokasi');
                $data['keterangan']     = $request->get('keterangan');
                $data['nodin']          = $perbaikan_barang->nodin;
                $data['status_id']      = $perbaikan_barang->status_id;
                if($request->nodin != null){
                    $data['nodin']      = $rename_nodin;
                }
                PerbaikanBarang::create($data);
            }
            //simpan file nodin
            if($request->nodin != null){
                Storage::disk('nodin')->put($rename_nodin, File::get($get_nodin));
            }

            return redirect()->route('perbaikan_barang.show', ['order_id' => $encrypt_order_id])->with('success', 'Permohonan berhasil diubah.');
        } 
        // jika sudah diverifikasi admin maka tidak bisa diubah
        else { 
            return redirect()->route('perbaikan_barang.show', ['order_id' => $encrypt_order_id])->with('error', 'Permohonan tidak dapat diubah.');
        }
    }

    public function destroy($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $perbaikan_barang               = PerbaikanBarang::where('order_id', $order_id)->get();
        // validasi status masih belum diverifikasi admin
        if($perbaikan_barang->first()['status_id'] != 3 OR $perbaikan_barang->first()['status_id'] != 4){
            // jika statusnya belum diverifikasi admin maka permohonan bisa dihapus
            Storage::disk('nodin')->delete($perbaikan_barang->first()['nodin']);
            foreach($perbaikan_barang as $p){
                $p->delete();
            }
            return redirect()->back()->with('success', 'Berhasil menghapus permohonan.');
        }
        else {
            // jika ruangan tidak tersedia maka permohonan tidak bisa dihapus
            return redirect()->back()->with('error', 'Tidak dapat menghapus permohonan.');
        }
    }
}
