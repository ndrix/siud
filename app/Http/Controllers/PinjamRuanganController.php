<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\PinjamRuangan;
use App\Ruangan;
use App\Satker;
use App\StatusHistories;
use App\Runtext;
use App\User;
use App\Http\Requests\PinjamRuanganRequest;
use App\Http\Requests\UploadNodinRequest;
use App\Notifications\Notify;
use App\Events\SocketEvent;
use App\Transformers\PinjamRuangTransformer;

class PinjamRuanganController extends Controller
{
    public function create()
    {
        $satker                     = Satker::all();
        $ruangan                    = Ruangan::all();
        $runtext                    = Runtext::all();

        return view('pinjam_ruangan.create', ['ruangan' => $ruangan, 'satker' => $satker, 'tanggal' => '', 'cek' => '', 'runtext' => $runtext]);
    }

    public function store(PinjamRuanganRequest $request)
    {
        $bulan_romawi               = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
        $angka_bulan                = date('n', strtotime(Carbon::now()));
        $tahun                      = date('Y', strtotime(Carbon::now()));
        $waktu_mulai                = $request->get('waktu_mulai');
        $waktu_selesai              = $request->get('waktu_selesai');

        if($request->get('ketersediaan') != null){
            $satker                 = Satker::all();
            $ruangan                = Ruangan::all();
            $ruangan_tersedia       = array();
            foreach($ruangan as $n){
                $cek                = PinjamRuangan::
                                        where('ruangan_id',$request->get('ruangan_id'))
                                        ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                            $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                        })
                                        ->where('status_id',2)
                                        ->first();
                if($cek == null){
                    array_push($ruangan_tersedia, $n->nama);
                }
            }
            if($ruangan_tersedia != null){
                return back()->withInput()->with('ruangan_tersedia', $ruangan_tersedia);
            } else {
                return back()->withInput()->with('tidak_tersedia', 'Ruangan Tidak Tersedia');
            }
        } else {
            $order_id_sebelumnya            = PinjamRuangan::all()->sortByDesc('id')->first()['order_id'];
            if($order_id_sebelumnya != null){
                $substr                     = substr($order_id_sebelumnya, strpos($order_id_sebelumnya,"SIUD.1/")+strlen("SIUD.1/"),strlen($order_id_sebelumnya));
                $urutan_sebelumnya          = substr($substr,0,strpos($substr,"/"));
                $urutan                     = $urutan_sebelumnya+1;
                $order_id                   = "SIUD.1/".$urutan."/".$bulan_romawi[$angka_bulan]."/".$tahun; 
            } else{
                $order_id                   = "SIUD.1/1/".$bulan_romawi[$angka_bulan]."/".$tahun;
            }
            $data['user_id']            = Auth::user()->id;
            $data['order_id']           = $order_id;
            $data['ruangan_id']         = $request->get('ruangan_id');
            $data['waktu_mulai']        = $waktu_mulai;
            $data['waktu_selesai']      = $waktu_selesai;
            $data['kegiatan']           = $request->get('kegiatan');
            $data['satker_id']          = $request->get('satker_id');
            $data['jml_pengguna']       = $request->get('jml_pengguna');
            $data['layout']             = $request->get('layout');
            $data['status_id']          = "1";
            //validate nodin
            Validator::make($request->all(), [
                'nodin' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ])->validate();
            //file nodin
            $replace_slash_order_id     = str_replace("/", "-", $order_id);
            $order_id_dash              = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
            $get_nodin                  = $request->nodin;
            $ext                        = $get_nodin->getClientOriginalExtension();
            $rename_nodin               = "NODIN_".$order_id_dash.".".$ext;
            $data['nodin']              = $rename_nodin;
            //file lampiran
            if($request->lampiran != NULL){
                $get_lampiran           = $request->lampiran;
                $ext                    = $get_lampiran->getClientOriginalExtension();
                $rename_lampiran        = "LAMPIRAN_".$order_id_dash.".".$ext;
                $data['lampiran']       = $rename_lampiran;
            }
            //cek ketersediaan ruangan
            $cek_ketersediaan           = PinjamRuangan::
                                            where('ruangan_id',$request->get('ruangan_id'))
                                            ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                                $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                    ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                            })
                                            ->where('status_id', 3)
                                            ->first();
            if($cek_ketersediaan == null){
                //cek duplikasi permohonan
                $cek_duplikasi          = PinjamRuangan::
                                            where('ruangan_id',$request->get('ruangan_id'))
                                            ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                                $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                    ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                            })
                                            ->where('user_id', Auth::user()->id)
                                            ->first();
                if($cek_duplikasi == null){
                    //jika tidak duplikasi
                    PinjamRuangan::create($data);
                    Storage::disk('nodin')->put($rename_nodin, File::get($get_nodin));
                    if($request->lampiran != NULL){
                        Storage::disk('lampiran')->put($rename_lampiran, File::get($get_lampiran));
                    }
                    //notify
                    User::find(2)->notify(new Notify($order_id_dash, 'ruangan', ''));
                    event(new SocketEvent('2'));
                    //send email
                    $pinjam_ruangan     = PinjamRuangan::where('order_id', $order_id)->first();
                    Mail::send('email', ['permohonan' => $pinjam_ruangan], function ($message) use($pinjam_ruangan)
                    {
                        $message->subject(strtoupper($pinjam_ruangan->status->status));
                        $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                        $message->to($pinjam_ruangan->user->email);
                    });
                    return back()->with('success', 'Permohonan Anda akan diproses.');
                } else{
                    return back()->with('error', 'Anda telah mengajukan permohonan ruangan yang sama di waktu tersebut.');
                }
            } else {
                return back()->with('error', 'Permohonan Anda tidak dapat diproses karena ruangan tidak tersedia.');
            }
        }
    }

    public function show($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $pinjam_ruangan                 = PinjamRuangan::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('pinjam_ruangan.show', ['pinjam_ruangan' => $pinjam_ruangan, 'kelola' => 'no', 'runtext' => $runtext]);
    }

    public function kelola($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $pinjam_ruangan                 = PinjamRuangan::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('pinjam_ruangan.show', ['pinjam_ruangan' => $pinjam_ruangan, 'kelola' => 'yes', 'runtext' => $runtext]);
    }

    public function upload_nodin($order_id, UploadNodinRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $status_id                      = PinjamRuangan::where('order_id', $order_id)->first()['status_id'];
        //validasi apakah status masih belum diverifikasi
        if($status_id != 2 OR $status_id != 3){
            //operasi file
            $get_file                   = $request->nodin;
            $ext                        = $get_file->getClientOriginalExtension();
            $rename_file                = "Nodin_".$decrypt_order_id.".".$ext;
            //simpan update
            $pinjam_ruangan             = PinjamRuangan::where('order_id', $order_id)->first();
            $pinjam_ruangan->nodin      = $rename_file;
            $pinjam_ruangan->status_id  = "2";
            $pinjam_ruangan->save();
            Storage::disk('nodin')->delete($rename_file);
            Storage::disk('nodin')->put($rename_file, File::get($get_file));
            //send email
            Mail::send('email', ['permohonan' => $pinjam_ruangan], function ($message) use($pinjam_ruangan)
            {
                $message->subject(strtoupper($pinjam_ruangan->status->status));
                $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                $message->to($pinjam_ruangan->user->email);
            });

            return redirect()->back()->with('success', 'Berhasil mengunggah file nota dinas, silakan menunggu Admin untuk memverifikasi.');
        }
        //jika sudah diverifikasi
        else {
            return redirect()->back()->with('error', 'Tidak dapat mengunggah file nota dinas.');
        }
    }

    public function download_nodin($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $nodin                          = PinjamRuangan::where('order_id', $order_id)->first()['nodin'];

        return Storage::disk('nodin')->download($nodin);
    }

    public function download_lampiran($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $lampiran                       = PinjamRuangan::where('order_id', $order_id)->first()['lampiran'];

        return Storage::disk('lampiran')->download($lampiran);
    }

    public function edit($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $pinjam_ruangan                 = PinjamRuangan::where('order_id', $order_id)->first();
        $ruangan                        = Ruangan::all();
        $satker                         = Satker::all();
        $runtext                        = Runtext::all();

        return view('pinjam_ruangan.edit', ['pinjam_ruangan' => $pinjam_ruangan, 'ruangan' => $ruangan, 'satker' => $satker, 'runtext' => $runtext]);
    }

    public function update($order_id, PinjamRuanganRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $pinjam_ruangan                 = PinjamRuangan::where('order_id', $order_id)->first();
        $waktu_mulai                    = $request->get('waktu_mulai');
        $waktu_selesai                  = $request->get('waktu_selesai');
        $ruangan                        = Ruangan::all();
        $delete_dot_order_id            = str_replace(".", "", $order_id); 
        $replace_slash_order_id         = str_replace("/", "-", $delete_dot_order_id); 
        $encrypt_order_id               = Crypt::encryptString($replace_slash_order_id);
        // validasi apakah status permohonan masih belum diverifikasi admin
        if($pinjam_ruangan->status_id != 2 OR $pinjam_ruangan->status_id != 2){
            // fitur cek ketersediaan
            if($request->get('ketersediaan') != null){
                $ruangan_tersedia       = array();
                foreach($ruangan as $n){
                    $cek                = PinjamRuangan::
                                            where('ruangan_id',$request->get('ruangan_id'))
                                            ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                                $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                    ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                            })
                                            ->where('status_id', 3)
                                            ->first();
                    if($cek == null){
                        array_push($ruangan_tersedia, $n->nama);
                    }
                }
                if($ruangan_tersedia != null){
                    return back()->withInput()->with('ruangan_tersedia', $ruangan_tersedia);
                } else {
                    return back()->withInput()->with('tidak_tersedia', 'Ruangan Tidak Tersedia');
                }
            }
            // jika statusnya belum diverifikasi admin maka permohonan bisa diubah
            $pinjam_ruangan->satker_id      = $request->get('satker_id');
            $pinjam_ruangan->ruangan_id     = $request->get('ruangan_id');
            $pinjam_ruangan->waktu_mulai    = $request->get('waktu_mulai');
            $pinjam_ruangan->waktu_selesai  = $request->get('waktu_selesai');
            $pinjam_ruangan->kegiatan       = $request->get('kegiatan');
            $pinjam_ruangan->jml_pengguna   = $request->get('jml_pengguna');
            $pinjam_ruangan->layout         = $request->get('layout');
            // jika ada file nodin
            if($request->lampiran != NULL){
                $replace_slash_order_id     = str_replace("/", "-", $order_id);
                $order_id_dash              = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
                $get_nodin                  = $request->nodin;
                $ext                        = $get_nodin->getClientOriginalExtension();
                $rename_nodin               = "NODIN_".$order_id_dash.".".$ext;
                $pinjam_ruangan->nodin      = $rename_nodin;
            }
            // jika ada lampirannya
            if($request->lampiran != NULL){
                $get_lampiran               = $request->lampiran;
                $ext                        = $get_lampiran->getClientOriginalExtension();
                $rename_lampiran            = "LAMPIRAN_".$order_id_dash.".".$ext;
                $pinjam_ruangan->lampiran   = $rename_lampiran;
            }
            // validasi apakah ruangan tersedia
            $cek_ketersediaan           = PinjamRuangan::
                                            where('ruangan_id',$request->get('ruangan_id'))
                                            ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                                $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                    ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                            })
                                            ->where('order_id', '!=', $order_id)
                                            ->where('status_id',3)
                                            ->first();
            if($cek_ketersediaan == null){
                // jika tersedia, maka lakukan validasi apakah ada permohonan yang sama
                $cek_duplikasi          = PinjamRuangan::
                                            where('ruangan_id',$request->get('ruangan_id'))
                                            ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                                $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                    ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                            })
                                            ->where('user_id', Auth::user()->id)
                                            ->where('order_id', '!=', $order_id)
                                            ->first();
                if($cek_duplikasi == null){
                    // jika ada lampirannya
                    if($request->lampiran != NULL){
                        // hapus file jika ada yg sebelumnya
                        if(PinjamRuangan::where('order_id', $order_id)->first()['lampiran'] != null){
                            Storage::disk('lampiran')->delete(PinjamRuangan::where('order_id', $order_id)->first()['lampiran']);
                        }
                        // simpan file baru
                        Storage::disk('lampiran')->put($rename_lampiran, File::get($get_lampiran));
                    }
                    // jika tidak ada lampirannya dan sebelumnya ada lampirannya
                    if(PinjamRuangan::where('order_id', $order_id)->first()['lampiran'] != null){
                        $pinjam_ruangan->lampiran   = null;
                        Storage::disk('lampiran')->delete(PinjamRuangan::where('order_id', $order_id)->first()['lampiran']);
                    }
                    // jika tidak ada permohonan yang sama maka bisa disimpan
                    $pinjam_ruangan->save();
                    if($request->nodin != null){
                        Storage::disk('nodin')->delete(PinjamRuangan::where('order_id', $order_id)->first()['nodin']);
                        Storage::disk('nodin')->put($rename_nodin, File::get($get_nodin));
                    }
                    return redirect()->route('pinjam_ruangan.show', ['order_id' => $encrypt_order_id])->with('success', 'Berhasil mengubah data permohonan.');
                } else{
                    // jika ada permohonan yang sama maka permohonan tidak bisa disimpan
                    return redirect()->route('pinjam_ruangan.show', ['order_id' => $encrypt_order_id])->with('error', 'Anda telah mengajukan permohonan ruangan yang sama di waktu tersebut.');
                }
            } else {
                // jika ruangan tidak tersedia maka permohonan tidak bisa disimpan
                return redirect()->route('pinjam_ruangan.show', ['order_id' => $encrypt_order_id])->with('error', 'Permohonan Anda tidak dapat diproses karena ruangan tidak tersedia.');
            }
        }
    }

    public function destroy($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $pinjam_ruangan                 = PinjamRuangan::where('order_id', $order_id)->first();
        // validasi status masih belum diverifikasi admin
        if($pinjam_ruangan->status_id != 2 OR $pinjam_ruangan->status_id != 3){
            // jika statusnya belum diverifikasi admin maka permohonan bisa dihapus
            Storage::disk('nodin')->delete($pinjam_ruangan->nodin);
            $pinjam_ruangan->delete();
            //read-notif
            foreach(Auth::user()->unreadnotifications as $n){
                if($n->data['order_id'] == $decrypt_order_id){
                    $n->markasread();
                };
            }
            return redirect()->back()->with('success', 'Berhasil menghapus permohonan.');
        }
        else {
            // jika ruangan tidak tersedia maka permohonan tidak bisa dihapus
            return redirect()->back()->with('error', 'Tidak dapat menghapus permohonan.');
        }
    }





    public function add(AddPinjamRuang $request, PinjamRuang $pinjamruang)
    {
        $get_file       = $request->lampiran;
        $ext            = $get_file->getClientOriginalExtension();
        $rename_file    = "lampiran_".rand(100000,1001238912).".".$ext;
        Storage::disk('lampiran')->put($rename_file, File::get($get_file));

        $pinjamruang = $pinjamruang->create([
            'user_id'       => $request->user_id,
            'ruang_id'      => $request->ruang_id,
            'waktu_mulai'   => $request->waktu_mulai,
            'waktu_selesai' => $request->waktu_selesai,
            'nodin'         => $request->nodin,
            'kegiatan'      => $request->kegiatan,
            'lampiran'      => $rename_file,
        ]);
        
        $response = fractal()
                        ->item($pinjamruang)
                        ->transformWith(new PinjamRuangTransformer)
                        ->toArray();
        return response()->json($response, 301);
    }

    public function lists(PinjamRuang $pinjamruang)
    {
        $pinjamruangs = $pinjamruang->all();

        $response = fractal()
                        ->collection($pinjamruangs)
                        ->transformWith(new PinjamRuangTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function accepted(PinjamRuang $pinjamruang)
    {
        $pinjamruangs   = $pinjamruang->where('status_id', 2)->get();
        $response = fractal()
                        ->collection($pinjamruangs)
                        ->transformWith(new PinjamRuangTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function count_accepted(PinjamRuang $pinjamruang)
    {
        $pinjamruangs   = $pinjamruang->where('status_id', 2)->get();
        $jumlah         = count($pinjamruangs);

        return response()->json(['jumlah' => $jumlah], 200);
    }

    public function rejected(PinjamRuang $pinjamruang)
    {
        $pinjamruangs = $pinjamruang->where('status_id', 3)->get();

        $response = fractal()
                        ->collection($pinjamruangs)
                        ->transformWith(new PinjamRuangTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function count_rejected(PinjamRuang $pinjamruang)
    {
        $pinjamruangs   = $pinjamruang->where('status_id', 3)->get();
        $jumlah         = count($pinjamruangs);

        return response()->json(['jumlah' => $jumlah], 200);
    }

    public function accept(PinjamRuang $pinjamruang, $id)
    {
        $pinjamruangs = $pinjamruang->find($id);

        if($pinjamruangs->status_id == 1)
        {
            $data_status['order_id']        = $id;
            $data_status['models_id']       = 1;
            $data_status['status_id']       = 2;
            StatusHistories::create($data_status);
            $pinjamruangs->status_id        = 2;
            $pinjamruangs->save();
            
        } else {
            if($pinjamruang->status_id == 0) {
                return response()->json(['message' => 'Peminjaman belum dapat diproses!'], 404);
            } else {
                return response()->json(['message' => 'Peminjaman telah diproses!'], 404);
            }
        }

        $response = fractal()
                        ->item($pinjamruangs)
                        ->transformWith(new PinjamRuangTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function reject(PinjamRuang $pinjamruang, Request $request)
    {
        if($pinjamruang->status_id == 1)
        {
            $data_status['order_id']        = $pinjamruang->id;
            $data_status['models_id']       = 1;
            $data_status['status_id']       = 3;
            $data_status['alasan']          = $request->get('alasan', null);
            StatusHistories::create($data_status);
            $pinjamruang->status_id         = 3;
            $pinjamruang->save();
        } else {
            if($pinjamruang->status_id == 0) {
                return response()->json(['message' => 'Peminjaman belum dapat diproses!'], 404);
            } else {
                return response()->json(['message' => 'Peminjaman telah diproses!'], 404);
            }
        }

        $response = fractal()
                        ->item($pinjamruang)
                        ->transformWith(new PinjamRuangTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function shows(PinjamRuang $pinjamruang, $id)
    {
        $pinjamruangs = $pinjamruang->find($id);

        $response = fractal()
                        ->item($pinjamruangs)
                        ->transformWith(new PinjamRuangTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function edits(PinjamRuang $pinjamruang, Request $request)
    {
        $pinjamruang->ruang_id          = $request->get('ruang_id', $pinjamruang->ruang_id);
        $pinjamruang->waktu_mulai       = $request->get('waktu_mulai', $pinjamruang->waktu_mulai);
        $pinjamruang->waktu_selesai     = $request->get('waktu_selesai', $pinjamruang->waktu_selesai);
        $pinjamruang->kegiatan          = $request->get('kegiatan', $pinjamruang->kegiatan);
        $pinjamruang->nodin             = $request->get('nodin', $pinjamruang->nodin);
        
        if ($request->get('lampiran') != NULL)
        {
            $get_file                   = $request->lampiran;
            $ext                        = $file->getClientOriginalExtension();
            $new_file                   = "lampiran_".rand(100000,1001238912).".".$ext;
            Storage::disk('lampiran')->put($new_file, File::get($get_file));
            $pinjamruang->lampiran      = $request->get('lampiran', $pinjamruang->lampiran);
        }
        
        $pinjamruang->save();

        $response = fractal()
                        ->item($pinjamruang)
                        ->transformWith(new PinjamRuangTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function delete(PinjamRuang $pinjamruang)
    {
        $pinjamruang->delete();

        return response()->json([
            'message'   => 'Deleted!'
        ]);
    }

    public function get_file_lampiran(PinjamRuang $pinjamruang, $id)
    {
        $pinjamruangs   = $pinjamruang->find($id);
        $filename       = $pinjamruangs->lampiran;
        
        // $file           = Storage::disk('lampiran')->get($filename);
        // return (new Response($file, 200))
        // ->header('Content-Type', $pinjamruangs->mime);

		return response()->download(storage_path('app/public/lampiran/'.$filename));
    }
}
