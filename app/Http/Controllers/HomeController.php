<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Spatie\Permission\Models\Permission;
use Spatie\Permission\Models\Role;
use App\User;
use App\Penggandaan;
use App\Pengumuman;
use App\PerbaikanBarang;
use App\PinjamBarang;
use App\PinjamRuangan;
use App\PinjamKendaraan;
use App\SaranAduan;
use App\Barang;
use App\Satker;
use App\Runtext;
use Auth;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        // $role = Role::findByName('administrator');
        // $role->givePermissionTo('edit ruangan');
        // $user = User::where('name', 'test')->first();
        // $user->assignRole('administrator');
        $user = Auth::user();
        if($user->hasAnyRole('Administrator|Pengelola Urdal|Pengelola Kendaraan')){
            $total_permohonan               = count(Penggandaan::groupBy('order_id')->get()) + 
                                              count(PerbaikanBarang::groupBy('order_id')->get()) +
                                              count(PinjamBarang::groupBy('order_id')->get()) + 
                                              PinjamRuangan::count() + 
                                              PinjamKendaraan::count() + 
                                              SaranAduan::count();
            $permohonan_disetujui           = count(Penggandaan::where('status_id', 3)->groupBy('order_id')->get()) + 
                                              count(PerbaikanBarang::where('status_id', 3)->groupBy('order_id')->get()) +
                                              count(PinjamBarang::where('status_id', 3)->groupBy('order_id')->get()) + 
                                              PinjamRuangan::where('status_id', 3)->count() + 
                                              PinjamKendaraan::where('status_id', 3)->count() + 
                                              SaranAduan::where('status_id', 3)->count();
            $permohonan_ditolak             = count(Penggandaan::where('status_id', 4)->groupBy('order_id')->get()) + 
                                              count(PerbaikanBarang::where('status_id', 4)->groupBy('order_id')->get()) +
                                              count(PinjamBarang::where('status_id', 4)->groupBy('order_id')->get()) + 
                                              PinjamRuangan::where('status_id', 4)->count() + 
                                              PinjamKendaraan::where('status_id', 4)->count() + 
                                              SaranAduan::where('status_id', 4)->count();
            $permohonan_menunggu            = count(Penggandaan::where('status_id', 2)->groupBy('order_id')->get()) + 
                                              count(PerbaikanBarang::where('status_id', 2)->groupBy('order_id')->get()) +
                                              count(PinjamBarang::where('status_id', 2)->groupBy('order_id')->get()) + 
                                              PinjamRuangan::where('status_id', 2)->count() + 
                                              PinjamKendaraan::where('status_id', 2)->count() + 
                                              SaranAduan::where('status_id', 2)->count();
            $runtext                        = Runtext::all();
            return view('home_admin', ['total_permohonan' => $total_permohonan, 'permohonan_disetujui' => $permohonan_disetujui, 
                            'permohonan_ditolak' => $permohonan_ditolak, 'permohonan_menunggu' => $permohonan_menunggu, 'runtext' => $runtext]);
        }
        else{
            $runtext                        = Runtext::all();
            $pengumuman                     = Pengumuman::first();
            return view('home', ['runtext' => $runtext, 'pengumuman' => $pengumuman]);
        }
    }
}
