<?php

namespace App\Http\Controllers;

use App\Ruangan;
use App\Runtext;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests\RuanganRequest;
use App\Transformers\RuangRapatTransformer;

class RuanganController extends Controller
{
    public function index()
    {
        $ruangan_all            = Ruangan::all();
        $ruangan                = Ruangan::where('id', 1)->first();
        $runtext                = Runtext::all();

        return view('ruangan.index', ['ruangan_all' => $ruangan_all, 'ruangan' => $ruangan, 'runtext' => $runtext]);
    }

    public function filter(Request $request)
    {
        $ruangan                = Ruangan::where('id', $request->ruangan_id)->first();
        $runtext                = Runtext::all();

        return redirect()->route('ruangan.index', ['ruangan' => $ruangan, 'runtext' => $runtext]);
    }

    public function create()
    {
        $runtext                = Runtext::all();

        return view('ruangan.create', ['runtext' => $runtext]);
    }

    public function store(RuanganRequest $request)
    {
        $ruangan_all            = Ruangan::all();
        $ruangan['nama']        = $request->get('nama');
        $ruangan['lokasi']      = $request->get('lokasi');
        $ruangan['kapasitas']   = $request->get('kapasitas');
        $ruangan['fasilitas']   = $request->get('fasilitas');
        $ruangan['keterangan']  = $request->get('keterangan');
        if($request->image != null){
            $get_file           = $request->image;
            $ext                = $get_file->getClientOriginalExtension();
            $rename_file        = "ruangan_".rand(100000,1001238912).".".$ext;
            $ruangan['image']   = $rename_file; 
        } else {
            $ruangan['image']   = "room_1.jpg";
        }
        Ruangan::create($ruangan);
        Storage::disk('public')->put($rename_file, File::get($get_file));

        return redirect()->route('ruangan.kelola', ['ruangan' => $ruangan_all])->with('success', 'Berhasil menambah ruangan.');
    }

    public function kelola()
    {
        $ruangan                = Ruangan::all();
        $runtext                = Runtext::all();

        return view('ruangan.kelola', ['ruangan' => $ruangan, 'runtext' => $runtext]);
    }

    public function edit($id)
    {
        $id                     = Crypt::decryptString($id);
        $ruangan                = Ruangan::where('id', $id)->first();
        $runtext                = Runtext::all();

        return view('ruangan.edit', ['ruangan' => $ruangan, 'runtext' => $runtext]);
    }

    public function update($id, RuanganRequest $request)
    {
        $id                     = Crypt::decryptString($id);
        $ruangan                = Ruangan::where('id', $id)->first();
        $ruangan_all            = Ruangan::all();
        $ruangan->nama          = $request->get('nama');
        $ruangan->lokasi        = $request->get('lokasi');
        $ruangan->kapasitas     = $request->get('kapasitas');
        $ruangan->fasilitas     = $request->get('fasilitas');
        $ruangan->keterangan    = $request->get('keterangan');
        if($request->image != null){
            $get_file           = $request->image;
            $ext                = $get_file->getClientOriginalExtension();
            $rename_file        = "ruangan_".rand(100000,1001238912).".".$ext;
            $ruangan->image     = $rename_file; 
            Storage::disk('public')->delete(Ruangan::where('id', $id)->first()['image']);
            Storage::disk('public')->put($rename_file, File::get($get_file));
        }
        $ruangan->save();

        return redirect()->route('ruangan.kelola', ['ruangan' => $ruangan_all])->with('success', 'Berhasil mengubah data ruangan.');
    }

    public function destroy($id)
    {
        $id                     = Crypt::decryptString($id);
        $ruangan_all            = Ruangan::all();
        $ruangan                = Ruangan::where('id', $id)->first();
        $ruangan->delete();
        Storage::disk('public')->delete(Ruangan::where('id', $id)->first()['image']);

        return redirect()->route('ruangan.kelola', ['ruangan' => $ruangan_all])->with('success', 'Berhasil menghapus ruangan.');
    }




    public function lists(RuangRapat $ruangrapat)
    {
        $ruangrapats = $ruangrapat->all();
        $response = fractal()
            ->collection($ruangrapats)
            ->transformWith(new RuangRapatTransformer)
            ->toArray();

        return response()->json($response, 200);
    }

    public function show(RuangRapat $ruangrapat, $id)
    {
        $ruangrapat = $ruangrapat->find($id);

        $response = fractal()
            ->item($ruangrapat)
            ->transformWith(new RuangRapatTransformer)
            ->toArray();

        return response()->json($response, 200);
    }

    public function add(AddRuangRapat $request, RuangRapat $ruangrapat)
    {
        $ruangrapat = $ruangrapat->create([
            'nama'      => $request->nama,
            'lokasi'    => $request->lokasi,
            'fasilitas' => $request->fasilitas,
            'kapasitas' => $request->kapasitas,
        ]);

        $response = fractal()
            ->item($ruangrapat)
            ->transformWith(new RuangRapatTransformer)
            ->addMeta([
                'message' => "Added!",
            ])
            ->toArray();

        return response()->json($response, 201);
    }

    public function edits(Request $request, RuangRapat $ruangrapat)
    {
        $ruangrapat->nama       = $request->get('nama', $ruangrapat->nama);
        $ruangrapat->lokasi     = $request->get('lokasi', $ruangrapat->lokasi);
        $ruangrapat->fasilitas  = $request->get('fasilitas', $ruangrapat->fasilitas);
        $ruangrapat->kapasitas  = $request->get('kapasitas', $ruangrapat->kapasitas);

        $ruangrapat->save();

        return fractal()
            ->item($ruangrapat)
            ->transformWith(new RuangRapatTransformer)
            ->addMeta([
                'message' => "Updated!",
            ])
            ->toArray();
    }
    
    public function delete(RuangRapat $ruangrapat)
    {
        $ruangrapat->delete();

        return response()->json([
            'message' => 'Deleted!',
        ]);
    }
}
