<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use App\SaranAduan;
use App\Runtext;
use App\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Http\Requests\SaranAduanRequest;
use App\Http\Requests\UploadLampiranRequest;
use App\Notifications\Notify;
use App\Events\SocketEvent;

class SaranAduanController extends Controller
{
    public function create()
    {
        $runtext                        = Runtext::all();
        return view('saran_aduan.create', ['runtext' => $runtext]);
    }

    public function store(SaranAduanRequest $request)
    {
        $bulan_romawi                   = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
        $angka_bulan                    = date('n', strtotime(Carbon::now()));
        $tahun                          = date('Y', strtotime(Carbon::now()));
        $data['jenis']                  = $request->get('jenis');
        $data['isian']                  = $request->get('isian');
        $data['user_id']                = Auth::user()->id;
        $data['status_id']              = "2";
        $order_id_sebelumnya            = SaranAduan::all()->sortByDesc('id')->first()['order_id'];
        if($order_id_sebelumnya != null){
            $substr                     = substr($order_id_sebelumnya, strpos($order_id_sebelumnya,"SIUD.7/")+strlen("SIUD.7/"),strlen($order_id_sebelumnya));
            $urutan_sebelumnya          = substr($substr,0,strpos($substr,"/"));
            $urutan                     = $urutan_sebelumnya+1;
            $order_id                   = "SIUD.7/".$urutan."/".$bulan_romawi[$angka_bulan]."/".$tahun; 
        } else{
            $order_id                   = "SIUD.7/1/".$bulan_romawi[$angka_bulan]."/".$tahun;
        }
        if($request->lampiran != NULL){
            $get_file                   = $request->lampiran;
            $ext                        = $get_file->getClientOriginalExtension();
            $rename_file                = "Lampiran_".rand(100000,1001238912).".".$ext;
            Storage::disk('lampiran')->put($rename_file, File::get($get_file));
            $data['lampiran']           = $rename_file;
        }
        $data['order_id']               = $order_id;
        SaranAduan::create($data);
        $replace_slash_order_id         = str_replace("/", "-", $order_id);
        $order_id_dash                  = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
        //notify
        User::find(2)->notify(new Notify($order_id_dash, 'saran / aduan', ''));
        event(new SocketEvent('2'));
        //send email
        $saran_aduan                    = SaranAduan::where('order_id', $order_id)->first();
        Mail::send('email', ['permohonan' => $saran_aduan], function ($message) use($saran_aduan)
        {
            $message->subject(strtoupper($saran_aduan->status->status));
            $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
            $message->to($saran_aduan->user->email);
        });

        return redirect()->back()->with('success', 'Terima kasih atas masukan dan sarannya.');
    }

    public function show($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $saran_aduan                    = SaranAduan::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('saran_aduan.show', ['saran_aduan' => $saran_aduan, 'kelola' => 'no', 'runtext' => $runtext]);
    }

    public function kelola($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $saran_aduan                    = SaranAduan::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('saran_aduan.show', ['saran_aduan' => $saran_aduan, 'kelola' => 'yes', 'runtext' => $runtext]);
    }

    public function upload_lampiran($order_id, UploadLampiranRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $saran_aduan                    = SaranAduan::where('order_id', $order_id)->first();
        //validasi apakah status masih belum diverifikasi
        if($saran_aduan->status_id != 2 OR $saran_aduan->status_id != 3){
            //operasi file
            $get_file                   = $request->lampiran;
            $ext                        = $get_file->getClientOriginalExtension();
            $rename_file                = "Lampiran_".$decrypt_order_id.".".$ext;
            //simpan update
            $saran_aduan                = SaranAduan::where('order_id', $order_id)->get();
            foreach($saran_aduan as $n){
                $n->lampiran            = $rename_file;
                $n->status_id           = "2";
                $n->save();
            }
            Storage::disk('lampiran')->delete($rename_file);
            Storage::disk('lampiran')->put($rename_file, File::get($get_file));

            return redirect()->back()->with('success', 'Berhasil mengunggah file lampiran.');
        }
        //jika sudah diverifikasi
        else {
            return redirect()->back()->with('error', 'Tidak dapat mengunggah file lampiran.');
        }
    }

    public function download_lampiran($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $lampiran                       = SaranAduan::where('order_id', $order_id)->first()['lampiran'];

        return Storage::disk('lampiran')->download($lampiran);
    }

    public function edit($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $saran_aduan                    = SaranAduan::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('saran_aduan.edit', ['saran_aduan' => $saran_aduan, 'runtext' => $runtext]);
    }

    public function update($order_id, SaranAduanRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $saran_aduan                    = SaranAduan::where('order_id', $order_id)->first();
        $delete_dot_order_id            = str_replace(".", "", $order_id); 
        $replace_slash_order_id         = str_replace("/", "-", $delete_dot_order_id); 
        $encrypt_order_id               = Crypt::encryptString($replace_slash_order_id);
        // validasi apakah status permohonan masih belum diverifikasi admin
        if($saran_aduan->status_id != 2 OR $saran_aduan->status_id != 3){
            $saran_aduan->jenis         = $request->get('jenis');
            $saran_aduan->isian         = $request->get('isian');
            $saran_aduan->save();

            return redirect()->route('saranaduan.show', ['order_id' => $encrypt_order_id])->with('success', 'Berhasil mengubah data permohonan.');
        }
        else {
            // jika ruangan tidak tersedia maka permohonan tidak bisa disimpan
            return redirect()->route('saranaduan.show', ['order_id' => $encrypt_order_id])->with('error', 'Tidak dapat mengubah permohonan.');
        }
    }

    public function destroy($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $saran_aduan                    = SaranAduan::where('order_id', $order_id)->first();
        // validasi apakah status permohonan masih belum diverifikasi admin
        if($saran_aduan->status_id != 2 OR $saran_aduan->status_id != 3){
            // jika statusnya belum diverifikasi admin maka permohonan bisa dihapus
            Storage::disk('lampiran')->delete($saran_aduan->lampiran);
            $saran_aduan->delete();
            //read-notif
            foreach(Auth::user()->unreadnotifications as $n){
                if($n->data['order_id'] == $decrypt_order_id){
                    $n->markasread();
                };
            }
            return redirect()->back()->with('success', 'Berhasil menghapus permohonan.');
        }
        else {
            // jika ruangan tidak tersedia maka permohonan tidak bisa dihapus
            return redirect()->back()->with('error', 'Tidak dapat menghapus permohonan.');
        }
    }
}
