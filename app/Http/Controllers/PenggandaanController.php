<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Satker;
use App\Penggandaan;
use App\Runtext;
use App\User;
use App\Http\Requests\PenggandaanRequest;
use App\Notifications\Notify;
use App\Events\SocketEvent;

class PenggandaanController extends Controller
{
    public function create()
    {
        $satker                 = Satker::all();
        $runtext                = Runtext::all();
        return view('penggandaan.create', ['satker' => $satker, 'runtext' => $runtext]);
    }

    public function store(PenggandaanRequest $request)
    {
        $bulan_romawi                   = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
        $angka_bulan                    = date('n', strtotime(Carbon::now()));
        $tahun                          = date('Y', strtotime(Carbon::now()));
        $cek_entri_sebelumnya           = Penggandaan::first();
        if($cek_entri_sebelumnya == null){
            $order_id                   = "SIUD.5/1/".$bulan_romawi[$angka_bulan]."/".$tahun;
        } else {
            $order_id_sebelumnya        = Penggandaan::orderByDesc('id')->first()['order_id'];
            $substr                     = substr($order_id_sebelumnya, strpos($order_id_sebelumnya,"SIUD.5/")+strlen("SIUD.5/"),strlen($order_id_sebelumnya));
            $urutan_sebelumnya          = substr($substr,0,strpos($substr,"/"));
            $urutan                     = $urutan_sebelumnya+1;
            $order_id                   = "SIUD.5/".$urutan."/".$bulan_romawi[$angka_bulan]."/".$tahun; 
        }
        $penggandaan['user_id']         = Auth::user()->id;
        $penggandaan['order_id']        = $order_id;
        $penggandaan['satker_id']       = $request->get('satker_id');
        foreach($request->get('barang') as $n){
            $penggandaan['judul']           = $n['judul'];
            $penggandaan['kertas']          = $n['kertas'];
            $penggandaan['hlm_warna']       = $n['hlm_warna'];
            $penggandaan['jml_warna']       = $n['jml_warna'];
            $penggandaan['hlm_hitam_putih'] = $n['hlm_hitam_putih'];
            $penggandaan['jml_hitam_putih'] = $n['jml_hitam_putih'];
            $penggandaan['keterangan']      = $n['keterangan'];
            $penggandaan['status_id']       = "1";
            Penggandaan::create($penggandaan);
        }
        $penggandaan                    = Penggandaan::where('order_id', $order_id)->get();
        // return $penggandaan;
        $replace_slash_order_id         = str_replace("/", "-", $order_id);
        $order_id_dash                  = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
        //notify
        User::find(4)->notify(new Notify($order_id_dash, 'penggandaan', ''));
        event(new SocketEvent('4'));
        //send email
        Mail::send('email', ['permohonan' => $penggandaan], function ($message) use($penggandaan)
        {
            $message->subject(strtoupper($penggandaan->first()->status->status));
            $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
            $message->to($penggandaan->first()->user->email);
        });

        return redirect()->back()->with('success', 'Permohonan Anda akan segera diproses.');
    }

    public function show($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $penggandaan                    = Penggandaan::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('penggandaan.show', ['penggandaan' => $penggandaan, 'kelola' => 'no', 'runtext' => $runtext]);
    }

    public function kelola($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $penggandaan                    = Penggandaan::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('penggandaan.show', ['penggandaan' => $penggandaan, 'kelola' => 'yes', 'runtext' => $runtext]);
    }

    public function edit($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $penggandaan                    = Penggandaan::where('order_id', $order_id)->get();
        $satker                         = Satker::all();
        $runtext                        = Runtext::all();

        return view('penggandaan.edit', ['penggandaan' => $penggandaan, 'satker' => $satker, 'runtext' => $runtext]);
    }

    public function update($order_id, PenggandaanRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $penggandaan                    = Penggandaan::where('order_id', $order_id)->first();
        $delete_dot_order_id            = str_replace(".", "", $order_id); 
        $replace_slash_order_id         = str_replace("/", "-", $delete_dot_order_id); 
        $encrypt_order_id               = Crypt::encryptString($replace_slash_order_id);
        // validasi status masih belum diverifikasi admin
        if($penggandaan->status_id != 2 OR $penggandaan->status_id != 3){
            // jika statusnya belum diverifikasi admin maka permohonan bisa diubah
            $delete_entry               = Penggandaan::where('order_id', $order_id)->delete();
            foreach($request->get('barang') as $n){
                $data['order_id']       = $order_id;
                $data['user_id']        = Auth::user()->id;
                $data['satker_id']      = $request->get('satker_id');
                $data['judul']          = $n['judul'];
                $data['kertas']         = $n['kertas'];
                $data['hlm_warna']      = $n['hlm_warna'];
                $data['jml_warna']      = $n['jml_warna'];
                $data['hlm_hitam_putih']= $n['hlm_hitam_putih'];
                $data['jml_hitam_putih']= $n['jml_hitam_putih'];
                $data['keterangan']     = $n['keterangan'];
                $data['status_id']      = $penggandaan->status_id;
                Penggandaan::create($data);
            }
            return redirect()->route('penggandaan.show', ['order_id' => $encrypt_order_id])->with('success', 'Permohonan berhasil diubah.');
        }
        // jika sudah diverifikasi admin maka tidak bisa diubah
        else { 
            return redirect()->route('penggandaan.show', ['order_id' => $encrypt_order_id])->with('error', 'Permohonan tidak dapat diubah.');
        }
    }

    public function destroy($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $penggandaan                    = Penggandaan::where('order_id', $order_id)->get();
        // validasi status masih belum diverifikasi admin
        if($penggandaan->first()['status_id'] != 2 OR $penggandaan->first()['status_id'] != 3){
            // jika statusnya belum diverifikasi admin maka permohonan bisa dihapus
            foreach($penggandaan as $p){
                $p->delete();
            }
            //read-notif
            foreach(Auth::user()->unreadnotifications as $n){
                if($n->data['order_id'] == $decrypt_order_id){
                    $n->markasread();
                };
            }
            return redirect()->back()->with('success', 'Berhasil menghapus permohonan.');
        }
        else {
            // jika ruangan tidak tersedia maka permohonan tidak bisa dihapus
            return redirect()->back()->with('error', 'Tidak dapat menghapus permohonan.');
        }
    }
}
