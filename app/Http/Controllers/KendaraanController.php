<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Kendaraan;
use App\PinjamKendaraan;
use App\Runtext;
use App\Http\Requests\KendaraanRequest;
use App\Transformers\KendaraanTransformer;

class KendaraanController extends Controller
{
    public function index()
    {
        $kendaraan              = Kendaraan::all();
        $pinjam_kendaraan       = PinjamKendaraan::orderBy('waktu_mulai', 'desc')->get();
        $runtext                = Runtext::all();

        return view('kendaraan.index', ['kendaraan' => $kendaraan, 'pinjam_kendaraan' => $pinjam_kendaraan, 'runtext' => $runtext]);
    }

    public function kelola()
    {
        $kendaraan              = Kendaraan::all();
        $runtext                = Runtext::all();

        return view('kendaraan.kelola', ['kendaraan' => $kendaraan, 'runtext' => $runtext]);
    }

    public function create()
    {
        $runtext                = Runtext::all();

        return view('kendaraan.create', ['runtext' => $runtext]);
    }

    public function store(KendaraanRequest $request)
    {
        $kendaraan_all          = Kendaraan::all();
        $kendaraan['nama']          = $request->get('nama');
        $kendaraan['nomor_polisi']  = $request->get('nomor_polisi');
        $kendaraan['kapasitas']     = $request->get('kapasitas');
        Kendaraan::create($kendaraan);

        return redirect()->route('kendaraan.kelola', ['kendaraan' => $kendaraan_all])->with('success', 'Berhasil menambah kendaraan.');
    }

    public function edit($id)
    {
        $id                     = Crypt::decryptString($id);
        $kendaraan              = Kendaraan::where('id', $id)->first();
        $runtext                = Runtext::all();

        return view('kendaraan.edit', ['kendaraan' => $kendaraan, 'runtext' => $runtext]);
    }

    public function update($id, KendaraanRequest $request)
    {
        $id                     = Crypt::decryptString($id);
        $kendaraan_all          = Kendaraan::all();
        $kendaraan              = Kendaraan::where('id', $id)->first();
        $kendaraan->nama        = $request->get('nama');
        $kendaraan->nomor_polisi= $request->get('nomor_polisi');
        $kendaraan->kapasitas   = $request->get('kapasitas');
        $kendaraan->save();

        return redirect()->route('kendaraan.kelola', ['kendaraan' => $kendaraan_all])->with('success', 'Berhasil mengubah data kendaraan.');
    }
    
    public function destroy($id)
    {
        $id                     = Crypt::decryptString($id);
        $kendaraan              = Kendaraan::where('id', $id)->first();
        $kendaraan->delete();

        return redirect()->back()->with('success', 'Berhasil mengubah data kendaraan.');
    }




    public function lists(Kendaraan $kendaraan)
    {
        $kendaraan = $kendaraan->all();
        $response = fractal()
            ->collection($kendaraan)
            ->transformWith(new KendaraanTransformer)
            ->toArray();

        return response()->json($response, 200);
    }

    public function show(Kendaraan $kendaraan, $id)
    {
        $kendaraan = $kendaraan->find($id);

        $response = fractal()
            ->item($kendaraan)
            ->transformWith(new KendaraanTransformer)
            ->toArray();

        return response()->json($response, 200);
    }

    public function add(AddKendaraan $request, Kendaraan $kendaraan)
    {
        $kendaraan = $kendaraan->create([
            'nama'              => $request->nama,
            'nomor_polisi'      => $request->nomor_polisi,
            'kapasitas'         => $request->kapasitas." seats",
        ]);

        $response = fractal()
            ->item($kendaraan)
            ->transformWith(new KendaraanTransformer)
            ->addMeta([
                'message' => "Added!",
            ])
            ->toArray();

        return response()->json($response, 201);
    }

    public function edits(Request $request, Kendaraan $kendaraan)
    {
        $kendaraan->nama            = $request->get('nama', $kendaraan->nama);
        $kendaraan->nomor_polisi    = $request->get('nomor_polisi', $kendaraan->nomor_polisi);
        $kendaraan->kapasitas       = $request->get('kapasitas'." seats", $kendaraan->kapasitas);

        $kendaraan->save();

        return fractal()
            ->item($kendaraan)
            ->transformWith(new KendaraanTransformer)
            ->addMeta([
                'message' => "Updated!",
            ])
            ->toArray();
    }

    public function delete(Kendaraan $kendaraan)
    {
        $kendaraan->delete();

        return response()->json([
            'message' => 'Deleted!',
        ]);
    }
}
