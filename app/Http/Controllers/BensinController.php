<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Validator;
use Carbon\Carbon;
use App\Bensin;
use App\Satker;
use App\StatusHistories;
use App\Runtext;
use App\User;
use App\Transformers\BensinTransformer;
use App\Http\Requests\BensinRequest;
use App\Http\Requests\UploadKuitansiRequest;
use Illuminate\Http\Request;
use App\Notifications\Notify;
use App\Events\SocketEvent;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use Illuminate\Support\Facades\Crypt;

class BensinController extends Controller
{
    public function create()
    {
        $satker                         = Satker::all();
        $runtext                        = Runtext::all();
        return view('bensin.create', ['satker' => $satker, 'runtext' => $runtext]);
    }

    public function store(BensinRequest $request)
    {
        Validator::make($request->all(), [
            'kuitansi' => 'required',
        ], [
            'required' => 'Kolom :attribute harus diisi.',
        ])->validate();
        $bulan_romawi                   = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
        $angka_bulan                    = date('n', strtotime(Carbon::now()));
        $tahun                          = date('Y', strtotime(Carbon::now()));
        $bensin['waktu']                = date('Y-m-d', strtotime($request->get('waktu')));
        $bensin['satker_id']            = $request->get('satker_id');
        $bensin['jabatan']              = $request->get('jabatan');
        $bensin['total_penggunaan']     = $request->get('total_penggunaan');
        $bensin['user_id']              = Auth::user()->id;
        $bensin['status_id']            = "1";
        $order_id_sebelumnya            = Bensin::all()->sortByDesc('id')->first()['order_id'];
        if($order_id_sebelumnya != null){
            $substr                     = substr($order_id_sebelumnya, strpos($order_id_sebelumnya,"SIUD.1/")+strlen("SIUD.1/"),strlen($order_id_sebelumnya));
            $urutan_sebelumnya          = substr($substr,0,strpos($substr,"/"));
            $urutan                     = $urutan_sebelumnya+1;
            $order_id                   = "SIUD.6/".$urutan."/".$bulan_romawi[$angka_bulan]."/".$tahun; 
        } else{
            $order_id                   = "SIUD.6/1/".$bulan_romawi[$angka_bulan]."/".$tahun;
        }
        $get_file                       = $request->kuitansi;
        $ext                            = $get_file->getClientOriginalExtension();
        $rename_file                    = "Kuitansi_".rand(100000,1001238912).".".$ext;
        Storage::disk('kuitansi')->put($rename_file, File::get($get_file));
        $bensin['kuitansi']             = $rename_file;
        $bensin['order_id']             = $order_id;
        Bensin::create($bensin);
        $replace_slash_order_id     = str_replace("/", "-", $order_id);
        $order_id_dash              = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
        //notify
        User::find(2)->notify(new Notify($order_id_dash, 'bensin', ''));
        event(new SocketEvent('2'));
        //send email
        $bensin                         = Bensin::where('order_id', $order_id)->first();
        Mail::send('email', ['permohonan' => $bensin], function ($message) use($bensin)
        {
            $message->subject(strtoupper($bensin->status->status));
            $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
            $message->to($bensin->user->email);
        });

        return redirect()->back()->with('success', 'Berhasil menambah permohonan baru. Silakan periksa di halaman Permohonan Saya.');
    }

    public function show($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $bensin                         = Bensin::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('bensin.show', ['bensin' => $bensin, 'kelola' => 'no', 'runtext' => $runtext]);
    }

    public function kelola($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $bensin                         = Bensin::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('bensin.show', ['bensin' => $bensin, 'kelola' => 'yes', 'runtext' => $runtext]);
    }

    public function download_kuitansi($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $kuitansi                       = Bensin::where('order_id', $order_id)->first()['kuitansi'];

        return Storage::disk('kuitansi')->download($kuitansi);
    }

    public function upload_kuitansi($order_id, UploadKuitansiRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $bensin                         = Bensin::where('order_id', $order_id)->first();
        //validasi apakah status masih belum diverifikasi
        if($bensin->status_id != 2 OR $bensin->status_id != 3){
            //operasi file
            $get_file                   = $request->kuitansi;
            $ext                        = $get_file->getClientOriginalExtension();
            $rename_file                = "Kuitansi_".$decrypt_order_id.".".$ext;
            //simpan update
            $bensin->kuitansi           = $rename_file;
            $bensin->save();
            Storage::disk('kuitansi')->delete($rename_file);
            Storage::disk('kuitansi')->put($rename_file, File::get($get_file));

            return redirect()->back()->with('success', 'Berhasil mengunggah file lampiran.');
        }
        //jika sudah diverifikasi
        else {
            return redirect()->back()->with('error', 'Tidak dapat mengunggah file lampiran.');
        }
    }

    public function edit($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $bensin                         = Bensin::where('order_id', $order_id)->first();
        $satker                         = Satker::all();
        $runtext                        = Runtext::all();

        return view('bensin.edit', ['bensin' => $bensin, 'satker' => $satker, 'runtext' => $runtext]);
    }

    public function update($order_id, BensinRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $bensin                         = Bensin::where('order_id', $order_id)->first();
        $delete_dot_order_id            = str_replace(".", "", $order_id); 
        $replace_slash_order_id         = str_replace("/", "-", $delete_dot_order_id); 
        $encrypt_order_id               = Crypt::encryptString($replace_slash_order_id);
        // validasi apakah status permohonan masih belum diverifikasi admin
        if($bensin->status_id != 2 OR $bensin->status_id != 3){
            $bensin->waktu              = date('Y-m-d', strtotime($request->get('waktu')));
            $bensin->satker_id          = $request->get('satker_id');
            $bensin->jabatan            = $request->get('jabatan');
            $bensin->total_penggunaan   = $request->get('total_penggunaan');
            $bensin->save();

            return redirect()->route('bensin.show', ['order_id' => $encrypt_order_id])->with('success', 'Berhasil mengubah data permohonan.');
        }
        else {
            // jika ruangan tidak tersedia maka permohonan tidak bisa disimpan
            return redirect()->route('bensin.show', ['order_id' => $encrypt_order_id])->with('error', 'Tidak dapat mengubah permohonan.');
        }
    }

    public function destroy($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $bensin                         = Bensin::where('order_id', $order_id)->first();
        // validasi apakah status permohonan masih belum diverifikasi admin
        if($bensin->status_id != 2 OR $bensin->status_id != 3){
            // jika statusnya belum diverifikasi admin maka permohonan bisa dihapus
            Storage::disk('kuitansi')->delete($bensin->kuitansi);
            $bensin->delete();
            //read-notif
            foreach(Auth::user()->unreadnotifications as $n){
                if($n->data['order_id'] == $decrypt_order_id){
                    $n->markasread();
                };
            }
            return redirect()->back()->with('success', 'Berhasil menghapus permohonan.');
        }
        else {
            // jika ruangan tidak tersedia maka permohonan tidak bisa dihapus
            return redirect()->back()->with('error', 'Tidak dapat menghapus permohonan.');
        }
    }





    public function add(BensinRequest $request, Bensin $bensin)
    {
        $get_file       = $request->kwitansi;
        $ext            = $get_file->getClientOriginalExtension();
        $rename_file    = "kwitansi_".rand(100000,1001238912).".".$ext;
        Storage::disk('kwitansi')->put($rename_file, File::get($get_file));

        $bensin         = $bensin->create([
            'user_id'           => $request->user_id,
            'waktu'             => $request->waktu,
            'jabatan'           => $request->jabatan,
            'total_penggunaan'  => $request->total_penggunaan,
            'kwitansi'          => $rename_file,
        ]);

        $response = fractal()
                        ->item($bensin)
                        ->transformWith(new BensinTransformer)
                        ->toArray();
        return response()->json($response, 301);
    }

    public function lists(Bensin $bensin)
    {
        $bensin = $bensin->all();

        $response = fractal()
                        ->collection($bensin)
                        ->transformWith(new BensinTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function accepted(Bensin $bensin)
    {
        $bensin   = $bensin->where('status_id', 2)->get();
        $response = fractal()
                        ->collection($bensin)
                        ->transformWith(new BensinTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function count_accepted(Bensin $bensin)
    {
        $bensin         = $bensin->where('status_id', 2)->get();
        $jumlah         = count($bensin);

        return response()->json(['jumlah' => $jumlah], 200);
    }

    public function rejected(Bensin $bensin)
    {
        $bensin         = $bensin->where('status_id', 3)->get();

        $response = fractal()
                        ->collection($bensin)
                        ->transformWith(new BensinTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function count_rejected(Bensin $bensin)
    {
        $bensin         = $bensin->where('status_id', 3)->get();
        $jumlah         = count($bensin);

        return response()->json(['jumlah' => $jumlah], 200);
    }

    public function accept(Bensin $bensin, $id)
    {
        $bensin         = $bensin->find($id);

        if($bensin->status_id == 1)
        {
            $data_status['order_id']        = $id;
            $data_status['models_id']       = 3;
            $data_status['status_id']       = 2;
            StatusHistories::create($data_status);
            $bensin->status_id              = 2;
            $bensin->save();
            
        } else {
            if($bensin->status_id == 0) {
                return response()->json(['message' => 'Peminjaman belum dapat diproses!'], 404);
            } else {
                return response()->json(['message' => 'Peminjaman telah diproses!'], 404);
            }
        }

        $response = fractal()
                        ->item($bensin)
                        ->transformWith(new BensinTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function reject(Bensin $bensin, Request $request)
    {
        if($bensin->status_id == 1)
        {
            $data_status['order_id']        = $bensin->id;
            $data_status['models_id']       = 3;
            $data_status['status_id']       = 3;
            $data_status['alasan']          = $request->get('alasan', null);
            StatusHistories::create($data_status);
            $bensin->status_id         = 3;
            $bensin->save();
        } else {
            if($bensin->status_id == 0) {
                return response()->json(['message' => 'Peminjaman belum dapat diproses!'], 404);
            } else {
                return response()->json(['message' => 'Peminjaman telah diproses!'], 404);
            }
        }

        $response = fractal()
                        ->item($bensin)
                        ->transformWith(new BensinTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function shows(Bensin $bensin, $id)
    {
        $bensin         = $bensin->find($id);

        $response = fractal()
                        ->item($bensin)
                        ->transformWith(new BensinTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function edits(Bensin $bensin, Request $request)
    {
        $bensin->waktu              = $request->get('waktu', $bensin->waktu);
        $bensin->jabatan            = $request->get('jabatan', $bensin->jabatan);
        $bensin->total_penggunaan   = $request->get('total_penggunaan', $bensin->total_penggunaan);
        
        if ($request->get('kwitansi') != NULL)
        {
            $get_file                   = $request->kwitansi;
            $ext                        = $file->getClientOriginalExtension();
            $new_file                   = "kwitansi".rand(100000,1001238912).".".$ext;
            Storage::disk('kwitansi')->put($new_file, File::get($get_file));
            $bensin->kwitansi           = $request->get('kwitansi', $bensin->kwitansi);
        }
        
        $bensin->save();

        $response = fractal()
                        ->item($bensin)
                        ->transformWith(new BensinTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function delete(Bensin $bensin)
    {
        $bensin->delete();

        return response()->json([
            'message'   => 'Deleted!'
        ]);
    }

    public function get_file_kwitansi(Bensin $bensin, $id)
    {
        $bensin         = $bensin->find($id);
        $filename       = $bensin->kwitansi;
        
        // $file           = Storage::disk('lampiran')->get($filename);
        // return (new Response($file, 200))
        // ->header('Content-Type', $pinjamruangs->mime);

		return response()->download(storage_path('app/public/kwitansi/'.$filename));
    }
}
