<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use App\Penggandaan;
// use App\PerbaikanBarang;
use App\PinjamBarang;
use App\PinjamRuangan;
use App\PinjamKendaraan;
use App\SaranAduan;
use App\Barang;
use App\Satker;
use App\Bensin;
use App\Runtext;
use App\User;
use App\Dinas;
use Auth;
use Validator;
use Mail;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Notifications\Notify;
use App\Events\SocketEvent;
use Spatie\Permission\Models\Role;

class PermohonanController extends Controller
{
    public function index()
    {
        $penggandaan                    = Penggandaan::where('user_id', Auth::user()->id)->groupBy('order_id')->get();
        $dinas                          = Dinas::where('user_id', Auth::user()->id)->groupBy('order_id')->get();
        $pinjam_barang                  = PinjamBarang::where('user_id', Auth::user()->id)->groupBy('order_id')->get();
        $pinjam_ruangan                 = PinjamRuangan::where('user_id', Auth::user()->id)->get();
        $pinjam_kendaraan               = PinjamKendaraan::where('user_id', Auth::user()->id)->get();
        $bensin                         = Bensin::where('user_id', Auth::user()->id)->get();
        $saran_aduan                    = SaranAduan::where('user_id', Auth::user()->id)->get();
        $runtext                        = Runtext::all();
        
        $permohonan = collect($penggandaan)->merge($dinas)->merge($pinjam_barang)->merge($pinjam_kendaraan)->merge($pinjam_ruangan)->merge($bensin)->merge($saran_aduan)->sortByDesc('created_at');

        return view('permohonan.index', ['permohonan' => $permohonan, 'runtext' => $runtext]);
    }

    public function kelola()
    {
        $user                           = Auth::user();
        $penggandaan                    = Penggandaan::groupBy('order_id')->get();
        $dinas                          = Dinas::all();
        $pinjam_barang                  = PinjamBarang::groupBy('order_id')->get();
        $pinjam_ruangan                 = PinjamRuangan::all();
        $pinjam_kendaraan               = PinjamKendaraan::all();
        $bensin                         = Bensin::all();
        $saran_aduan                    = SaranAduan::all();
        $runtext                        = Runtext::all();
        
        if($user->hasRole('Administrator')){
            $permohonan = collect($penggandaan)->merge($dinas)->merge($pinjam_barang)->merge($pinjam_kendaraan)->merge($pinjam_ruangan)->merge($bensin)->merge($saran_aduan)->sortByDesc('created_at');
        } else {
            if($user->hasRole('Pengelola Urdal')){
                $permohonan = collect($dinas)->merge($pinjam_barang)->merge($pinjam_ruangan)->merge($bensin)->merge($saran_aduan)->sortByDesc('created_at');
            } else {
                if($user->hasRole('Pengelola Kendaraan')){
                    $permohonan = collect($pinjam_kendaraan)->merge($bensin)->merge($saran_aduan)->sortByDesc('created_at');
                } else {
                    $permohonan = collect($penggandaan)->merge($saran_aduan)->sortByDesc('created_at');
                }
            }
        }

        return view('permohonan.kelola', ['permohonan' => $permohonan, 'runtext' => $runtext]);
    }

    public function setuju($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $substr                         = substr($order_id, strpos($order_id,"SIUD.")+strlen("SIUD."),strlen($order_id)); 
        $jenis                          = substr($substr,0,strpos($substr,"/"));
        switch($jenis){
            case(1) :
                $pinjam_ruangan = PinjamRuangan::where('order_id', $order_id)->first();
                $pinjam_ruangan->status_id = "2";
                $pinjam_ruangan->alasan = null;
                $pinjam_ruangan->save();
                $user_id        = $pinjam_ruangan->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'ruangan', 'setuju'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $pinjam_ruangan], function ($message) use($pinjam_ruangan)
                {
                    $message->subject(strtoupper($pinjam_ruangan->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($pinjam_ruangan->user->email);
                });
                break;
            case(2) :
                $pinjam_kendaraan = PinjamKendaraan::where('order_id', $order_id)->first();
                $pinjam_kendaraan->status_id = "2";
                $pinjam_kendaraan->alasan = null;
                $pinjam_kendaraan->save();
                $user_id        = $pinjam_kendaraan->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'kendaraan', 'setuju'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $pinjam_kendaraan], function ($message) use($pinjam_kendaraan)
                {
                    $message->subject(strtoupper($pinjam_kendaraan->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($pinjam_kendaraan->user->email);
                });
                break;
            case(3) :
                $dinas          = Dinas::where('order_id', $order_id)->first();
                $dinas->status_id = "2";
                $dinas->alasan  = null;
                $dinas->save();
                $user_id        = $dinas->first()->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'dinas', 'setuju'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $dinas], function ($message) use($dinas)
                {
                    $message->subject(strtoupper($dinas->first()->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($dinas->first()->user->email);
                });
                break;
            case(4) :
                $pinjam_barang = PinjamBarang::where('order_id', $order_id)->get();
                foreach($pinjam_barang as $p){    
                    $p->status_id = "2";
                    $p->alasan = null;
                    $p->save();
                }
                $user_id        = $pinjam_barang->first()->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'peminjaman', 'setuju'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $pinjam_barang], function ($message) use($pinjam_barang)
                {
                    $message->subject(strtoupper($pinjam_barang->first()->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($pinjam_barang->first()->user->email);
                });
                break;
            case(5) :
                $penggandaan = Penggandaan::where('order_id', $order_id)->get();
                foreach($penggandaan as $p){    
                    $p->status_id = "2";
                    $p->alasan = null;
                    $p->save();
                }
                $user_id        = $penggandaan->first()->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'penggandaan', 'setuju'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $penggandaan], function ($message) use($penggandaan)
                {
                    $message->subject(strtoupper($penggandaan->first()->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($penggandaan->first()->user->email);
                });
                break;
            case(6) :
                $bensin = Bensin::where('order_id', $order_id)->first();
                $bensin->status_id = "2";
                $bensin->alasan = null;
                $bensin->save();
                $user_id        = $bensin->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'bensin', 'setuju'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $bensin], function ($message) use($bensin)
                {
                    $message->subject(strtoupper($bensin->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($bensin->user->email);
                });
                break;
            case(7) :
                $saran_aduan = SaranAduan::where('order_id', $order_id)->first();
                $saran_aduan->status_id = "2";
                $saran_aduan->alasan = null;
                $saran_aduan->save();
                $user_id     = $saran_aduan->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'saran / aduan', 'setuju'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $saran_aduan], function ($message) use($saran_aduan)
                {
                    $message->subject(strtoupper($saran_aduan->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($saran_aduan->user->email);
                });
                break;
        }
        return redirect()->back()->with('success', 'Berhasil mengubah status permohonan.');
    }

    public function tolak($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $runtext                        = Runtext::all();

        return view('permohonan.tolak', ['order_id' => $order_id, 'runtext' => $runtext]);
    }

    public function tolak_store($order_id, Request $request)
    {
        Validator::make($request->all(), [
            'alasan' => 'required',
        ], [
            'required' => 'Kolom :attribute harus diisi.',
        ])->validate();
        // simpan alasan
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $substr                         = substr($order_id, strpos($order_id,"SIUD.")+strlen("SIUD."),strlen($order_id)); 
        $jenis                          = substr($substr,0,strpos($substr,"/"));
        $delete_dot_order_id            = str_replace(".", "", $order_id); 
        $replace_slash_order_id         = str_replace("/", "-", $delete_dot_order_id); 
        $encrypt_order_id               = Crypt::encryptString($replace_slash_order_id);
        switch($jenis){
            case(1) :
                $pinjam_ruangan = PinjamRuangan::where('order_id', $order_id)->first();
                $pinjam_ruangan->status_id = "3";
                $pinjam_ruangan->alasan = $request->get('alasan');
                $pinjam_ruangan->save();
                $user_id        = $pinjam_ruangan->user_id;
                //read-notif
                foreach(User::find(Auth::user())->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'ruangan', 'tolak'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $pinjam_ruangan], function ($message) use($pinjam_ruangan)
                {
                    $message->subject(strtoupper($pinjam_ruangan->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($pinjam_ruangan->user->email);
                });
                return redirect()->route('pinjam_ruangan.kelola', ['order_id' => $encrypt_order_id])->with('success', 'Berhasil mengubah status permohonan.');
                break;
            case(2) :
                $pinjam_kendaraan = PinjamKendaraan::where('order_id', $order_id)->first();
                $pinjam_kendaraan->status_id = "3";
                $pinjam_kendaraan->alasan = $request->get('alasan');
                $pinjam_kendaraan->save();
                $user_id        = $pinjam_kendaraan->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'kendaraan', 'tolak'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $pinjam_kendaraan], function ($message) use($pinjam_kendaraan)
                {
                    $message->subject(strtoupper($pinjam_kendaraan->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($pinjam_kendaraan->user->email);
                });
                return redirect()->route('pinjam_kendaraan.kelola', ['order_id' => $encrypt_order_id])->with('success', 'Berhasil mengubah status permohonan.');
                break;
            case(3) :
                $dinas = Dinas::where('order_id', $order_id)->first();
                $dinas->status_id   = "3";
                $dinas->alasan      = $request->get('alasan');
                $dinas->save();
                $user_id            = $dinas->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'dinas', 'tolak'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $dinas], function ($message) use($dinas)
                {
                    $message->subject(strtoupper($dinas->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($dinas->user->email);
                });
                return redirect()->route('dinas.kelola', ['order_id' => $encrypt_order_id])->with('success', 'Berhasil mengubah status permohonan.');
                break;
            case(4) :
                $pinjam_barang = PinjamBarang::where('order_id', $order_id)->get();
                foreach($pinjam_barang as $p){
                    $p->status_id = "3";
                    $p->alasan = $request->get('alasan');
                    $p->save();
                }
                $user_id        = $pinjam_barang->first()->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'peminjaman', 'tolak'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $pinjam_barang], function ($message) use($pinjam_barang)
                {
                    $message->subject(strtoupper($pinjam_barang->first()->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($pinjam_barang->first()->user->email);
                });
                return redirect()->route('pinjam_barang.kelola', ['order_id' => $encrypt_order_id])->with('success', 'Berhasil mengubah status permohonan.');
                break;
            case(5) :
                $penggandaan = Penggandaan::where('order_id', $order_id)->get();
                foreach($penggandaan as $p){
                    $p->status_id = "3";
                    $p->alasan = $request->get('alasan');
                    $p->save();
                }
                $user_id        = $penggandaan->first()->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'penggandaan', 'tolak'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $penggandaan], function ($message) use($penggandaan)
                {
                    $message->subject(strtoupper($penggandaan->first()->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($penggandaan->first()->user->email);
                });
                return redirect()->route('penggandaan.kelola', ['order_id' => $encrypt_order_id])->with('success', 'Berhasil mengubah status permohonan.');
                break;
            case(6) :
                $bensin = Bensin::where('order_id', $order_id)->first();
                $bensin->status_id = "3";
                $bensin->alasan = $request->get('alasan');
                $bensin->save();
                $user_id        = $bensin->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'bensin', 'tolak'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $bensin], function ($message) use($bensin)
                {
                    $message->subject(strtoupper($bensin->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($bensin->user->email);
                });
                return redirect()->route('bensin.kelola', ['order_id' => $encrypt_order_id])->with('success', 'Berhasil mengubah status permohonan.');
                break;
            case(7) :
                $saran_aduan = SaranAduan::where('order_id', $order_id)->first();
                $saran_aduan->status_id = "3";
                $saran_aduan->alasan = $request->get('alasan');
                $saran_aduan->save();
                $user_id        = $saran_aduan->user_id;
                //read-notif
                foreach(Auth::user()->unreadnotifications as $n){
                    if($n->data['order_id'] == $decrypt_order_id){
                        $n->markasread();
                    };
                }
                //notify
                User::find($user_id)->notify(new Notify($decrypt_order_id, 'saran / aduan', 'tolak'));
                event(new SocketEvent($user_id));
                //send email
                Mail::send('email', ['permohonan' => $saran_aduan], function ($message) use($saran_aduan)
                {
                    $message->subject(strtoupper($saran_aduan->status->status));
                    $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                    $message->to($saran_aduan->user->email);
                });
                return redirect()->route('saranaduan.kelola', ['order_id' => $encrypt_order_id])->with('success', 'Berhasil mengubah status permohonan.');
                break;
        }
    }
}
