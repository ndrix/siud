<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests\DinasRequest;
use App\Notifications\Notify;
use App\Events\SocketEvent;
use Validator;
use Auth;
use Mail;
use App\Dinas;
use App\Runtext;
use App\User;
use Carbon\Carbon;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;

class DinasController extends Controller
{
    public function create()
    {
        $runtext                        = Runtext::all();

        return view('dinas.create', ['runtext' => $runtext]);
    }

    public function store(DinasRequest $request)
    {
        $bulan_romawi                   = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
        $angka_bulan                    = date('n', strtotime(Carbon::now()));
        $tahun                          = date('Y', strtotime(Carbon::now()));
        $order_id_sebelumnya            = Dinas::all()->sortByDesc('id')->first()['order_id'];
        // generate order_id
        if($order_id_sebelumnya != null){
            $substr                     = substr($order_id_sebelumnya, strpos($order_id_sebelumnya,"SIUD.3/")+strlen("SIUD.3/"),strlen($order_id_sebelumnya));
            $urutan_sebelumnya          = substr($substr,0,strpos($substr,"/"));
            $urutan                     = $urutan_sebelumnya+1;
            $order_id                   = "SIUD.3/".$urutan."/".$bulan_romawi[$angka_bulan]."/".$tahun; 
        } else{
            $order_id                   = "SIUD.3/1/".$bulan_romawi[$angka_bulan]."/".$tahun;
        }
        //validate nodin
        Validator::make($request->all(), [
            'nodin' => 'required',
        ], [
            'required' => 'Kolom :attribute harus diisi.',
        ])->validate();
        //file nodin
        $replace_slash_order_id     = str_replace("/", "-", $order_id);
        $order_id_dash              = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
        $get_nodin                  = $request->nodin;
        $ext                        = $get_nodin->getClientOriginalExtension();
        $rename_nodin               = "NODIN_".$order_id_dash.".".$ext;
        //jika tidak ada duplikasi
        $data['order_id']           = $order_id;
        $data['user_id']            = Auth::user()->id;
        $data['kegiatan']           = $request->get('kegiatan');
        $data['lokasi']             = $request->get('lokasi');
        $nama                       = null;
        $ktp                        = null;
        foreach($request->get('peserta') as $index=>$n)
        {
            if($index == 0)
            {
                $nama               = $n['nama'];
                $ktp                = $n['ktp'];
            } else 
            {
                $nama               = $nama.",".$n['nama'];
                $ktp                = $ktp.",".$n['ktp'];
            }
        }
        $data['peserta']            = $nama;
        $data['ktp']                = $ktp;
        $data['status_id']          = "1";
        $data['nodin']              = $rename_nodin;
        Dinas::create($data);
        //simpan file nodin
        Storage::disk('nodin')->put($rename_nodin, File::get($get_nodin));
        //notify
        User::find(2)->notify(new Notify($order_id_dash, 'dinas', ''));
        event(new SocketEvent('2'));
        //send email
        $dinas                      = Dinas::where('order_id', $order_id)->get();
        Mail::send('email', ['permohonan' => $dinas], function ($message) use($dinas)
        {
            $message->subject(strtoupper($dinas->first()->status->status));
            $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
            $message->to($dinas->first()->user->email);
        });

        return back()->with('success', 'Permohonan Anda akan diproses.');
    }

    public function show($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $dinas                          = Dinas::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('dinas.show', ['dinas' => $dinas, 'kelola' => 'no', 'runtext' => $runtext]);
    }

    public function kelola($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $dinas                          = Dinas::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('dinas.show', ['dinas' => $dinas, 'kelola' => 'yes', 'runtext' => $runtext]);
    }

    public function download_nodin($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $nodin                          = Dinas::where('order_id', $order_id)->first()['nodin'];

        return Storage::disk('nodin')->download($nodin);
    }

    public function edit($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $dinas                          = Dinas::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('dinas.edit', ['dinas' => $dinas, 'runtext' => $runtext]);
    }

    public function update($order_id, DinasRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $dinas                          = Dinas::where('order_id', $order_id)->first();
        $delete_dot_order_id            = str_replace(".", "", $order_id); 
        $replace_slash_order_id         = str_replace("/", "-", $delete_dot_order_id); 
        $encrypt_order_id               = Crypt::encryptString($replace_slash_order_id);

        if($dinas->status_id != 2 OR $dinas->status_id != 3){
            // jika statusnya belum diverifikasi admin maka permohonan bisa diubah
            if(Dinas::where('order_id', $order_id)->first()->nodin != null){
                //delete file nodin
                Storage::disk('nodin')->delete(Dinas::where('order_id', $order_id)->first()['nodin']);
            }
            $delete_entry               = Dinas::where('order_id', $order_id)->delete();
            //file nodin baru
            if($request->nodin != null){
                $replace_slash_order_id = str_replace("/", "-", $order_id);
                $order_id_dash          = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
                $get_nodin              = $request->nodin;
                $ext                    = $get_nodin->getClientOriginalExtension();
                $rename_nodin           = "NODIN_".$order_id_dash.".".$ext;
            }
            //update entry
            $data['order_id']       = $order_id;
            $data['user_id']        = Auth::user()->id;
            $data['kegiatan']       = $request->get('kegiatan');
            $data['lokasi']         = $request->get('lokasi');
            $data['status_id']      = $dinas->status_id;
            $nama                   = null;
            $ktp                    = null;
            foreach($request->get('peserta') as $index=>$n)
            {
                if($index == 0)
                {
                    $nama           = $n['nama'];
                    $ktp            = $n['ktp'];
                } else 
                {
                    $nama           = $nama.",".$n['nama'];
                    $ktp            = $ktp.",".$n['ktp'];
                }
            }
            $data['peserta']        = $nama;
            $data['ktp']            = $ktp;
            if($request->nodin != null){
                $data['nodin']      = $rename_nodin;
            }
            Dinas::create($data);
            //simpan file nodin
            if($request->nodin != null){
                Storage::disk('nodin')->put($rename_nodin, File::get($get_nodin));
            }
            return redirect()->route('dinas.show', ['order_id' => $encrypt_order_id])->with('success', 'Permohonan berhasil diubah.');
        }
        //jika sudah diverifikasi admin maka tidak bisa diubah
        else {
            return redirect()->route('dinas.show', ['order_id' => $encrypt_order_id])->with('error', 'Permohonan tidak dapat diubah.');
        }
    }

    public function delete($order_id)
    {
        $decrypt_order_id           = Crypt::decryptString($order_id);
        $replace_dash_order_id      = str_replace("-", "/", $decrypt_order_id);
        $order_id                   = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $dinas                      = Dinas::where('order_id', $order_id)->first();
        // validasi status masih belum diverifikasi admin
        if($dinas->first()['status_id'] != 2 OR $dinas->first()['status_id'] != 3){
            // jika statusnya belum diverifikasi admin maka permohonan bisa dihapus
            Storage::disk('nodin')->delete($dinas->first()['nodin']);
            $dinas->delete();
            //read-notif
            foreach(Auth::user()->unreadnotifications as $n){
                if($n->data['order_id'] == $decrypt_order_id){
                    $n->markasread();
                };
            }
            return redirect()->back()->with('success', 'Berhasil menghapus permohonan.');
        }
        else {
            // jika ruangan tidak tersedia maka permohonan tidak bisa dihapus
            return redirect()->back()->with('error', 'Tidak dapat menghapus permohonan.');
        }
    }
}
