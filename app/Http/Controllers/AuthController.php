<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\User;
use App\Transformers\UserTransformer;
use Auth;
use Faker\Test\Provider\UserAgentTest;
use Adldap\Laravel\Facades\Adldap;
use Symfony\Component\HttpFoundation\Request;

class AuthController extends Controller
{
    public function login(Request $request, User $user)
    {
        if (!Auth::attempt(['username' => $request->username, 'password' => $request->password])){
            return response()->json(['error' => 'Your credential is wrong'], 401);
        }
        $user = $user->find(Auth::user()->id);
        return fractal()
            ->item($user)
            ->transformWith(new UserTransformer)
            ->addMeta([
                'api_token' => $user->api_token
            ])
            ->toArray();
    }
}