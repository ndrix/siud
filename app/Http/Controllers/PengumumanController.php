<?php

namespace App\Http\Controllers;

use App\Pengumuman;
use App\Runtext;
use Illuminate\Http\Request;
use App\Http\Requests\PengumumanRequest;

class PengumumanController extends Controller
{
    public function edit()
    {
        $pengumuman                     = Pengumuman::first();
        $runtext                        = Runtext::all();
        
        return view('pengumuman.edit', ['pengumuman' => $pengumuman, 'runtext' => $runtext]);
    }

    public function update(PengumumanRequest $request)
    {
        $pengumuman                     = Pengumuman::first();
        $pengumuman->pengumuman         = $request->get('pengumuman');
        $pengumuman->save();

        return redirect()->back()->with('success', 'Berhasil mengubah pengumuman.');
    }
}
