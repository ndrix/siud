<?php

namespace App\Http\Controllers;

use Auth;
use Mail;
use Validator;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Crypt;
use Illuminate\Support\Facades\Storage;
use Illuminate\Support\Facades\File;
use App\Kendaraan;
use App\Satker;
use App\PinjamKendaraan;
use App\StatusHistories;
use App\Runtext;
use App\User;
use App\Http\Requests\PinjamKendaraanRequest;
use App\Http\Requests\UploadNodinRequest;
use App\Notifications\Notify;
use App\Events\SocketEvent;
use App\Transformers\PinjamKendaraanTransformer;

class PinjamKendaraanController extends Controller
{
    public function create()
    {
        $kendaraan                  = Kendaraan::all();
        $satker                     = Satker::all();
        $runtext                    = Runtext::all();

        return view('pinjam_kendaraan.create', ['kendaraan' => $kendaraan, 'satker' => $satker, 'runtext' => $runtext]);
    }

    public function store(PinjamKendaraanRequest $request)
    {
        $bulan_romawi               = array(1 => "I", "II", "III", "IV", "V", "VI", "VII", "VIII", "IX", "X", "XI", "XII");
        $angka_bulan                = date('n', strtotime(Carbon::now()));
        $tahun                      = date('Y', strtotime(Carbon::now()));
        $waktu_mulai                = $request->get('waktu_mulai');
        $waktu_selesai              = $request->get('waktu_selesai');

        if($request->get('ketersediaan') != null){
            $satker                 = Satker::all();
            $kendaraan              = Kendaraan::all();
            $kendaraan_tersedia     = array();
            foreach($kendaraan as $n){
                $cek                = PinjamKendaraan::
                                        where('kendaraan_id',$n->id)
                                        ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                            $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                        })
                                        ->where('status_id',2)
                                        ->first();
                if($cek == null){
                    array_push($kendaraan_tersedia, $n->nama.' - '.$n->nomor_polisi);
                }
            }
            if($kendaraan_tersedia != null){
                return back()->withInput()->with('kendaraan_tersedia', $kendaraan_tersedia);
            } else {
                return back()->withInput()->with('tidak_tersedia', 'Kendaraan Tidak Tersedia');
            }
        } else {
            $order_id_sebelumnya            = PinjamKendaraan::all()->sortByDesc('id')->first()['order_id'];
            if($order_id_sebelumnya != null){
                $substr                     = substr($order_id_sebelumnya, strpos($order_id_sebelumnya,"SIUD.2/")+strlen("SIUD.2/"),strlen($order_id_sebelumnya));
                $urutan_sebelumnya          = substr($substr,0,strpos($substr,"/"));
                $urutan                     = $urutan_sebelumnya+1;
                $order_id                   = "SIUD.2/".$urutan."/".$bulan_romawi[$angka_bulan]."/".$tahun; 
            } else{
                $order_id                   = "SIUD.2/1/".$bulan_romawi[$angka_bulan]."/".$tahun;
            }
            $data['order_id']           = $order_id;
            $data['user_id']            = Auth::user()->id;
            $data['satker_id']          = $request->get('satker_id');
            $data['kendaraan_id']       = $request->get('kendaraan_id');
            $data['kegiatan']           = $request->get('kegiatan');
            $data['tujuan']             = $request->get('tujuan');
            $data['jml_penumpang']      = $request->get('jml_penumpang');
            $data['waktu_mulai']        = $waktu_mulai;
            $data['waktu_selesai']      = $waktu_selesai;
            $data['dukungan_pengemudi'] = $request->get('dukungan_pengemudi');
            $data['status_id']          = "1";
            //validate nodin
            Validator::make($request->all(), [
                'nodin' => 'required',
            ], [
                'required' => 'Kolom :attribute harus diisi.',
            ])->validate();
            //file nodin
            $replace_slash_order_id     = str_replace("/", "-", $order_id);
            $order_id_dash              = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
            $get_nodin                  = $request->nodin;
            $ext                        = $get_nodin->getClientOriginalExtension();
            $rename_nodin               = "NODIN_".$order_id_dash.".".$ext;
            $data['nodin']              = $rename_nodin;
            //cek ketersediaan
            $cek_ketersediaan           = PinjamKendaraan::
                                            where('kendaraan_id',$request->get('kendaraan_id'))
                                            ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                                $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                    ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                            })
                                            ->where('status_id', 3)
                                            ->first();
            if($cek_ketersediaan == null){
                //cek duplikasi
                $cek_duplikasi          = PinjamKendaraan::
                                            where('kendaraan_id',$request->get('kendaraan_id'))
                                            ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                                $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                    ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                            })
                                            ->first();
                if($cek_duplikasi == null){
                    //jika tidak ada duplikasi
                    PinjamKendaraan::create($data);
                    Storage::disk('nodin')->put($rename_nodin, File::get($get_nodin));
                    //notify
                    User::find(3)->notify(new Notify($order_id_dash, 'kendaraan', ''));
                    event(new SocketEvent('3'));
                    //send email
                    $pinjam_kendaraan   = PinjamKendaraan::where('order_id', $order_id)->first();
                    Mail::send('email', ['permohonan' => $pinjam_kendaraan], function ($message) use($pinjam_kendaraan)
                    {
                        $message->subject(strtoupper($pinjam_kendaraan->status->status));
                        $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                        $message->to($pinjam_kendaraan->user->email);
                    });
                    return redirect()->back()->with('success', 'Permohonan Anda akan diproses, sebelumnya lengkapi dulu Nota Dinas Permohonan di halaman Permohonan Saya.');
                } else {
                    return redirect()->back()->with('error', 'Anda telah mengajukan permohonan kendaraan yang sama di waktu tersebut.');
                }
            } else {
                return redirect()->back()->with('error', 'Permohonan Anda tidak dapat diproses karena kendaraan tidak tersedia.');
            }
        }
    }

    public function show($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $pinjam_kendaraan               = PinjamKendaraan::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('pinjam_kendaraan.show', ['pinjam_kendaraan' => $pinjam_kendaraan, 'kelola' => 'no', 'runtext' => $runtext]);
    }

    public function kelola($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $pinjam_kendaraan               = PinjamKendaraan::where('order_id', $order_id)->get();
        $runtext                        = Runtext::all();

        return view('pinjam_kendaraan.show', ['pinjam_kendaraan' => $pinjam_kendaraan, 'kelola' => 'yes', 'runtext' => $runtext]);
    }

    public function upload_nodin($order_id, UploadNodinRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $status_id                      = PinjamKendaraan::where('order_id', $order_id)->first()['status_id'];
        //validasi apakah status masih belum diverifikasi
        if($status_id != 2 OR $status_id != 3){
            //operasi file
            $get_file                   = $request->nodin;
            $ext                        = $get_file->getClientOriginalExtension();
            $rename_file                = "Nodin_".$decrypt_order_id.".".$ext;
            //simpan update
            $pinjam_kendaraan           = PinjamKendaraan::where('order_id', $order_id)->first();
            $pinjam_kendaraan->nodin    = $rename_file;
            $pinjam_kendaraan->status_id= "2";
            $pinjam_kendaraan->save();
            Storage::disk('nodin')->delete($rename_file);
            Storage::disk('nodin')->put($rename_file, File::get($get_file));
            //send email
            Mail::send('email', ['permohonan' => $pinjam_kendaraan], function ($message) use($pinjam_kendaraan)
            {
                $message->subject(strtoupper($pinjam_kendaraan->status->status));
                $message->from('hendrik.maulana@bssn.go.id', 'SIUD 2.0');
                $message->to($pinjam_kendaraan->user->email);
            });

            return redirect()->back()->with('success', 'Berhasil mengunggah file nota dinas, silakan menunggu Admin untuk memverifikasi.');
        }
        //jika sudah diverifikasi
        else {
            return redirect()->back()->with('error', 'Tidak dapat mengunggah file nota dinas.');
        }
    }

    public function download_nodin($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $nodin                          = PinjamKendaraan::where('order_id', $order_id)->first()['nodin'];

        return Storage::disk('nodin')->download($nodin);
    }

    public function edit($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $pinjam_kendaraan               = PinjamKendaraan::where('order_id', $order_id)->first();
        $kendaraan                      = Kendaraan::all();
        $satker                         = Satker::all();
        $runtext                        = Runtext::all();

        return view('pinjam_kendaraan.edit', ['pinjam_kendaraan' => $pinjam_kendaraan, 'kendaraan' => $kendaraan, 'satker' => $satker, 'runtext' => $runtext]);
    }

    public function update($order_id, PinjamKendaraanRequest $request)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $pinjam_kendaraan               = PinjamKendaraan::where('order_id', $order_id)->first();
        $waktu_mulai                    = $request->get('waktu_mulai');
        $waktu_selesai                  = $request->get('waktu_selesai');
        $kendaraan                      = Kendaraan::all();
        $delete_dot_order_id            = str_replace(".", "", $order_id); 
        $replace_slash_order_id         = str_replace("/", "-", $delete_dot_order_id); 
        $encrypt_order_id               = Crypt::encryptString($replace_slash_order_id);
        // validasi apakah status permohonan masih belum diverifikasi admin
        if($pinjam_kendaraan->status_id != 2 OR $pinjam_kendaraan->status_id != 3){
            // fitur cek ketersediaan
            if($request->get('ketersediaan') != null){
                $satker                 = Satker::all();
                $kendaraan              = Kendaraan::all();
                $kendaraan_tersedia     = array();
                foreach($kendaraan as $n){
                    $cek                = PinjamKendaraan::
                                            where('kendaraan_id',$n->id)
                                            ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                                $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                    ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                            })
                                            ->where('status_id', 3)
                                            ->first();
                    if($cek == null){
                        array_push($kendaraan_tersedia, $n->nama.' - '.$n->nomor_polisi);
                    }
                }
                if($kendaraan_tersedia != null){
                    return back()->withInput()->with('kendaraan_tersedia', $kendaraan_tersedia);
                } else {
                    return back()->withInput()->with('tidak_tersedia', 'Kendaraan Tidak Tersedia');
                }
            }
            // jika statusnya belum diverifikasi admin maka permohonan bisa diubah
            $pinjam_kendaraan->waktu_mulai          = $request->get('waktu_mulai');
            $pinjam_kendaraan->waktu_selesai        = $request->get('waktu_selesai');
            $pinjam_kendaraan->kendaraan_id         = $request->get('kendaraan_id');
            $pinjam_kendaraan->satker_id            = $request->get('satker_id');
            $pinjam_kendaraan->kegiatan             = $request->get('kegiatan');
            $pinjam_kendaraan->tujuan               = $request->get('tujuan');
            $pinjam_kendaraan->dukungan_pengemudi   = $request->get('dukungan_pengemudi');
            $pinjam_kendaraan->jml_penumpang        = $request->get('jml_penumpang');
            // file nodin
            if($request->nodin != null){
                $replace_slash_order_id     = str_replace("/", "-", $order_id);
                $order_id_dash              = str_replace("SIUD.", "SIUD", $replace_slash_order_id);
                $get_nodin                  = $request->nodin;
                $ext                        = $get_nodin->getClientOriginalExtension();
                $rename_nodin               = "NODIN_".$order_id_dash.".".$ext;
                $pinjam_kendaraan->nodin    = $rename_nodin;
            }
            // validasi apakah kendaraan tersedia
            $cek_ketersediaan           = PinjamKendaraan::
                                            where('kendaraan_id',$request->get('kendaraan_id'))
                                            ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                                $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                    ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                            })
                                            ->where('order_id', '!=', $order_id)
                                            ->where('status_id',3)
                                            ->first();
            if($cek_ketersediaan == null){
                // jika tersedia, maka lakukan validasi apakah ada permohonan yang sama
                $cek_duplikasi          = PinjamKendaraan::
                                            where('kendaraan_id',$request->get('kendaraan_id'))
                                            ->where(function($query)use($waktu_mulai,$waktu_selesai){
                                                $query->whereBetween('waktu_mulai', [$waktu_mulai,$waktu_selesai])
                                                    ->orWhereBetween('waktu_selesai', [$waktu_mulai,$waktu_selesai]);
                                            })
                                            ->where('user_id', Auth::user()->id)
                                            ->where('order_id', '!=', $order_id)
                                            ->first();
                if($cek_duplikasi == null){
                    // jika tidak ada permohonan yang sama maka bisa disimpan
                    $pinjam_kendaraan->save();
                    if($request->nodin != null){
                        Storage::disk('nodin')->delete(PinjamKendaraan::where('order_id', $order_id)->first()['nodin']);
                        Storage::disk('nodin')->put($rename_nodin, File::get($get_nodin));
                    }
                    return redirect()->route('pinjam_kendaraan.show', ['order_id' => $encrypt_order_id])->with('success', 'Permohonan berhasil diubah.');
                } else{
                    // jika ada permohonan yang sama maka permohonan tidak bisa disimpan
                    return redirect()->route('pinjam_kendaraan.show', ['order_id' => $encrypt_order_id])->with('error', 'Anda telah mengajukan permohonan kendaraan yang sama di waktu tersebut.');
                }
            } else {
                // jika kendaraan tidak tersedia maka permohonan tidak bisa disimpan
                return redirect()->route('pinjam_kendaraan.show', ['order_id' => $encrypt_order_id])->with('error', 'Permohonan Anda tidak dapat diproses karena kendaraan tidak tersedia.');
            }
        }
    }

    public function destroy($order_id)
    {
        $decrypt_order_id               = Crypt::decryptString($order_id);
        $replace_dash_order_id          = str_replace("-", "/", $decrypt_order_id);
        $order_id                       = str_replace("SIUD", "SIUD.", $replace_dash_order_id);
        $pinjam_kendaraan               = PinjamKendaraan::where('order_id', $order_id)->first();
        // validasi status masih belum diverifikasi admin
        if($pinjam_kendaraan->status_id != 2 OR $pinjam_kendaraan->status_id != 3){
            // jika statusnya belum diverifikasi admin maka permohonan bisa dihapus
            Storage::disk('nodin')->delete($pinjam_kendaraan->nodin);
            $pinjam_kendaraan->delete();
            //read-notif
            foreach(Auth::user()->unreadnotifications as $n){
                if($n->data['order_id'] == $decrypt_order_id){
                    $n->markasread();
                };
            }
            return redirect()->back()->with('success', 'Berhasil menghapus permohonan.');
        }
        else {
            // jika kendaraan tidak tersedia maka permohonan tidak bisa dihapus
            return redirect()->back()->with('error', 'Tidak dapat menghapus permohonan.');
        }
    }




    
    public function add(AddPinjamKendaraan $request, PinjamKendaraan $pinjamkendaraan)
    {
        $pinjamkendaraan = $pinjamkendaraan->create([
            'user_id'           => $request->user_id,
            'kendaraan_id'      => $request->kendaraan_id,
            'kegiatan'          => $request->kegiatan,
            'waktu_mulai'       => $request->waktu_mulai,
            'waktu_selesai'     => $request->waktu_selesai,
            'dukungan_supir'    => $request->dukungan_supir,
            'nodin'             => $request->nodin,
        ]);
        
        $response = fractal()
                        ->item($pinjamkendaraan)
                        ->transformWith(new PinjamKendaraanTransformer)
                        ->toArray();
        return response()->json($response, 301);
    }

    public function lists(PinjamKendaraan $pinjamkendaraan)
    {
        $pinjamkendaraan    = $pinjamkendaraan->all();

        $response = fractal()
                        ->collection($pinjamkendaraan)
                        ->transformWith(new PinjamKendaraanTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function accepted(PinjamKendaraan $pinjamkendaraan)
    {
        $pinjamkendaraan    = $pinjamkendaraan->where('status_id', 2)->get();
        $response = fractal()
                        ->collection($pinjamkendaraan)
                        ->transformWith(new PinjamKendaraanTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function count_accepted(PinjamKendaraan $pinjamkendaraan)
    {
        $pinjamkendaraan    = $pinjamkendaraan->where('status_id', 2)->get();
        $jumlah             = count($pinjamkendaraan);

        return response()->json(['jumlah' => $jumlah], 200);
    }

    public function rejected(PinjamKendaraan $pinjamkendaraan)
    {
        $pinjamkendaraan    = $pinjamkendaraan->where('status_id', 3)->get();

        $response = fractal()
                        ->collection($pinjamkendaraan)
                        ->transformWith(new PinjamKendaraanTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function count_rejected(PinjamKendaraan $pinjamkendaraan)
    {
        $pinjamkendaraan    = $pinjamkendaraan->where('status_id', 3)->get();
        $jumlah             = count($pinjamkendaraan);

        return response()->json(['jumlah' => $jumlah], 200);
    }

    public function accept(PinjamKendaraan $pinjamkendaraan, $id)
    {
        $pinjamkendaraan = $pinjamkendaraan->find($id);

        if($pinjamkendaraan->status_id == 1)
        {
            $data_status['order_id']        = $id;
            $data_status['models_id']       = 2;
            $data_status['status_id']       = 2;
            StatusHistories::create($data_status);
            $pinjamkendaraan->status_id     = 2;
            $pinjamkendaraan->save();
        } else {
            if($pinjamkendaraan->status_id == 0){
                return response()->json(['message' => 'Peminjaman belum dapat diproses!'], 404);
            } else {
                return response()->json(['message' => 'Peminjaman telah diproses!'], 404);
            }
        }

        $response = fractal()
                        ->item($pinjamkendaraan)
                        ->transformWith(new PinjamKendaraanTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function reject(PinjamKendaraan $pinjamkendaraan, Request $request)
    {
        if($pinjamkendaraan->status_id == 1)
        {
            $data_status['order_id']        = $pinjamkendaraan->id;
            $data_status['models_id']       = 2;
            $data_status['status_id']       = 3;
            $data_status['alasan']          = $request->get('alasan', null);
            StatusHistories::create($data_status);
            $pinjamkendaraan->status_id         = 3;
            $pinjamkendaraan->save();
        } else {
            if($pinjamkendaraan->status_id == 0) {
                return response()->json(['message' => 'Peminjaman belum dapat diproses!'], 404);
            } else {
                return response()->json(['message' => 'Peminjaman telah diproses!'], 404);
            }
        }

        $response = fractal()
                        ->item($pinjamkendaraan)
                        ->transformWith(new PinjamKendaraanTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function shows(PinjamKendaraan $pinjamkendaraan, $id)
    {
        $pinjamkendaraans = $pinjamkendaraan->find($id);

        $response = fractal()
                        ->item($pinjamkendaraans)
                        ->transformWith(new PinjamKendaraanTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function edits(PinjamKendaraan $pinjamkendaraan, Request $request)
    {
        $pinjamkendaraan->kendaraan_id      = $request->get('kendaraan_id', $pinjamkendaraan->kendaraan_id);
        $pinjamkendaraan->waktu_mulai       = $request->get('waktu_mulai', $pinjamkendaraan->waktu_mulai);
        $pinjamkendaraan->waktu_selesai     = $request->get('waktu_selesai', $pinjamkendaraan->waktu_selesai);
        $pinjamkendaraan->kegiatan          = $request->get('kegiatan', $pinjamkendaraan->kegiatan);
        $pinjamkendaraan->dukungan_supir    = $request->get('dukungan_supir', $pinjamkendaraan->dukungan_supir);
        $pinjamkendaraan->nodin             = $request->get('nodin', $pinjamkendaraan->nodin);
        
        $pinjamkendaraan->save();

        $response = fractal()
                        ->item($pinjamkendaraan)
                        ->transformWith(new PinjamKendaraanTransformer)
                        ->toArray();
        return response()->json($response, 200);
    }

    public function delete(PinjamKendaraan $pinjamkendaraan)
    {
        $pinjamkendaraan->delete();

        return response()->json([
            'message'   => 'Deleted!'
        ]);
    }
}
