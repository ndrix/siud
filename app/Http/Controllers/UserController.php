<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Crypt;
use Spatie\Permission\Models\Role;
use App\User;
use App\Runtext;

class UserController extends Controller
{
    public function index()
    {
        $user                       = User::all();
        $runtext                    = Runtext::all();
        return view('user.index', ['user' => $user, 'runtext' => $runtext]);
    }

    public function edit($id)
    {
        $decrypt_id                 = Crypt::decryptString($id);
        $user                       = User::where('id', $decrypt_id)->first();
        $runtext                    = Runtext::all();
        return view('user.edit', ['user' => $user, 'runtext' => $runtext]);
    }

    public function update($id, Request $request)
    {
        $user_all                   = User::all();
        $decrypt_id                 = Crypt::decryptString($id);
        $hak_akses                  = $request->hak_akses;
        $user                       = User::where('id', $decrypt_id)->first();
        if($request->password != null){
            $user->password         = bcrypt($request->password);
        }
        $user->name                 = $request->name;
        $user->email                = $request->email;
        $user->save();
        $user_role                  = $user->getRoleNames()->first();
        if($user_role != $hak_akses AND $hak_akses != null AND $user_role != null){
            $user->removeRole($user_role);
            $user->assignRole($hak_akses);
        } else{
            $user->assignRole($hak_akses);
        }
        return redirect()->route('user.index', ['user' => $user_all])->with('success', 'Berhasil mengubah data pengguna.');
    }
}
