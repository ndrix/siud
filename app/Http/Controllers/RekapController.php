<?php

namespace App\Http\Controllers;

use Auth;
use PDF;
use Carbon\Carbon;
use App\Penggandaan;
use App\PerbaikanBarang;
use App\PinjamBarang;
use App\PinjamRuangan;
use App\PinjamKendaraan;
use App\SaranAduan;
use App\Barang;
use App\Satker;
use App\Bensin;
use App\User;
use App\Runtext;
use Illuminate\Http\Request;
use Spatie\Permission\Models\Role;

class RekapController extends Controller
{
    public function index()
    {
        $tahun                          = Carbon::now()->year;
        $penggandaan                    = Penggandaan::whereYear('created_at', $tahun)->groupBy('order_id')->get();
        $pinjam_barang                  = PinjamBarang::whereYear('created_at', $tahun)->groupBy('order_id')->get();
        $perbaikan_barang               = PerbaikanBarang::whereYear('created_at', $tahun)->groupBy('order_id')->get();
        $pinjam_ruangan                 = PinjamRuangan::whereYear('created_at', $tahun)->get();
        $pinjam_kendaraan               = PinjamKendaraan::whereYear('created_at', $tahun)->get();
        $bensin                         = Bensin::whereYear('created_at', $tahun)->get();
        $saran_aduan                    = SaranAduan::whereYear('created_at', $tahun)->get();
        $runtext                        = Runtext::all();

        $total_permohonan               = count(Penggandaan::where('status_id', '!=', 1)->groupBy('order_id')->get()) + 
                                            count(PerbaikanBarang::where('status_id', '!=', 1)->groupBy('order_id')->get()) +
                                            count(PinjamBarang::where('status_id', '!=', 1)->groupBy('order_id')->get()) + 
                                            PinjamRuangan::where('status_id', '!=', 1)->count() + 
                                            PinjamKendaraan::where('status_id', '!=', 1)->count() + 
                                            SaranAduan::where('status_id', '!=', 1)->count();
        $permohonan_disetujui           = count(Penggandaan::where('status_id', 3)->groupBy('order_id')->get()) + 
                                            count(PerbaikanBarang::where('status_id', 3)->groupBy('order_id')->get()) +
                                            count(PinjamBarang::where('status_id', 3)->groupBy('order_id')->get()) + 
                                            PinjamRuangan::where('status_id', 3)->count() + 
                                            PinjamKendaraan::where('status_id', 3)->count() + 
                                            SaranAduan::where('status_id', 3)->count();
        $permohonan_ditolak             = count(Penggandaan::where('status_id', 4)->groupBy('order_id')->get()) + 
                                            count(PerbaikanBarang::where('status_id', 4)->groupBy('order_id')->get()) +
                                            count(PinjamBarang::where('status_id', 4)->groupBy('order_id')->get()) + 
                                            PinjamRuangan::where('status_id', 4)->count() + 
                                            PinjamKendaraan::where('status_id', 4)->count() + 
                                            SaranAduan::where('status_id', 4)->count();

        $permohonan     = collect($penggandaan)->merge($perbaikan_barang)->merge($pinjam_barang)->merge($pinjam_kendaraan)->merge($pinjam_ruangan)->merge($bensin)->merge($saran_aduan)->sortByDesc('created_at');
        // ->groupBy(function($date) {
        //     return Carbon::parse($date->created_at)->format('Y'); // grouping by years
        //     //return Carbon::parse($date->created_at)->format('m'); // grouping by months
        // });

        $pengguna       = $permohonan->groupBy(function($user){
                                return $user->user_id;
                            });
        $total_pengguna = count($pengguna);
        $total_bensin   = 0;
        foreach($bensin as $b){
            $total_bensin += $b->total_penggunaan;
        };
        $list_tahun     = ["2018"];

        return view('rekap.index', ['permohonan' => $permohonan, 'total_permohonan' => $total_permohonan, 'permohonan_disetujui' => $permohonan_disetujui, 'permohonan_ditolak' => $permohonan_ditolak, 'total_pengguna' => $total_pengguna, 'list_tahun' => $list_tahun, 'tahun' => $tahun, 'total_bensin' => $total_bensin, 'runtext' => $runtext]);
    }

    public function filter(Request $request)
    {
        $tahun                          = $request->tahun;
        $penggandaan                    = Penggandaan::whereYear('created_at', $tahun)->groupBy('order_id')->get();
        $pinjam_barang                  = PinjamBarang::whereYear('created_at', $tahun)->groupBy('order_id')->get();
        $perbaikan_barang               = PerbaikanBarang::whereYear('created_at', $tahun)->groupBy('order_id')->get();
        $pinjam_ruangan                 = PinjamRuangan::whereYear('created_at', $tahun)->get();
        $pinjam_kendaraan               = PinjamKendaraan::whereYear('created_at', $tahun)->get();
        $bensin                         = Bensin::whereYear('created_at', $tahun)->get();
        $saran_aduan                    = SaranAduan::whereYear('created_at', $tahun)->get();
        $runtext                        = Runtext::all();

        $total_permohonan               = count(Penggandaan::where('status_id', '!=', 1)->groupBy('order_id')->get()) + 
                                            count(PerbaikanBarang::where('status_id', '!=', 1)->groupBy('order_id')->get()) +
                                            count(PinjamBarang::where('status_id', '!=', 1)->groupBy('order_id')->get()) + 
                                            PinjamRuangan::where('status_id', '!=', 1)->count() + 
                                            PinjamKendaraan::where('status_id', '!=', 1)->count() + 
                                            SaranAduan::where('status_id', '!=', 1)->count();
        $permohonan_disetujui           = count(Penggandaan::where('status_id', 3)->groupBy('order_id')->get()) + 
                                            count(PerbaikanBarang::where('status_id', 3)->groupBy('order_id')->get()) +
                                            count(PinjamBarang::where('status_id', 3)->groupBy('order_id')->get()) + 
                                            PinjamRuangan::where('status_id', 3)->count() + 
                                            PinjamKendaraan::where('status_id', 3)->count() + 
                                            SaranAduan::where('status_id', 3)->count();
        $permohonan_ditolak             = count(Penggandaan::where('status_id', 4)->groupBy('order_id')->get()) + 
                                            count(PerbaikanBarang::where('status_id', 4)->groupBy('order_id')->get()) +
                                            count(PinjamBarang::where('status_id', 4)->groupBy('order_id')->get()) + 
                                            PinjamRuangan::where('status_id', 4)->count() + 
                                            PinjamKendaraan::where('status_id', 4)->count() + 
                                            SaranAduan::where('status_id', 4)->count();

        $permohonan     = collect($penggandaan)->merge($perbaikan_barang)->merge($pinjam_barang)->merge($pinjam_kendaraan)->merge($pinjam_ruangan)->merge($bensin)->merge($saran_aduan)->sortByDesc('created_at');
        // ->groupBy(function($date) {
        //     return Carbon::parse($date->created_at)->format('Y'); // grouping by years
        //     //return Carbon::parse($date->created_at)->format('m'); // grouping by months
        // });

        $pengguna       = $permohonan->groupBy(function($user){
                                return $user->user_id;
                            });
        $total_pengguna = count($pengguna);
        $total_bensin   = 0;
        foreach($bensin as $b){
            $total_bensin += $b->total_penggunaan;
        };
        $list_tahun     = ["2018"];

        return redirect()->route('rekap.index', ['tahun' => $tahun]);
    }

    public function download($tahun)
    {
        $penggandaan                    = Penggandaan::whereYear('created_at', $tahun)->groupBy('order_id')->get();
        $pinjam_barang                  = PinjamBarang::whereYear('created_at', $tahun)->groupBy('order_id')->get();
        $perbaikan_barang               = PerbaikanBarang::whereYear('created_at', $tahun)->groupBy('order_id')->get();
        $pinjam_ruangan                 = PinjamRuangan::whereYear('created_at', $tahun)->get();
        $pinjam_kendaraan               = PinjamKendaraan::whereYear('created_at', $tahun)->get();
        $bensin                         = Bensin::whereYear('created_at', $tahun)->get();
        $saran_aduan                    = SaranAduan::whereYear('created_at', $tahun)->get();
        $total_permohonan               = count(Penggandaan::where('status_id', '!=', 1)->groupBy('order_id')->get()) + 
                                            count(PerbaikanBarang::where('status_id', '!=', 1)->groupBy('order_id')->get()) +
                                            count(PinjamBarang::where('status_id', '!=', 1)->groupBy('order_id')->get()) + 
                                            PinjamRuangan::where('status_id', '!=', 1)->count() + 
                                            PinjamKendaraan::where('status_id', '!=', 1)->count() + 
                                            SaranAduan::where('status_id', '!=', 1)->count();
        $permohonan_disetujui           = count(Penggandaan::where('status_id', 3)->groupBy('order_id')->get()) + 
                                            count(PerbaikanBarang::where('status_id', 3)->groupBy('order_id')->get()) +
                                            count(PinjamBarang::where('status_id', 3)->groupBy('order_id')->get()) + 
                                            PinjamRuangan::where('status_id', 3)->count() + 
                                            PinjamKendaraan::where('status_id', 3)->count() + 
                                            SaranAduan::where('status_id', 3)->count();
        $permohonan_ditolak             = count(Penggandaan::where('status_id', 4)->groupBy('order_id')->get()) + 
                                            count(PerbaikanBarang::where('status_id', 4)->groupBy('order_id')->get()) +
                                            count(PinjamBarang::where('status_id', 4)->groupBy('order_id')->get()) + 
                                            PinjamRuangan::where('status_id', 4)->count() + 
                                            PinjamKendaraan::where('status_id', 4)->count() + 
                                            SaranAduan::where('status_id', 4)->count();
        $permohonan         = collect($penggandaan)->merge($perbaikan_barang)->merge($pinjam_barang)->merge($pinjam_kendaraan)->merge($pinjam_ruangan)->merge($bensin)->merge($saran_aduan)->sortByDesc('created_at');
        $total_permohonan   = count(Penggandaan::where('status_id', '!=', 1)->groupBy('order_id')->get()) + 
                                count(PerbaikanBarang::where('status_id', '!=', 1)->groupBy('order_id')->get()) +
                                count(PinjamBarang::where('status_id', '!=', 1)->groupBy('order_id')->get()) + 
                                PinjamRuangan::where('status_id', '!=', 1)->count() + 
                                PinjamKendaraan::where('status_id', '!=', 1)->count() + 
                                SaranAduan::where('status_id', '!=', 1)->count();
                                $pengguna       = $permohonan->groupBy(function($user){
                                    return $user->user_id;
                                });
        $total_pengguna     = count($pengguna);
        $total_bensin       = 0;
        foreach($bensin as $b){
            $total_bensin += $b->total_penggunaan;
        };
        $pdf                = PDF::loadView('rekap/pdf', ['tanggal' => Carbon::now(), 'tahun' => $tahun, 'total_permohonan' => $total_permohonan, 'total_bensin' => $total_bensin, 'total_pengguna' => $total_pengguna])->setTemporaryFolder(storage_path('temp'));
        $pdf->setOption('enable-javascript', true);
        $pdf->setOption('javascript-delay', 5000);
        $pdf->setOption('enable-smart-shrinking', true);
        $pdf->setOption('no-stop-slow-scripts', true);

        return $pdf->download('Rekapitulasi.pdf');
    }
}
