<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\PiketPengemudi;
use App\Runtext;
use Carbon\Carbon;

class PiketPengemudiController extends Controller
{
    public function index()
    {
        $sekarang           = Carbon::now();
        $bulan_input        = date('m', strtotime($sekarang));
        $runtext            = Runtext::all();

        return view('piket_pengemudi.index', ['bulan_input' => $bulan_input, 'runtext' => $runtext]);
    }

    public function filter(Request $request)
    {
        $sekarang           = Carbon::now();
        $runtext            = Runtext::all();
        if(strlen($request->get('bulan_input')) == 1){
            $bulan_input    = "0".$request->get('bulan_input');
        } else{
            $bulan_input    = $request->get('bulan_input');
        }

        return redirect()->route('piket_pengemudi.index', ['bulan_input' => $bulan_input, 'runtext' => $runtext]);
    }

    public function store(Request $request)
    {
        // hitung jml req
        $count              = count($request->get('tanggal'));
        for($i=1;$i<=$count;$i++)
        {
            // tentukan simpan atau update
            if(PiketPengemudi::where('tanggal', date('Y-m-d', strtotime($request->get('tanggal')[$i-1])))->first() != null){
                $piket          = PiketPengemudi::where('tanggal', date('Y-m-d', strtotime($request->get('tanggal')[$i-1])))->first();
                $piket->nama    = $request->get('nama')[$i-1];
                $piket->kontak  = $request->get('kontak')[$i-1];
                $piket->save();
            } else{
                $piket['tanggal']   = date('Y-m-d', strtotime($request->get('tanggal')[$i-1]));
                $piket['nama']      = $request->get('nama')[$i-1];
                $piket['kontak']    = $request->get('kontak')[$i-1];
                if($piket['nama'] != null){
                    PiketPengemudi::create($piket);
                }
            }
        }
        return redirect()->back()->with('success', 'Berhasil menyimpan jadwal piket.');
    }
}
