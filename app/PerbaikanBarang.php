<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class PerbaikanBarang extends Model
{
    protected $table = 'perbaikan_barangs';

    protected $fillable = [
        'order_id', 'user_id', 'satker_id', 'barang_id', 'jumlah', 'lokasi', 
        'keterangan', 'nodin', 'status_id', 'alasan'
    ];

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }

    public function barang()
    {
        return $this->belongsTo('App\Barang', 'barang_id');
    }

    public function satker()
    {
        return $this->belongsTo('App\Satker', 'satker_id');
    }

    public function status()
    {
        return $this->belongsTo('App\StatusFlags', 'status_id');
    }
}
