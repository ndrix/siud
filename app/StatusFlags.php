<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusFlags extends Model
{
    protected $table = 'status_flags';

    protected $fillable = [
        'nama',
    ];

    public function pinjamRuang()
    {
        return $this->hasMany('App\PinjamRuang', 'status_id');
    }

    public function pinjamKendaraan()
    {
        return $this->hasMany('App\PinjamKendaraan', 'status_id');
    }

    public function bensin()
    {
        return $this->hasMany('App\Bensin', 'status_id');
    }

    public function history()
    {
        return $this->hasMany('App\StatusHistories', 'status_id');
    }

    public function perbaikanBarang()
    {
        return $this->hasMany('App\PerbaikanBarang', 'status_id');
    }

    public function pinjamBarang()
    {
        return $this->hasMany('App\PinjamBarang', 'status_id');
    }

    public function penggandaan()
    {
        return $this->hasMany('App\Penggandaan', 'status_id');
    }

    public function saranAduan()
    {
        return $this->hasMany('App\SaranAduan', 'status_id');
    }
}
