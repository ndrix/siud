<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class StatusHistories extends Model
{
    protected $table = 'status_histories';

    protected $fillable = [
        'order_id', 'models_id', 'status_id', 'alasan', 
    ];

    public function pinjamRuang()
    {
        return $this->belongsTo('App\PinjamRuang', 'order_id');
    }

    public function pinjamKendaraan()
    {
        return $this->belongsTo('App\PinjamKendaraan', 'order_id');
    }

    public function status()
    {
        return $this->belongsTo('App\StatusFlags', 'status_id');
    }

    public function models()
    {
        return $this->belongsTo('App\ModelFLags', 'models_id');
    }
}
