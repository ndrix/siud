<?php

namespace App\Handlers;

use App\User as EloquentUser;
use Adldap\Models\User as LdapUser;
use App\User;

class LdapAttributeHandler
{
    /**
     * Synchronizes ldap attributes to the specified model.
     *
     * @param LdapUser     $ldapUser
     * @param EloquentUser $eloquentUser
     *
     * @return void
     */
    public function handle(LdapUser $ldapUser, EloquentUser $eloquentUser)
    {
        $eloquentUser->name         = $ldapUser->getCommonName();
        $eloquentUser->username     = $ldapUser->getAccountName();
        $eloquentUser->email        = strtolower($ldapUser->getUserPrincipalName());
    }
}

?>