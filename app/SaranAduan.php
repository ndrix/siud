<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SaranAduan extends Model
{
    protected $table = 'saran_aduans';
    
    protected $fillable = [
        'order_id', 'user_id', 'isian', 'status_id', 'alasan'
    ];

    public function status()
    {
        return $this->belongsTo('App\StatusFlags', 'status_id');
    }

    public function user()
    {
        return $this->belongsTo('App\User', 'user_id');
    }
}
