<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Barang extends Model
{
    protected $table = 'barangs';

    protected $fillable = [
        'barang', 'kategori' 
    ];

    public function perbaikanBarang()
    {
        return $this->hasMany('App\PerbaikanBarang', 'barang_id');
    }

    public function pinjamBarang()
    {
        return $this->hasMany('App\PinjamBarang' , 'barang_id');
    }
}
