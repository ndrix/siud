<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Satker extends Model
{
    protected $table = 'satkers';
    
    protected $fillable = [
        'nama', 'created_at', 'updated_at'
    ];

    public function pinjamRuangan()
    {
        return $this->hasMany('App\PinjamRuangan', 'satker_id');
    }

    public function pinjamKendaraan()
    {
        return $this->hasMany('App\PinjamKendaraan', 'satker_id');
    }

    public function penggandaan()
    {
        return $this->hasMany('App\Penggandaan', 'satker_id');
    }

    public function bensin()
    {
        return $this->hasMany('App\Bensin', 'satker_id');
    }
}
