<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class ModelFlagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('model_flags')->insert(array(
            array(
                'nama'          => 'Ruangan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'nama'          => 'Kendaraan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'nama'          => 'Bensin',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'nama'          => 'Inventaris',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
        ));
    }
}
