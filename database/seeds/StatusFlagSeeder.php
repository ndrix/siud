<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class StatusFlagSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('status_flags')->insert(array(
            array(
                'status'        => 'menunggu verifikasi',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'status'        => 'disetujui',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'status'        => 'ditolak',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
        ));
    }
}
