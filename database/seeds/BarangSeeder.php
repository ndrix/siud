<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('barangs')->insert(array(
            array(
                'barang'        => 'air conditioner',
                'kategori'      => 'perbaikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'jaringan internet',
                'kategori'      => 'perbaikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'listrik',
                'kategori'      => 'perbaikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'lampu',
                'kategori'      => 'perbaikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'laptop',
                'kategori'      => 'perbaikan & peminjaman',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'printer',
                'kategori'      => 'perbaikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'proyektor',
                'kategori'      => 'perbaikan & peminjaman',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'converter',
                'kategori'      => 'peminjaman',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'toa (pengeras suara)',
                'kategori'      => 'peminjaman',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'kabel roll',
                'kategori'      => 'peminjaman',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'telepon',
                'kategori'     => 'perbaikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'meubelair',
                'kategori'      => 'perbaikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'toilet & sanitary',
                'kategori'      => 'perbaikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'gedung (lantai/dinding/plafon/jendela)',
                'kategori'      => 'perbaikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'barang'        => 'sarana keamanan kantor (cctv/gerbang)',
                'kategori'      => 'perbaikan',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
        ));
    }
}
