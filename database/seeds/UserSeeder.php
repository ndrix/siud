<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert(array(
            array(
                'name'              => 'test',
                'email'             => 'test@lemsaneg.go.id',
                'password'          => bcrypt('test'),
                'created_at'        => Carbon::now(), 
                'updated_at'        => Carbon::now(),
            ),
            array(
                'name'              => 'joko',
                'email'             => 'joko@lemsaneg.go.id',
                'password'          => bcrypt('joko'),
                'created_at'        => Carbon::now(), 
                'updated_at'        => Carbon::now(),
            ),
            array(
                'name'              => 'joni',
                'email'             => 'joni@lemsaneg.go.id',
                'password'          => bcrypt('joni'),
                'created_at'        => Carbon::now(), 
                'updated_at'        => Carbon::now(),
            ),
            array(
                'name'              => 'jono',
                'email'             => 'jono@lemsaneg.go.id',
                'password'          => bcrypt('jono'),
                'created_at'        => Carbon::now(), 
                'updated_at'        => Carbon::now(),
            ),
            array(
                'name'              => 'joki',
                'email'             => 'joki@lemsaneg.go.id',
                'password'          => bcrypt('joki'),
                'created_at'        => Carbon::now(), 
                'updated_at'        => Carbon::now(),
            ),
        ));
    }
}
