<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class RuanganSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('ruangans')->insert(array(
            array(
                'nama'          => 'Auditorium Roebiono Kertopati', 
                'lokasi'        => 'Gedung A',
                'fasilitas'     => 'Kursi (250), Meja Bulat (15), Meja Kotak (45), Proyektor (1), Sound System (1), Microfone (5)', 
                'kapasitas'     => '250',
                'image'         => 'aula.jpeg',
                'keterangan'    => '',
                'created_at'    => Carbon::now(), 
                'updated_at'    => Carbon::now(),
            ),
            array(
                'nama'          => 'Ruang Opska', 
                'lokasi'        => 'Gedung A Lantai 2',
                'fasilitas'     => 'Kursi (25), Proyektor (1)', 
                'kapasitas'     => '25',
                'image'         => 'room_1.jpeg',
                'keterangan'    => 'Harus dipimpin Eselon I',
                'created_at'    => Carbon::now(), 
                'updated_at'    => Carbon::now(),
            ),
            array(
                'nama'          => 'Ruang Rapat Lt 3', 
                'lokasi'        => 'Gedung C',
                'fasilitas'     => 'Kursi (30), Meja (1), Proyektor (1), Layar (1)', 
                'kapasitas'     => '25',
                'image'         => 'room_2.jpeg',
                'keterangan'    => '',
                'created_at'    => Carbon::now(), 
                'updated_at'    => Carbon::now(),
            ),
            array(
                'nama'          => 'Ruang Rapat Ren', 
                'lokasi'        => 'Gedung C',
                'fasilitas'     => 'Kursi (25), Meja (5), Proyektor (1), Layar (1)', 
                'kapasitas'     => '25',
                'image'         => 'room_2.jpeg',
                'keterangan'    => '',
                'created_at'    => Carbon::now(), 
                'updated_at'    => Carbon::now(),
            ),
        ));
    }
}
