<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class KendaraanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('kendaraans')->insert(array(
            array(
                'nama'              => 'APV 1',
                'nomor_polisi'      => 'B 1366 MQ',
                'kapasitas'         => '7 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'APV 2',
                'nomor_polisi'      => 'B 1760 SQO',
                'kapasitas'         => '7 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'APV 3',
                'nomor_polisi'      => 'B 1396 MQ',
                'kapasitas'         => '7 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Bus Besar',
                'nomor_polisi'      => 'B 7294 DQ',
                'kapasitas'         => '55 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Bus Mini 1',
                'nomor_polisi'      => 'B 7367 QK',
                'kapasitas'         => '25 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Bus Mini 2',
                'nomor_polisi'      => 'B 7365 QK',
                'kapasitas'         => '25 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Dyna 1',
                'nomor_polisi'      => 'B 7141 SPA',
                'kapasitas'         => '15 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Dyna 2',
                'nomor_polisi'      => 'B 7142 SPA',
                'kapasitas'         => '15 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Dyna 3',
                'nomor_polisi'      => 'B 7139 SPA',
                'kapasitas'         => '15 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Dyna 4',
                'nomor_polisi'      => 'B 7140 SPA',
                'kapasitas'         => '15 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Hilux',
                'nomor_polisi'      => 'B 9083 STA',
                'kapasitas'         => 'barang',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Mobil Box',
                'nomor_polisi'      => 'B 9827 WQ',
                'kapasitas'         => 'barang',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Suzuki Ertiga',
                'nomor_polisi'      => 'B 1085 SQQ',
                'kapasitas'         => '6 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Terrano',
                'nomor_polisi'      => 'B 1459 WQ',
                'kapasitas'         => '5 seats',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
            array(
                'nama'              => 'Truk',
                'nomor_polisi'      => 'B 9830 WQ',
                'kapasitas'         => 'barang',
                'created_at'        => Carbon::now(),
                'updated_at'        => Carbon::now(),
            ),
        ));
    }
}
