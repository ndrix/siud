<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(UserSeeder::class);
        $this->call(RuanganSeeder::class);
        $this->call(KendaraanSeeder::class);
        $this->call(StatusFlagSeeder::class);
        $this->call(ModelFlagSeeder::class);
        $this->call(SatkerSeeder::class);
        $this->call(BarangSeeder::class);
    }
}
