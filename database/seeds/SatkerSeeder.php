<?php

use Illuminate\Database\Seeder;
use Carbon\Carbon;

class SatkerSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('satkers')->insert(array(
            array(
                'satker'        => 'P21',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'satker'        => 'P22',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
            array(
                'satker'        => 'P23',
                'created_at'    => Carbon::now(),
                'updated_at'    => Carbon::now(),
            ),
        ));
    }
}
