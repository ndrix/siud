<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateBensinsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('bensins', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id');
            $table->integer('user_id')->unsigned();
            $table->integer('satker_id')->unsigned();
            $table->timestamp('waktu');
            $table->string('jabatan');
            $table->integer('total_penggunaan');
            $table->string('kuitansi');
            $table->integer('status_id')->nullable();
            $table->string('alasan')->nullable();
            $table->timestamps();

            $table->foreign('satker_id')->references('id')->on('satkers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('bensins');
    }
}
