<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreatePinjamKendaraansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('pinjam_kendaraans', function (Blueprint $table) {
            $table->increments('id');
            $table->string('order_id');
            $table->integer('user_id')->unsigned();
            $table->integer('satker_id')->unsigned();
            $table->integer('kendaraan_id')->unsigned();
            $table->string('kegiatan');
            $table->string('tujuan');
            $table->integer('jml_penumpang');
            $table->dateTime('waktu_mulai');
            $table->dateTime('waktu_selesai');
            $table->enum('dukungan_pengemudi', ['ya', 'tidak'])->default(NULL);
            $table->string('nodin')->nullable();
            $table->integer('status_id')->unsigned()->nullable();
            $table->string('alasan')->nullable();
            $table->timestamps();

            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('kendaraan_id')->references('id')->on('kendaraans')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('satker_id')->references('id')->on('satkers')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('status_id')->references('id')->on('status_flags')->onUpdate('cascade')->onDelete('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('pinjam_kendaraans');
    }
}
