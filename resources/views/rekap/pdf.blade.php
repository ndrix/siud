<html>
  <head>
    <title>Laporan Rekapitulasi</title>
    <!-- END THEME LAYOUT STYLES -->
    <link rel="shortcut icon" href="{{ asset('global/img/favicon.ico') }}" /> 
  </head>
  <body onload="init()">
    <table width="100%">
      <tr>
        <td width="12%" align="center">
          <img src="{{ asset('pages/img/logo-bssn.png') }}" width="80px" height="80px">
        </td>
        <td width="40%" align="center">
          <b>BADAN SIBER DAN SANDI NEGARA</b><br>
          Jl. Harsono RM. No. 70, Ragunan, Pasar Minggu, Jakarta Selatan 12550<br>
          Telp. 7805814, Fax. 78844104<br>
          email : <u>humas@bssn.go.id</u> &nbsp; website : <u>www.bssn.go.id</u>
        </td>
      </tr>
    </table>
    <style>
      hr {
        height: 3px;
        color: #123455;
        background-color: #123455;
        border: none;
      }
      #opname {
        font-family: "Trebuchet MS", Arial, Helvetica, sans-serif;
        border-collapse: collapse;
        width: 100%;
      }
      #opname td, #opname th {
        border: 1px solid #ddd;
        padding: 8px;
      }
      #opname tr:nth-child(even){background-color: #f2f2f2;}
      #opname tr:hover {background-color: #ddd;}
      #opname th {
        padding-top: 12px;
        padding-bottom: 12px;
        text-align: left;
        background-color: #696969;
        color: white;
      }
      .bar-chart {
            width: 700px;
            height: 500px;
            margin: 0 auto;
      }
      .bar-chart-2 {
            width: 1200px;
            height: 500px;
            margin: 0 auto;
      }
      .pie-chart {
            width: 500px;
            height: 400px;
            margin: 0 auto;
      }
      .pie-chart-2 {
            width: 500px;
            height: 300px;
            margin: 0 auto;
      }
      .pie-chart-3 {
            width: 500px;
            height: 300px;
            margin: 0 auto;
      }
      table {
        border-collapse: collapse;
      }
    </style>
    <hr>
    <br>
    @php
        $angka_bulan = date('m', strtotime($tanggal));
        switch($angka_bulan){
            case "01" :
                $bulan = "Januari";
                break;
            case "02" :
                $bulan = "Februari";
                break;
            case "03" :
                $bulan = "Maret";
                break;
            case "04" :
                $bulan = "April";
                break;
            case "05" :
                $bulan = "Mei";
                break;
            case "06" :
                $bulan = "Juni";
                break;
            case "07" :
                $bulan = "Juli";
                break;
            case "08" :
                $bulan = "Agustus";
                break;
            case "09" :
                $bulan = "September";
                break;
            case "10" :
                $bulan = "Oktober";
                break;
            case "11" :
                $bulan = "November";
                break;
            case "12" :
                $bulan = "Desember";
                break;
        }
    @endphp
    <div align="center">
      <h3>LAPORAN REKAPITULASI<br>
        SIUD 2.0
      </h3>
      <b>Tahun {{ date('Y', strtotime($tanggal)) }}</b>
    </div><br>
    <?php
        //jml permohonan tiap bulan
        for($i=1; $i<=12; $i++){
        $ruangan            = \App\PinjamRuangan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->whereYear('updated_at', $tahun)->count();
        $kendaraan          = \App\PinjamKendaraan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->whereYear('updated_at', $tahun)->count();
        $perbaikan          = count(\App\PerbaikanBarang::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
        $pinjam_barang      = count(\App\PinjamBarang::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
        $penggandaan        = count(\App\Penggandaan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
        $saran_aduan        = \App\SaranAduan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->whereYear('updated_at', $tahun)->count();
        $jml_permohonan     = $ruangan + $kendaraan + $perbaikan + $pinjam_barang + $penggandaan + $saran_aduan;
        $arr_jml_permohonan[$i] = $jml_permohonan;
        };
        //jml permohonan seluruhnya
        $ruangan            = \App\PinjamRuangan::where('status_id', '!=', 1)->whereYear('updated_at', $tahun)->count();
        $kendaraan          = \App\PinjamKendaraan::where('status_id', '!=', 1)->whereYear('updated_at', $tahun)->count();
        $perbaikan          = count(\App\PerbaikanBarang::where('status_id', '!=', 1)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
        $pinjam_barang      = count(\App\PinjamBarang::where('status_id', '!=', 1)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
        $penggandaan        = count(\App\Penggandaan::where('status_id', '!=', 1)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
        $saran_aduan        = \App\SaranAduan::where('status_id', '!=', 1)->whereYear('updated_at', $tahun)->count();
        $total_permohonan   = $ruangan + $kendaraan + $perbaikan + $pinjam_barang + $penggandaan + $saran_aduan;
        //jml bensin
        $bensin             = \App\Bensin::whereYear('created_at', $tahun)->get();
        $total_bensin       = 0;
        foreach($bensin as $b){
            $total_bensin += $b->total_penggunaan;
        };
    ?>
    <div class="row">
        <div class="col-md-12">
            <table border="0">
                <tr>
                    <td><div id="barchart" class="bar-chart"></div></td>
                    <td><div id="piechart" class="pie-chart"></div></td>
                </tr>
            </table>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <center>
            <table border="1">
                <tr>
                    <td width="500" height="40">&emsp;<b>Total Permohonan</b></td>
                    <td width="300"><center><b><?php echo $total_permohonan; ?></b></center></td>
                </tr>
                <tr>
                    <td width="500" height="40">&emsp;<b>Total Bensin</b></td>
                    <td width="300"><center><b><?php echo $total_bensin; ?></b></center></td>
                </tr>
                <tr>
                    <td width="500" height="40">&emsp;<b>Pengguna Aktif</b></td>
                    <td width="300"><center><b><?php echo '10'; ?></b></center></td>
                </tr>
            </table>
            </center>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <center>
            <table border="0">
                <tr>
                    <td><div id="barchart-2" class="bar-chart-2"></div></td>
                </tr>
            </table>
            </center>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <center>
            <table border="0">
                <tr>
                    <td><div id="piechart-2" class="pie-chart-2"></div></td>
                    <td><div id="piechart-3" class="pie-chart-3"></div></td>
                </tr>
            </table>
            </center>
        </div>
    </div>
  </body>
  <script type="text/javascript" src="https://www.google.com/jsapi"></script>
  <script type="text/javascript">
      function init() {
          google.load("visualization", "1.1", {
              packages: ["corechart"],
              callback: 'drawCharts'
          });
      }
      function drawCharts() {
          var data = google.visualization.arrayToDataTable([
              ['Bulan', 'Jumlah', { role: 'style' }],
              ['Januari', <?php echo $arr_jml_permohonan[1]; ?>, '#99ccff'],
              ['Februari', <?php echo $arr_jml_permohonan[2]; ?>, '#9999ff'],
              ['Maret', <?php echo $arr_jml_permohonan[3]; ?>, '#cc99ff'],
              ['April', <?php echo $arr_jml_permohonan[4]; ?>, '#ff99ff'],
              ['Mei', <?php echo $arr_jml_permohonan[5]; ?>, '#ff99cc'],
              ['Juni', <?php echo $arr_jml_permohonan[6]; ?>, '#66ffb2'],
              ['Juli', <?php echo $arr_jml_permohonan[7]; ?>, '#e5ccff'],
              ['Agustus', <?php echo $arr_jml_permohonan[8]; ?>, '#ffcce5'],
              ['September', <?php echo $arr_jml_permohonan[9]; ?>, '#ffccff'],
              ['Oktober', <?php echo $arr_jml_permohonan[10]; ?>, '#ccccff'],
              ['November', <?php echo $arr_jml_permohonan[11]; ?>, '#66b2ff'],
              ['Desember', <?php echo $arr_jml_permohonan[12]; ?>, '#ff66b2']
          ]);
          var view = new google.visualization.DataView(data);
          view.setColumns([0, 1,
                        { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                        2]);
          var options = {
              title: 'Jumlah Permohonan Tiap Bulan',
              chartArea: {
                backgroundColor: {
                    stroke: '#4322c0',
                    strokeWidth: 3
                }
              },
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('barchart'));
          chart.draw(view, options);

          var data2 = google.visualization.arrayToDataTable([
              ['Jenis', 'Jumlah', { role: 'style' }],
              ['Ruangan', 10, '#99ccff'],
              ['Kendaraan', 5, '#9999ff'],
              ['Perbaikan', 4, '#cc99ff'],
              ['Peminjaman Barang', 3, '#ff99ff'],
              ['Penggandaan', 2, '#ff99cc'],
              ['Saran Aduan', 1, '#66ffb2']
          ]);
          var options = {
              title: 'Sebaran Jenis Permohonan',
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart'));
          chart.draw(data2, options);

          var data3 = google.visualization.arrayToDataTable([
              ['Bulan', 'Jumlah', { role: 'style' }],
              <?php
                $satker = \App\Satker::all();
                foreach($satker as $s){
                  $ruangan            = \App\PinjamRuangan::where('status_id', '!=', 1)->where('satker_id', $s->id)->whereYear('updated_at', $tahun)->count();
                  $kendaraan          = \App\PinjamKendaraan::where('status_id', '!=', 1)->where('satker_id', $s->id)->whereYear('updated_at', $tahun)->count();
                  $perbaikan          = count(\App\PerbaikanBarang::where('status_id', '!=', 1)->where('satker_id', $s->id)->whereYear('updated_at', $tahun)->groupBy('order_id')->get());
                  $pinjam_barang      = count(\App\PinjamBarang::where('status_id', '!=', 1)->where('satker_id', $s->id)->whereYear('updated_at', $tahun)->groupBy('order_id')->get());
                  $penggandaan        = count(\App\Penggandaan::where('status_id', '!=', 1)->where('satker_id', $s->id)->whereYear('updated_at', $tahun)->groupBy('order_id')->get());
                  $jml_permohonan     = $ruangan + $kendaraan + $perbaikan + $pinjam_barang + $penggandaan;
                  $color              = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                  echo "["."'".$s->satker."',".$jml_permohonan.","."'".$color."'],"; 
                }
              ?>
          ]);
          var view3 = new google.visualization.DataView(data3);
          view3.setColumns([0, 1,
                        { calc: "stringify",
                            sourceColumn: 1,
                            type: "string",
                            role: "annotation" },
                        2]);
          var options = {
              title: 'Sebaran Permohonan Tiap Unit',
              chartArea: {
                backgroundColor: {
                    stroke: '#4322c0',
                    strokeWidth: 3
                }
              },
          };
          var chart = new google.visualization.ColumnChart(document.getElementById('barchart-2'));
          chart.draw(view3, options);

          var data4 = google.visualization.arrayToDataTable([
              ['Jenis', 'Jumlah', { role: 'style' }],
              <?php
                $kendaraan = \App\Kendaraan::all();
                foreach($kendaraan as $k){
                  $color                = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                  $jml_kendaraan        = \App\PinjamKendaraan::where('kendaraan_id', $k->id)->whereYear('updated_at', $tahun)->count();
                  echo "["."'".$k->nama."',".$jml_kendaraan.","."'"."'],"; 
                }
              ?>
          ]);
          var options = {
              title: 'Sebaran Jenis Kendaraan',
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart-2'));
          chart.draw(data4, options);

          var data5 = google.visualization.arrayToDataTable([
              ['Jenis', 'Jumlah', { role: 'style' }],
              <?php
                $ruangan = \App\Ruangan::all();
                foreach($ruangan as $r){
                  $color              = sprintf('#%06X', mt_rand(0, 0xFFFFFF));
                  $jml_ruangan  = \App\PinjamRuangan::where('ruangan_id', $r->id)->whereYear('updated_at', $tahun)->count();
                  echo "["."'".$r->nama."',".$jml_ruangan.","."'".$color."'],"; 
                }
              ?>
          ]);
          var options = {
              title: 'Sebaran Jenis Ruangan',
          };
          var chart = new google.visualization.PieChart(document.getElementById('piechart-3'));
          chart.draw(data5, options);
      }
  </script>
</html>