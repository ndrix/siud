@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar-check-o font-green"></i>
                            <span class="caption-subject font-green sbold uppercase">&nbsp; Rekapitulasi </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <form action="{{ route('rekap.filter') }}" method="post" class="form-horizontal">
                                @csrf
                                <div class="col-md-2">
                                    <h4>Pilih Tahun</h4>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <div class="col-md-5">
                                            <select class="form-control" name="tahun">
                                                @foreach ($list_tahun as $t)
                                                    @if($t == $tahun)
                                                        <option value="{{ $t }}" selected>{{ $t }}</option>
                                                    @else
                                                        <option value="{{ $t }}">{{ $t }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <button type="submit" class="btn btn green">Submit</button>
                                        </div>
                                        <div class="col-md-2">
                                            <a href="{{ route('rekap.download', ['tahun' => $tahun]) }}" class="btn red">Cetak Laporan</a>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row widget-row">
            <div class="col-md-4">
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading">Total Permohonan</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-blue icon-layers"></i>
                        <div class="widget-thumb-body">
                            {{-- <span class="widget-thumb-subtitle">USD</span> --}}<br>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $total_permohonan }}">{{ $total_permohonan }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading">Total Bensin (Rp)</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-purple fa fa-dollar"></i>
                        <div class="widget-thumb-body">
                            {{-- <span class="widget-thumb-subtitle">USD</span> --}}<br>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $total_bensin }}">{{ $total_bensin }}</span>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-4">
                <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                    <h4 class="widget-thumb-heading">Pengguna Aktif</h4>
                    <div class="widget-thumb-wrap">
                        <i class="widget-thumb-icon bg-yellow-lemon fa fa-user"></i>
                        <div class="widget-thumb-body">
                            {{-- <span class="widget-thumb-subtitle">USD</span> --}}<br>
                            <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $total_pengguna }}">{{ $total_pengguna }}</span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-signal font-purple-plum"></i>
                            <span class="caption-subject font-purple-plum sbold uppercase">&nbsp; Jumlah Permohonan Per Bulan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <canvas id="ChartOne" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-puzzle-piece font-blue"></i>
                            <span class="caption-subject font-blue sbold uppercase">&nbsp; Jenis Permohonan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <canvas id="ChartTwo" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-balance-scale font-red-pink"></i>
                            <span class="caption-subject font-red-pink sbold uppercase">&nbsp; Sebaran Permohonan Per Satker </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <canvas id="ChartThree" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-car font-blue-hoki"></i>
                            <span class="caption-subject font-blue-hoki sbold uppercase">&nbsp; Penggunaan Kendaraan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <canvas id="ChartFour" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-md-6">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-home font-yellow-crusta"></i>
                            <span class="caption-subject font-yellow-crusta sbold uppercase">&nbsp; Penggunaan Ruangan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <canvas id="ChartFive" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar font-red-haze"></i>
                            <span class="caption-subject font-red-haze sbold uppercase">&nbsp; Daftar Permohonan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-scrollable" id="sample_3">
                            <thead>
                                <tr>
                                    <th width="8%">No</th>
                                    <th width="20%">Nomor</th>
                                    <th width="25%">Tanggal Dibuat</th>
                                    <th width="17%">Jenis</th>
                                    <th width="20%">Status</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $bulan = array (1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                                    $i = 1;
                                @endphp
                                @foreach($permohonan as $n)
                                    <tr>
                                        <td style="font-size:12px" align="center">{{ $i }}</td>
                                        <td style="font-size:12px">{{ $n->order_id }}</td>
                                        <td style="font-size:12px">{{ date('d ', strtotime($n->created_at)).$bulan[date('n', strtotime($n->created_at))].date(' Y', strtotime($n->created_at)) }}</td>
                                        <td style="font-size:12px">
                                            @php 
                                                $substr = substr($n->order_id, strpos($n->order_id,"SIUD.")+strlen("SIUD."),strlen($n->order_id)); 
                                                $jenis  = substr($substr,0,strpos($substr,"/"));
                                            @endphp
                                            @switch($jenis)
                                                @case(1)
                                                    Peminjaman Ruangan
                                                    @break
                                                @case(2)
                                                    Peminjaman Kendaraan
                                                    @break
                                                @case(3)
                                                    Perbaikan Barang
                                                    @break
                                                @case(4)
                                                    Peminjaman Barang
                                                    @break
                                                @case(5)
                                                    Penggandaan
                                                    @break
                                                @case(6)
                                                    Bensin
                                                    @break
                                                @case(7)
                                                    Saran/Aduan
                                                    @break
                                            @endswitch
                                        </td>
                                        <td style="font-size:12px">
                                            @switch($n->status_id)
                                                @case(1)
                                                    <span class="label label-warning"><font style="font-size:12px"> {{ ucwords($n->status->status) }} </font></span>
                                                    @break
                                                @case(2)
                                                    <span class="label label-success"><font style="font-size:12px"> {{ ucwords($n->status->status) }} </font></span>
                                                    @break
                                                @case(3)
                                                    <span class="label label-danger"><font style="font-size:12px"> {{ ucwords($n->status->status) }} </font></span>
                                                    @break       
                                            @endswitch
                                        </td>
                                    </tr>    
                                    @php $i++; @endphp    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
@section('script')
<script src="{{ asset('pages/scripts/charts-chartjsbundle.min.js') }}"></script>
    <script>
        var ctx = document.getElementById("ChartOne");
        var chartOne = new Chart(ctx, {
            type: 'line',
            data: {
                labels: [
                    <?php
                        $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                        foreach($bulan as $b){
                            echo '"'.$b.'"'.","; 
                        }
                    ?>
                ],
                datasets: [{
                        label: 'Jumlah',
                        data: [
                            <?php
                                for($i=1; $i<=12; $i++){
                                    $ruangan            = \App\PinjamRuangan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->whereYear('updated_at', $tahun)->count();
                                    $kendaraan          = \App\PinjamKendaraan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->whereYear('updated_at', $tahun)->count();
                                    $perbaikan          = count(\App\PerbaikanBarang::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
                                    $pinjam_barang      = count(\App\PinjamBarang::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
                                    $penggandaan        = count(\App\Penggandaan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
                                    $saran_aduan        = \App\SaranAduan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->whereYear('updated_at', $tahun)->count();
                                    $jml_permohonan     = $ruangan + $kendaraan + $perbaikan + $pinjam_barang + $penggandaan + $saran_aduan;
                                    echo "'".$jml_permohonan."',";
                                }
                            ?>
                        ],
                        backgroundColor: [ 
                            <?php
                                for($i=0;$i<12;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderColor: [
                            <?php
                                for($i=0;$i<12;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        },
                        gridLines: {
                            display: true
                        },
                        display: true,
                    }],
                    xAxes: [{
                        ticks: {
                            autoSkip: false
                        },
                    }]
                }
            }
        });
    </script>
    <script>
        var ctx = document.getElementById("ChartTwo");
        var chartTwo = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [
                    'Ruangan', 'Kendaraan', 'Perbaikan', 'Peminjaman Barang', 'Penggandaan', 'Saran Aduan'
                ],
                datasets: [{
                        data: [
                            <?php
                                $ruangan            = \App\PinjamRuangan::where('status_id', '!=', 1)->whereYear('updated_at', $tahun)->count();
                                $kendaraan          = \App\PinjamKendaraan::where('status_id', '!=', 1)->whereYear('updated_at', $tahun)->count();
                                $perbaikan          = count(\App\PerbaikanBarang::where('status_id', '!=', 1)->whereYear('updated_at', $tahun)->groupBy('order_id')->get());
                                $pinjam_barang      = count(\App\PinjamBarang::where('status_id', '!=', 1)->whereYear('updated_at', $tahun)->groupBy('order_id')->get());
                                $penggandaan        = count(\App\Penggandaan::where('status_id', '!=', 1)->whereYear('updated_at', $tahun)->groupBy('order_id')->get());
                                $saran_aduan        = \App\SaranAduan::where('status_id', '!=', 1)->whereYear('updated_at', $tahun)->count();
                                echo "'".$ruangan."'".","."'".$kendaraan."'".","."'".$perbaikan."'".","."'".$pinjam_barang."'".","
                                    ."'".$penggandaan."'".","."'".$saran_aduan."'";
                            ?>
                        ],
                        backgroundColor: [ 
                            <?php
                                for($i=0;$i<6;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderColor: [
                            <?php
                                for($i=0;$i<6;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            },
                            display: false,
                        }]
                }
            }
        });
    </script>
    <script>
        var ctx = document.getElementById("ChartThree");
        var chartThree = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    <?php
                        $satker = \App\Satker::all();
                        foreach($satker as $s){
                            echo "'".$s->satker."'".","; 
                        }
                    ?>
                ],
                datasets: [{
                        label: 'Jumlah',
                        data: [
                            <?php
                                foreach($satker as $s){
                                    $ruangan            = \App\PinjamRuangan::where('status_id', '!=', 1)->where('satker_id', $s->id)->whereYear('updated_at', $tahun)->count();
                                    $kendaraan          = \App\PinjamKendaraan::where('status_id', '!=', 1)->where('satker_id', $s->id)->whereYear('updated_at', $tahun)->count();
                                    $perbaikan          = count(\App\PerbaikanBarang::where('status_id', '!=', 1)->where('satker_id', $s->id)->whereYear('updated_at', $tahun)->groupBy('order_id')->get());
                                    $pinjam_barang      = count(\App\PinjamBarang::where('status_id', '!=', 1)->where('satker_id', $s->id)->whereYear('updated_at', $tahun)->groupBy('order_id')->get());
                                    $penggandaan        = count(\App\Penggandaan::where('status_id', '!=', 1)->where('satker_id', $s->id)->whereYear('updated_at', $tahun)->groupBy('order_id')->get());
                                    $jml_permohonan     = $ruangan + $kendaraan + $perbaikan + $pinjam_barang + $penggandaan;
                                    echo "'".$jml_permohonan."'".","; 
                                }
                            ?>
                        ],
                        backgroundColor: [ 
                            <?php
                                for($i=0;$i<12;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderColor: [
                            <?php
                                for($i=0;$i<12;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        },
                        gridLines: {
                            display: true
                        },
                        display: true,
                    }],
                    xAxes: [{
                        ticks: {
                            autoSkip: false
                        },
                    }]
                }
            }
        });
    </script>
    <script>
        var ctx = document.getElementById("ChartFour");
        var chartFour = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: [
                    <?php
                        $kendaraan = \App\Kendaraan::all();
                        foreach($kendaraan as $k){
                            echo "'".$k->nama."'".","; 
                        }
                    ?>
                ],
                datasets: [{
                        data: [
                            <?php
                                foreach($kendaraan as $k){
                                    $jml_kendaraan          = \App\PinjamKendaraan::where('kendaraan_id', $k->id)->whereYear('updated_at', $tahun)->count();
                                    echo "'".$jml_kendaraan."'".","; 
                                }
                            ?>
                        ],
                        backgroundColor: [ 
                            <?php
                                for($i=0;$i<6;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderColor: [
                            <?php
                                for($i=0;$i<6;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            },
                            display: false,
                        }]
                }
            }
        });
    </script>
    <script>
        var ctx = document.getElementById("ChartFive");
        var chartFive = new Chart(ctx, {
            type: 'doughnut',
            data: {
                labels: [
                    <?php
                        $ruangan = \App\Ruangan::all();
                        foreach($ruangan as $r){
                            echo "'".$r->nama."'".","; 
                        }
                    ?>
                ],
                datasets: [{
                        data: [
                            <?php
                                foreach($ruangan as $r){
                                    $jml_ruangan    = \App\PinjamRuangan::where('ruangan_id', $r->id)->whereYear('updated_at', $tahun)->count();
                                    echo "'".$jml_ruangan."'".","; 
                                }
                            ?>
                        ],
                        backgroundColor: [ 
                            <?php
                                for($i=0;$i<6;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderColor: [
                            <?php
                                for($i=0;$i<6;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            },
                            display: false,
                        }]
                }
            }
        });
    </script>
@endsection