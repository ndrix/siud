<!DOCTYPE html>
<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->
    <head>
        <meta charset="utf-8" />
        <title>SIUD 2.0</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="SIUD versi terbaru." name="description" />
        <meta content="" name="author" />
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="{{ asset('global/plugins/google_fonts.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{ asset('pages/css/about.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <link href="{{ asset('global/plugins/bootstrap-daterangepicker/daterangepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/morris/morris.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/fullcalendar/fullcalendar.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/jqvmap/jqvmap/jqvmap.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/datatables/datatables.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/select2/css/select2.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/select2/css/select2-bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset('global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="{{ asset('layouts/layout4/css/layout.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('layouts/layout4/css/themes/default.min.css') }}" rel="stylesheet" type="text/css" id="style_color" />
        <link href="{{ asset('layouts/layout4/css/custom.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="{{ asset('global/img/favicon.ico') }}" /> 
    </head>
    <!-- END HEAD -->

    <body class="page-container-bg-solid page-header-fixed page-sidebar-fixed">
        <!-- BEGIN HEADER -->
        <div class="page-header navbar navbar-fixed-top">
            <!-- BEGIN HEADER INNER -->
            <div class="page-header-inner ">
                <!-- BEGIN LOGO -->
                <div class="page-logo">
                    <a href="index.html">
                        <img src="{{ asset('layouts/layout4/img/logo-new.png') }}" alt="logo" class="logo-default" /> </a>
                    <div class="menu-toggler sidebar-toggler">
                        <!-- DOC: Remove the above "hide" to enable the sidebar toggler button on header -->
                    </div>
                </div>
                <!-- END LOGO -->
                <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse"> </a>
                <!-- END RESPONSIVE MENU TOGGLER -->
                <!-- BEGIN PAGE TOP -->
                <div class="page-top">
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="separator hide"> </li>
                            <!-- BEGIN NOTIFICATION DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <!-- DOC: Apply "dropdown-hoverable" class after "dropdown" and remove data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to enable hover dropdown mode -->
                            <!-- DOC: Remove "dropdown-hoverable" and add data-toggle="dropdown" data-hover="dropdown" data-close-others="true" attributes to the below A element with dropdown-toggle class -->
                            @php
                                $notif_count    = Auth::user()->unreadnotifications->count();
                                function created($created_at)
                                {
                                    $created_at = $created_at->diffForHumans();
                                    $created_at = str_replace([' seconds', ' second'], ' s', $created_at);
                                    $created_at = str_replace([' minutes', ' minute'], ' m', $created_at);
                                    $created_at = str_replace([' hours', ' hour'], ' h', $created_at);
                                    $created_at = str_replace([' months', ' month'], ' m', $created_at);

                                    if(preg_match('(years|year)', $created_at)){
                                        $created_at = $this->created_at->toFormattedDateString();
                                    }

                                    return $created_at;
                                }
                            @endphp
                            <li class="dropdown dropdown-extended dropdown-notification dropdown-dark" id="header_notification_bar">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-success"> {{ $notif_count}} </span>
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>
                                            <span class="bold">{{ $notif_count }} notifikasi baru</span>
                                        </h3>
                                    </li>
                                    @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                            @foreach(Auth::user()->unreadnotifications as $n)
                                                @php
                                                    $encrypt_order_id   = Illuminate\Support\Facades\Crypt::encryptString($n->data['order_id']);
                                                @endphp
                                                <li>
                                                    @switch($n->data['jenis'])
                                                        @case('ruangan')
                                                            <a href="{{ route('pinjam_ruangan.kelola', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('kendaraan')
                                                            <a href="{{ route('pinjam_kendaraan.kelola', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('dinas')
                                                            <a href="{{ route('dinas.kelola', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('peminjaman')
                                                            <a href="{{ route('pinjam_barang.kelola', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('penggandaan')
                                                            <a href="{{ route('penggandaan.kelola', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('bensin')
                                                            <a href="{{ route('bensin.kelola', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('saran / aduan')
                                                            <a href="{{ route('saranaduan.kelola', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                    @endswitch
                                                        <span class="time">{{ created($n->created_at) }}</span>
                                                        <span class="details">
                                                            <span class="label label-sm label-icon label-success">
                                                                <i class="fa fa-plus"></i>
                                                            </span> Permohonan Masuk </span>
                                                    </a>
                                                </li>
                                            @endforeach
                                        </ul>
                                    </li>
                                    @else
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283">
                                            @foreach(Auth::user()->unreadnotifications as $n)
                                                @php
                                                    $encrypt_order_id   = Illuminate\Support\Facades\Crypt::encryptString($n->data['order_id']);
                                                @endphp
                                                @if($n->data['aksi'] == "setuju")
                                                    <li>
                                                        @switch($n->data['jenis'])
                                                            @case('ruangan')
                                                                <a href="{{ route('pinjam_ruangan.show', ['order_id' => $encrypt_order_id]) }}">
                                                            @break
                                                            @case('kendaraan')
                                                                <a href="{{ route('pinjam_kendaraan.show', ['order_id' => $encrypt_order_id]) }}">
                                                            @break
                                                            @case('dinas')
                                                                <a href="{{ route('dinas.show', ['order_id' => $encrypt_order_id]) }}">
                                                            @break
                                                            @case('peminjaman')
                                                                <a href="{{ route('pinjam_barang.show', ['order_id' => $encrypt_order_id]) }}">
                                                            @break
                                                            @case('penggandaan')
                                                                <a href="{{ route('penggandaan.show', ['order_id' => $encrypt_order_id]) }}">
                                                            @break
                                                            @case('bensin')
                                                                <a href="{{ route('bensin.show', ['order_id' => $encrypt_order_id]) }}">
                                                            @break
                                                            @case('saran / aduan')
                                                                <a href="{{ route('saranaduan.show', ['order_id' => $encrypt_order_id]) }}">
                                                            @break
                                                        @endswitch
                                                            <span class="time">{{ created($n->created_at) }}</span>
                                                            <span class="details">
                                                                <span class="label label-sm label-icon label-success">
                                                                    <i class="fa fa-check"></i>
                                                                </span> Permohonan {{ ucwords($n->data['jenis']) }} Disetujui </span>
                                                        </a>
                                                    </li>
                                                @else
                                                    <li>
                                                        @switch($n->data['jenis'])
                                                        @case('ruangan')
                                                            <a href="{{ route('pinjam_ruangan.show', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('kendaraan')
                                                            <a href="{{ route('pinjam_kendaraan.show', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('dinas')
                                                            <a href="{{ route('dinas.show', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('peminjaman')
                                                            <a href="{{ route('pinjam_barang.show', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('penggandaan')
                                                            <a href="{{ route('penggandaan.show', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('bensin')
                                                            <a href="{{ route('bensin.show', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @case('saran / aduan')
                                                            <a href="{{ route('saranaduan.show', ['order_id' => $encrypt_order_id]) }}">
                                                        @break
                                                        @endswitch
                                                            <span class="time">{{ created($n->created_at) }}</span>
                                                            <span class="details">
                                                                <span class="label label-sm label-icon label-danger">
                                                                    <i class="fa fa-remove"></i>
                                                                </span> Permohonan {{ ucwords($n->data['jenis']) }} Ditolak </span>
                                                        </a>
                                                    </li>
                                                @endif
                                            @endforeach
                                        </ul>
                                    </li>
                                    @endhasanyrole
                                </ul>
                            </li>
                            <!-- END NOTIFICATION DROPDOWN -->
                            <!-- BEGIN USER LOGIN DROPDOWN -->
                            <!-- DOC: Apply "dropdown-dark" class after below "dropdown-extended" to change the dropdown styte -->
                            <li class="dropdown dropdown-user dropdown-dark">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <span class="username username-hide-on-mobile"> {{ ucwords(Auth::user()->name) }} </span>
                                    <!-- DOC: Do not remove below empty space(&nbsp;) as its purposely used -->
                                    <img alt="" class="img-circle" src="{{ asset('layouts/layout4/img/user_avatar.png') }}" /> </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <li>
                                        <a class="dropdown-item" href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            <i class="icon-key"></i> Log Out </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </li>
                            <!-- END USER LOGIN DROPDOWN -->
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END PAGE TOP -->
            </div>
            <!-- END HEADER INNER -->
        </div>
        <!-- END HEADER -->
        <!-- BEGIN HEADER & CONTENT DIVIDER -->
        <div class="clearfix"> </div>
        <!-- END HEADER & CONTENT DIVIDER -->
        <!-- BEGIN CONTAINER -->
        <div class="page-container">
            <!-- BEGIN SIDEBAR -->
            <div class="page-sidebar-wrapper">
                <!-- BEGIN SIDEBAR -->
                <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                <!-- DOC: Change data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                <div class="page-sidebar navbar-collapse collapse">
                    <!-- BEGIN SIDEBAR MENU -->
                    <!-- DOC: Apply "page-sidebar-menu-light" class right after "page-sidebar-menu" to enable light sidebar menu style(without borders) -->
                    <!-- DOC: Apply "page-sidebar-menu-hover-submenu" class right after "page-sidebar-menu" to enable hoverable(hover vs accordion) sub menu mode -->
                    <!-- DOC: Apply "page-sidebar-menu-closed" class right after "page-sidebar-menu" to collapse("page-sidebar-closed" class must be applied to the body element) the sidebar sub menu mode -->
                    <!-- DOC: Set data-auto-scroll="false" to disable the sidebar from auto scrolling/focusing -->
                    <!-- DOC: Set data-keep-expand="true" to keep the submenues expanded -->
                    <!-- DOC: Set data-auto-speed="200" to adjust the sub menu slide up/down speed -->
                    @php 
                        $route_name     = \Request::route()->getName();
                        $route_kelola   = array('permohonan.kelola', 'pinjam_ruangan.kelola', 'pinjam_kendaraan.kelola', 'dinas.kelola',
                                            'pinjam_barang.kelola', 'penggandaan.kelola', 'saranaduan.kelola', 'tolak');
                        $route_ruangan  = array('ruangan', 'pinjam_ruangan.create', 'ruangan.index', 'ruangan.edit', 'ruangan.create', 'ruangan.kelola');
                        $route_kendaraan= array('kendaraan', 'pinjam_kendaraan.create', 'kendaraan.index', 'kendaraan.edit', 'kendaraan.create', 'kendaraan.kelola');
                        $route_permohonan = array('pinjam_ruangan.show', 'pinjam_kendaraan.show', 'dinas.show', 'pinjam_barang.show', 'penggandaan.show', 'bensin.show', 'saranaduan.show');
                    @endphp
                    <ul class="page-sidebar-menu" data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200">
                        <li class="nav-item @if(strpos($route_name,"home") !== false) active @endif">
                            <a href="{{ route('home') }}" class="nav-link nav-toggle">
                                <i class="icon-home"></i>
                                <span class="title">Home</span>
                            </a>
                        </li>
                        <!-- History -->
                        <li class="nav-item 
                                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                            " style="display:none;
                                        @else
                                            @if(strpos($route_name,"permohonan.index") !== false OR in_array($route_name, $route_permohonan)) 
                                                active 
                                            @endif
                                        @endhasanyrole
                                            ">
                            <a href="{{ route('permohonan.index') }}" class="nav-link nav-toggle">
                                <i class="icon-clock"></i>
                                <span class="title">Permohonan Saya</span>
                            </a>
                        </li>
                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                        <!-- Kelola Permohonan -->
                        <li class="nav-item @if(in_array($route_name, $route_kelola)) 
                                                active 
                                            @endif">
                            <a href="{{ route('permohonan.kelola') }}" class="nav-link nav-toggle">
                                <i class="fa fa-gear"></i>
                                <span class="title">&nbsp;Kelola Permohonan</span>
                            </a>
                        </li>
                        @endhasanyrole
                        <!-- Peminjaman Ruangan -->
                        <li class="nav-item @if(in_array($route_name, $route_ruangan)) active @endif">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-hospital-o"></i>
                                <span class="title">&nbsp;Ruangan</span>
                                <span class="arrow @if(in_array($route_name, $route_ruangan)) open @endif"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item 
                                            @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                                " style="display:none;
                                            @else
                                                @if($route_name == "pinjam_ruangan.create")
                                                    active 
                                                @endif
                                            @endhasanyrole
                                                ">
                                    <a href="{{ route('pinjam_ruangan.create') }}" class="nav-link ">
                                        <i class="icon-bulb"></i>
                                        <span class="title">Buat Peminjaman</span>
                                    </a>
                                </li>
                                <li class="nav-item @if($route_name == "ruangan.index") active @endif">
                                    <a href="{{ route('ruangan.index') }}" class="nav-link ">
                                        <i class="fa fa-home"></i>
                                        <span class="title">Data Ruangan</span>
                                    </a>
                                </li>
                                @hasanyrole('Administrator|Pengelola Urdal')
                                <li class="nav-item @if(in_array($route_name, ['ruangan.kelola', 'ruangan.edit' , 'ruangan.show', 'ruangan.create'])) active @endif">
                                    <a href="{{ route('ruangan.kelola') }}" class="nav-link ">
                                        <i class="fa fa-gear"></i>
                                        <span class="title">Kelola Ruangan</span>
                                    </a>
                                </li>
                                @endhasanyrole
                            </ul>
                        </li>
                        <!-- Peminjaman Kendaraan -->
                        <li class="nav-item @if(in_array($route_name, $route_kendaraan) OR strpos($route_name,"pengemudi") !== false) active @endif">
                            <a href="javascript:;" class="nav-link nav-toggle">
                                <i class="fa fa-car"></i>
                                <span class="title">&nbsp;Kendaraan</span>
                                <span class="arrow @if(in_array($route_name, $route_kendaraan) OR strpos($route_name,"pengemudi") !== false) open @endif"></span>
                            </a>
                            <ul class="sub-menu">
                                <li class="nav-item 
                                            @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                                " style="display:none;
                                            @else
                                                @if($route_name == "pinjam_kendaraan.create")
                                                    active 
                                                @endif
                                            @endhasanyrole
                                                ">
                                    <a href="{{ route('pinjam_kendaraan.create') }}" class="nav-link ">
                                        <i class="icon-bulb"></i>
                                        <span class="title">Buat Peminjaman</span>
                                    </a>
                                </li>
                                <li class="nav-item @if(strpos($route_name,"pengemudi") !== false) active @endif">
                                    <a href="{{ route('piket_pengemudi.index') }}" class="nav-link ">
                                        <i class="icon-calendar"></i>
                                        <span class="title">Piket Pengemudi</span>
                                    </a>
                                </li>
                                <li class="nav-item @if($route_name == "kendaraan.index") active @endif">
                                    <a href="{{ route('kendaraan.index') }}" class="nav-link ">
                                        <i class="fa fa-taxi"></i>
                                        <span class="title">Data Kendaraan</span>
                                    </a>
                                </li>
                                @hasanyrole('Administrator|Pengelola Kendaraan')
                                <li class="nav-item @if(in_array($route_name, ['kendaraan.kelola', 'kendaraan.edit' , 'kendaraan.show', 'kendaraan.create'])) active @endif">
                                    <a href="{{ route('kendaraan.kelola') }}" class="nav-link ">
                                        <i class="fa fa-gear"></i>
                                        <span class="title">Kelola Kendaraan</span>
                                    </a>
                                </li>
                                @endhasanyrole
                            </ul>
                        </li>
                        <!-- Dinas -->
                        <li class="nav-item
                                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                            " style="display:none;
                                        @else
                                            @if($route_name == "dinas.create")
                                                active 
                                            @endif
                                        @endhasanyrole
                                            ">
                            <a href="{{ route('dinas.create') }}" class="nav-link nav-toggle">
                                <i class="icon-wrench"></i>
                                <span class="title">Dinas KTLN</span>
                            </a>
                        </li>
                        <!-- Peminjaman Barang -->
                        <li class="nav-item
                                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                            " style="display:none;
                                        @else
                                            @if($route_name == "pinjam_barang.create")
                                                active 
                                            @endif
                                        @endhasanyrole
                                            ">
                            <a href="{{ route('pinjam_barang.create') }}" class="nav-link nav-toggle">
                                <i class="icon-social-dropbox"></i>
                                <span class="title">Peminjaman Barang</span>
                            </a>
                        </li>
                        <!-- Penggandaan -->
                        <li class="nav-item
                                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                            " style="display:none;
                                        @else
                                            @if($route_name == "penggandaan.create")
                                                active 
                                            @endif
                                        @endhasanyrole
                                            ">
                            <a href="{{ route('penggandaan.create') }}" class="nav-link nav-toggle">
                                <i class="fa fa-copy"></i>
                                <span class="title">&nbsp;Penggandaan</span>
                            </a>
                        </li>
                        <!-- Bensin -->
                        <li class="nav-item
                                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                            " style="display:none;
                                        @else
                                            @if($route_name == "bensin.create")
                                                active 
                                            @endif
                                        @endhasanyrole
                                            ">
                            <a href="{{ route('bensin.create') }}" class="nav-link nav-toggle">
                                <i class="fa fa-beer"></i>
                                <span class="title">&nbsp;Bensin</span>
                            </a>
                        </li>
                        <!-- Layanan Pengaduan -->
                        <li class="nav-item
                                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                            " style="display:none;
                                        @else
                                            @if($route_name == "saranaduan.create")
                                                active 
                                            @endif
                                        @endhasanyrole
                                            ">
                            <a href="{{ route('saranaduan.create') }}" class="nav-link nav-toggle">
                                <i class="icon-speech"></i>
                                <span class="title">Saran/Pengaduan</span>
                            </a>
                        </li>
                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                        <!-- Pengumuman -->
                        <li class="nav-item @if(strpos($route_name,"pengumuman") !== false) active @endif">
                            <a href="{{ route('pengumuman.edit') }}" class="nav-link nav-toggle">
                                <i class="fa fa-newspaper-o"></i>
                                <span class="title">&nbsp;Pengumuman</span>
                            </a>
                        </li>
                        @endhasanyrole
                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                        <!-- RunningText -->
                        <li class="nav-item @if(strpos($route_name,"runtext") !== false) active @endif">
                            <a href="{{ route('runtext.index') }}" class="nav-link nav-toggle">
                                <i class="fa fa-flag"></i>
                                <span class="title">&nbsp;Teks Berjalan</span>
                            </a>
                        </li>
                        @endhasanyrole
                        @role('Administrator')
                        <!-- Kelola User -->
                        <li class="nav-item @if(strpos($route_name,"user") !== false) active @endif">
                            <a href="{{ route('user.index') }}" class="nav-link nav-toggle">
                                <i class="icon-user"></i>
                                <span class="title">Akun Pengguna</span>
                            </a>
                        </li>
                        @endrole
                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                        <!-- Rekap -->
                        <li class="nav-item @if(strpos($route_name,"rekap") !== false) active @endif">
                            <a href="{{ route('rekap.index') }}" class="nav-link nav-toggle">
                                <i class="icon-paper-plane"></i>
                                <span class="title">Rekapitulasi</span>
                            </a>
                        </li>
                        @endhasanyrole
                    </ul>
                    <!-- END SIDEBAR MENU -->
                </div>
                <!-- END SIDEBAR -->
            </div>
            <!-- END SIDEBAR -->
            <!-- BEGIN CONTENT -->
            <div class="page-content-wrapper">
                @yield('content')
            </div>
            <!-- END CONTENT -->
        </div>
        <!-- END CONTAINER -->
        <!-- BEGIN FOOTER -->
        <div class="page-footer">
          <div class="page-footer-inner"> 2018 &copy; Urusan Dalam Badan Siber dan Sandi Negara
          </div>
        </div>
        <!--[if lt IE 9]>
        <script src="../assets/global/plugins/respond.min.js"></script>
        <script src="../assets/global/plugins/excanvas.min.js"></script>
        <script src="../assets/global/plugins/ie8.fix.min.js"></script>
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ asset('global/plugins/moment.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/morris/morris.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/morris/raphael-min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/counterup/jquery.waypoints.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/counterup/jquery.counterup.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/amcharts/amcharts/amcharts.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/amcharts/amcharts/serial.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/amcharts/amcharts/pie.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/amcharts/amcharts/radar.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/amcharts/amcharts/themes/light.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/amcharts/amcharts/themes/patterns.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/amcharts/amcharts/themes/chalk.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/amcharts/ammap/ammap.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/amcharts/ammap/maps/js/worldLow.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/amcharts/amstockcharts/amstock.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/fullcalendar/fullcalendar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/horizontal-timeline/horizontal-timeline.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/flot/jquery.flot.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/flot/jquery.flot.resize.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/flot/jquery.flot.categories.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery-easypiechart/jquery.easypiechart.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery.sparkline.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-daterangepicker/daterangepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/datatables/datatables.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery-repeater/jquery.repeater.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-fileinput/bootstrap-fileinput.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ asset('pages/scripts/dashboard.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('pages/scripts/components-date-time-pickers.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('pages/scripts/table-datatables-managed.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('pages/scripts/form-repeater.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('pages/scripts/components-select2.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <script src="{{ asset('layouts/layout4/scripts/layout.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('layouts/layout4/scripts/demo.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('layouts/global/scripts/quick-sidebar.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('layouts/global/scripts/quick-nav.min.js') }}" type="text/javascript"></script>
        <!-- END THEME LAYOUT SCRIPTS -->
        {{-- scripts --}}
        <script src="https://cdnjs.cloudflare.com/ajax/libs/socket.io/1.7.2/socket.io.min.js"></script>
        <script>
        // this is all it takes to capture it in jQuery
        // you put ready-snippet
        $(function() {
            //you define socket - you can use IP
            var socket = io('http://localhost:3000');
            //you capture message data
            socket.on('test-channel:App\\Events\\SocketEvent', function(message){
                //you append that data to DOM, so user can see it
                if(message.data.data == {{ Auth::user()->id }}){
                    location.reload();
                }
            });
        });
        </script>
        @yield('script')
    </body>
</html>
