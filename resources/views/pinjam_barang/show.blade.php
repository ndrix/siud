@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-sticky-note @role('Administrator') font-red-sunglo @else font-purple-plum @endrole"></i>
                            <span class="caption-subject @role('Administrator') font-red-sunglo @else font-purple-plum @endrole sbold uppercase">&nbsp; Rincian Permohonan </span>
                        </div>
                        <div class="actions">
                            @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                <a href="{{ route('permohonan.kelola') }}" class="btn default"><i class="fa  fa-chevron-circle-left"></i> Kembali</a>
                            @else
                                <a href="{{ route('permohonan.index') }}" class="btn default"><i class="fa  fa-chevron-circle-left"></i> Kembali</a>
                            @endhasanyrole
                        </div>
                    </div>
                    <div class="portlet-body">
                        @php 
                            $delete_dot_order_id = str_replace(".", "", $pinjam_barang->first()['order_id']); 
                            $replace_slash_order_id = str_replace("/", "-", $delete_dot_order_id); 
                            $encrypt_order_id = \Crypt::encryptString($replace_slash_order_id);
                        @endphp
                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Pengadaan|Pengelola Penggandaan')
                        @else    
                            @php    
                                foreach(Auth::user()->unreadnotifications as $n){
                                    if($n->data['order_id'] == $replace_slash_order_id){
                                        $n->markasread();
                                    };
                                };
                            @endphp
                        @endhasanyrole
                        <table class="table table-striped table-bordered table-hover">
                            <tbody> 
                                <tr>
                                    <td width="40%">Nomor</td>
                                    <td>{{ $pinjam_barang->first()['order_id'] }}</td>
                                </tr>
                                <tr>
                                    <td width="40%">Jenis</td>
                                    <td>
                                        Peminjaman Barang
                                    </td>
                                </tr>
                                <tr>
                                    @php
                                        $bulan = array (1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                                    @endphp
                                    <td width="40%">Tanggal</td>
                                    <td>{{ date('d ', strtotime($pinjam_barang->first()['waktu_mulai'])).$bulan[date('n', strtotime($pinjam_barang->first()['waktu_mulai']))].date(' Y', strtotime($pinjam_barang->first()['waktu_mulai'])).date(' (H:i)', strtotime($pinjam_barang->first()['waktu_mulai']))." s.d. ".date('d ', strtotime($pinjam_barang->first()['waktu_selesai'])).$bulan[date('n', strtotime($pinjam_barang->first()['waktu_selesai']))].date(' Y', strtotime($pinjam_barang->first()['waktu_selesai'])).date(' (H:i)', strtotime($pinjam_barang->first()['waktu_selesai'])) }}</td>
                                </tr>
                                <tr>
                                    <td width="40%">Item</td>
                                    <td>
                                        @foreach($pinjam_barang as $p)
                                            -&ensp;{{ ucwords($p->barang->barang) }}<br>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%">Kegiatan</td>
                                    <td>    
                                        {{ $pinjam_barang->first()['kegiatan'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%">Keterangan</td>
                                    <td>    
                                        {{ $pinjam_barang->first()['keterangan'] }}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%">Status</td>
                                    <td>
                                        @switch($pinjam_barang->first()['status_id'])
                                            @case(1)
                                                <span class="label label-danger"> {{ ucwords($pinjam_barang->first()->status->status) }} </span>
                                                @break
                                            @case(2)
                                                <span class="label label-warning"> {{ ucwords($pinjam_barang->first()->status->status) }} </span>
                                                @break
                                            @case(3)
                                                <span class="label label-success"> {{ ucwords($pinjam_barang->first()->status->status) }} </span>
                                                @break
                                            @case(4)
                                                <span class="label label-default"> {{ ucwords($pinjam_barang->first()->status->status) }} </span>
                                                @break        
                                        @endswitch
                                    </td>
                                </tr>
                                @if($pinjam_barang->first()['alasan'] != null)
                                    <tr>
                                        <td width="40%">Alasan</td>
                                        <td>{{ $pinjam_barang->first()['alasan'] }}</td>
                                    </tr>
                                @endif
                                @if($pinjam_barang->first()['nodin'] != null)
                                <tr>
                                    <td width="40%">Nodin</td>
                                    <td>
                                        <a href="{{ route('pinjam_barang.download_nodin', ['order_id' => $encrypt_order_id]) }}">Download Nodin</a>
                                    </td>
                                </tr>
                                @endif
                                {{-- @if($kelola == "no")
                                    @if($pinjam_barang->first()['status_id'] != 3 AND $pinjam_barang->first()['status_id'] != 4)
                                    <tr>
                                        <td width="40%">Unggah / Perbarui Nodin</td>
                                        <td>
                                            <form method="post" action="{{ route('pinjam_barang.upload_nodin', ['order_id' => $encrypt_order_id]) }}" enctype="multipart/form-data">
                                                @csrf
                                                <div class="fileinput fileinput-new" data-provides="fileinput">
                                                    <div class="input-group input-large">
                                                        <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                            <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                            <span class="fileinput-filename"> </span>
                                                        </div>
                                                        <span class="input-group-addon btn default btn-file">
                                                            <span class="fileinput-new"> Select file </span>
                                                            <span class="fileinput-exists"> Change </span>
                                                            <input type="file" name="nodin"> </span>
                                                        <a href="javascript:;" class="input-group-addon btn red fileinput-exists"> Remove </a>
                                                    </div>
                                                    <button type="submit" class="btn blue">Unggah</button>
                                                </div>
                                            </form>
                                        </td>
                                    </tr>
                                    @endif
                                @endif --}}
                            </tbody>
                        </table>
                        <center>
                            @if($kelola == "no")
                                @if($pinjam_barang->first()['status_id'] != 2 AND $pinjam_barang->first()['status_id'] != 3)
                                    <a href="{{ route('pinjam_barang.edit', ['order_id' => $encrypt_order_id]) }}" class="btn green">Ubah Data</a>
                                @endif
                            @else
                                <form method="post" action="{{ route('permohonan.setuju', ['order_id' => $encrypt_order_id]) }}">
                                    @csrf @method('put')
                                    <button class="btn blue">Setuju</button>
                                    <a href="{{ route('permohonan.tolak', ['order_id' => $encrypt_order_id]) }}" class="btn red">Tolak</a>
                                </form>
                            @endif
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection