@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file font-green"></i>
                            <span class="caption-subject font-green sbold uppercase">&nbsp; Form Peminjaman Barang </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form action="{{ route('pinjam_barang.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="form-body">
                                <div class="form-group">
                                    <label for="multiple" class="col-md-2 control-label">Barang</label>
                                    <div class="col-md-7">
                                        <div class="mt-repeater">
                                            <div data-repeater-list="barang">
                                                <div data-repeater-item class="row">
                                                    <div class="col-md-7">
                                                        <select class="form-control" name="barang_id">
                                                            @foreach($barang as $n)
                                                                <option value="{{ $n->id }}">{{ ucwords($n->barang) }}</option>
                                                            @endforeach
                                                            <option value="16">Lainnya</option>
                                                        </select>
                                                    </div>
                                                    <div class="col-md-3">
                                                        <input type="text" class="form-control" value="1" name="jumlah" oninput="this.value = this.value.replace(/[^1-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="1" />
                                                    </div>
                                                    <div class="col-md-1">
                                                        <a href="javascript:;" data-repeater-delete class="btn btn-danger">
                                                            <i class="fa fa-close"></i>
                                                        </a>
                                                    </div>
                                                </div>
                                            </div>
                                            <hr>
                                            <a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                                <i class="fa fa-plus"></i> Tambah</a>
                                            <br>
                                            <br>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Waktu</label>
                                    <div class="col-md-3">
                                        <input name="waktu_mulai" class="form-control form-control-inline date form_datetime form_datetime bs-datetime" size="16" type="text" value="{{ old('waktu_mulai') }}" />
                                    </div>
                                    <div class="col-md-1">
                                        <center><label class="control-label">s.d.</label></center>
                                    </div>
                                    <div class="col-md-3">
                                        <input name="waktu_selesai" class="form-control form-control-inline date form_datetime form_datetime bs-datetime" size="16" type="text" value="{{ old('waktu_selesai') }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Satker</label>
                                    <div class="col-md-2">
                                        <select class="form-control select2" name="satker_id">
                                            @foreach($satker as $n)
                                                @if(old('satker_id') == $n->id)
                                                    <option value="{{ $n->id }}" selected>{{ ucwords($n->satker) }}</option>
                                                @else
                                                    <option value="{{ $n->id }}">{{ ucwords($n->satker) }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Kegiatan</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="kegiatan" value="{{ old('kegiatan') }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Keterangan</label>
                                    <div class="col-md-7">
                                        <textarea class="form-control" rows="3" name="keterangan">{{ old('keterangan') }}</textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Scan Nodin</label>
                                    <div class="col-md-2">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select file </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="nodin"> 
                                                </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <hr>
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection