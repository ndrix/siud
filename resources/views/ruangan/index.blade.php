@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-hospital-o font-green"></i>
                            <span class="caption-subject font-green sbold uppercase">&nbsp; Data Ruangan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            @php
                                if(app('request')->input() != null){
                                    $ruangan = App\Ruangan::where('id', app('request')->input('ruangan'))->first();
                                }
                            @endphp
                            <form action="{{ route('ruangan.filter') }}" method="post" class="form-horizontal">
                                @csrf
                                <div class="col-md-2">
                                    <h4>Pilih Ruangan</h4>
                                </div>
                                <div class="form-body">
                                    <div class="form-group">
                                        <div class="col-md-5">
                                            <select class="form-control" name="ruangan_id">
                                                @foreach($ruangan_all as $n)
                                                    @if($ruangan->id == $n->id)
                                                        <option value="{{ $n->id }}" selected>{{ ucwords($n->nama) }}</option>
                                                    @else
                                                        <option value="{{ $n->id }}">{{ ucwords($n->nama) }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        </div>
                                        <div class="col-md-1">
                                            <button type="submit" class="btn btn green">Submit</button>
                                        </div>
                                    </div>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-home font-red-sunglo"></i>
                            <span class="caption-subject font-red-sunglo sbold uppercase">&nbsp; {{ $ruangan->nama }} </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                        <div class="col-md-8">
                        <table>
                            <tr>
                                <td width="90px" height="40px"><b>Fasilitas</b></td>
                                <td width="40px"><center>:</center></td>
                                <td>{{ $ruangan->fasilitas }}</td>
                            </tr>
                            <tr>
                                <td width="90px" height="40px"><b>Lokasi</b></td>
                                <td width="40px"><center>:</center></td>
                                <td>{{ $ruangan->lokasi }}</td>
                            </tr>
                            <tr>
                                <td width="90px" height="40px"><b>Kapasitas</b></td>
                                <td width="40px"><center>:</center></td>
                                <td>{{ $ruangan->kapasitas }}</td>
                            </tr>
                            <tr>
                                <td width="90px" height="40px"><b>Keterangan</b></td>
                                <td width="40px"><center>:</center></td>
                                <td>{{ $ruangan->keterangan }}</td>
                            </tr>
                        </table>
                        </div>
                        {{-- <div class="col-md-5"> --}}
                            <div class="col-sm-6 col-md-4">
                                <a href="javascript:;" class="thumbnail">
                                    <img src="{{ asset('storage/'.$ruangan->image) }}" alt="100%x180" style="height: 180px; width: 100%; display: block;"> </a>
                            </div>
                            {{-- <center><img src="{{ asset('storage/'.$ruangan->image) }}" class="img-responsive" width="100%"></center> --}}
                        {{-- </div> --}}
                        </div>
                    </div>
                </div>
            </div>
            {{-- <div class="col-md-5">
                <div class="portlet light portlet-fit">
                    <center><img src="{{ asset('storage/'.$ruangan->image) }}" class="img-responsive" width="100%"></center>
                </div>
            </div> --}}
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar font-purple-plum"></i>
                            <span class="caption-subject font-purple-plum sbold uppercase">&nbsp; Jadwal Penggunaan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th width="8%">No</th>
                                    <th width="15%">Tanggal Mulai</th>
                                    <th width="15%">Waktu Mulai</th>
                                    <th width="15%">Tanggal Selesai</th>
                                    <th width="15%">Waktu Selesai</th>
                                    <th width="20%">Kegiatan</th>
                                    <th width="12%">Satker</th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $entry_table = App\PinjamRuangan::where('ruangan_id', $ruangan->id)->where('status_id',2)->orderByDesc('waktu_mulai')->get();
                                    $bulan = array (1 => 'Januari',
                                        'Februari',
                                        'Maret',
                                        'April',
                                        'Mei',
                                        'Juni',
                                        'Juli',
                                        'Agustus',
                                        'September',
                                        'Oktober',
                                        'November',
                                        'Desember'
                                    );
                                @endphp
                                @foreach($entry_table as $n)
                                    <tr>
                                        <td style="font-size:12px" align="center">{{ $loop->iteration }}</td>
                                        <td style="font-size:12px">{{ date('d ', strtotime($n->waktu_mulai)).$bulan[date('n', strtotime($n->waktu_mulai))].date(' Y', strtotime($n->waktu_mulai)) }}</td>
                                        <td style="font-size:12px">{{ date('H : i', strtotime($n->waktu_mulai)) }}</td>
                                        <td style="font-size:12px">{{ date('d ', strtotime($n->waktu_selesai)).$bulan[date('n', strtotime($n->waktu_selesai))].date(' Y', strtotime($n->waktu_selesai)) }}</td>
                                        <td style="font-size:12px">{{ date('H : i', strtotime($n->waktu_selesai)) }}</td>
                                        <td style="font-size:12px">{{ $n->kegiatan }}</td>
                                        <td style="font-size:12px">{{ $n->satker->satker }}</td>
                                    </tr>        
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection