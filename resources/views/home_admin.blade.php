@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <!-- BEGIN PAGE BASE CONTENT -->
        <!-- BEGIN CONTENT HEADER -->
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        <div class="row margin-bottom-30 about-header">
            <div class="col-md-12">
                <h1>{{ Auth::user()->getRoleNames()->first() }}</h1><h2>Sistem Informasi Urusan Dalam 2.0</h2>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-green-sharp">
                                <span data-counter="counterup" data-value="{{ $total_permohonan }}">0</span>
                                {{-- <small class="font-green-sharp">$</small> --}}
                            </h3>
                            <small>Total Permohonan</small>
                        </div>
                        <div class="icon">
                            <i class="icon-pie-chart"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: 100%;" class="progress-bar progress-bar-success green-sharp">
                                <span class="sr-only">100%</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"> Persentase </div>
                            <div class="status-number"> 100% </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-red-haze">
                                <span data-counter="counterup" data-value="{{ $permohonan_disetujui }}">0</span>
                            </h3>
                            <small>Jumlah Disetujui</small>
                        </div>
                        <div class="icon">
                            <i class="icon-like"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: @if($total_permohonan != 0) {{ $permohonan_disetujui / $total_permohonan * 100 }} @else 0 @endif%;" class="progress-bar progress-bar-success red-haze">
                                <span class="sr-only">@if($total_permohonan != 0) {{ $permohonan_disetujui / $total_permohonan * 100 }} @else 0 @endif%</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"> Persentase </div>
                            <div class="status-number"> @if($total_permohonan != 0) {{ $permohonan_disetujui / $total_permohonan * 100 }} @else 0 @endif% </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-blue-sharp">
                                <span data-counter="counterup" data-value="{{ $permohonan_ditolak }}"></span>
                            </h3>
                            <small>Jumlah Ditolak</small>
                        </div>
                        <div class="icon">
                            <i class="icon-basket"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: @if($total_permohonan != 0) {{ $permohonan_ditolak / $total_permohonan * 100 }} @else 0 @endif%" class="progress-bar progress-bar-success blue-sharp">
                                <span class="sr-only"> @if($total_permohonan != 0) {{ $permohonan_ditolak / $total_permohonan * 100 }} @else 0 @endif%</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"> Persentase </div>
                            <div class="status-number"> @if($total_permohonan != 0) {{ $permohonan_ditolak / $total_permohonan * 100 }} @else 0 @endif% </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="dashboard-stat2 bordered">
                    <div class="display">
                        <div class="number">
                            <h3 class="font-purple-soft">
                                <span data-counter="counterup" data-value="{{ $permohonan_menunggu }}"></span>
                            </h3>
                            <small>Jumlah Menunggu</small>
                        </div>
                        <div class="icon">
                            <i class="icon-user"></i>
                        </div>
                    </div>
                    <div class="progress-info">
                        <div class="progress">
                            <span style="width: @if($total_permohonan != 0) {{ $permohonan_menunggu / $total_permohonan * 100 }} @else 0 @endif%;" class="progress-bar progress-bar-success purple-soft">
                                <span class="sr-only">@if($total_permohonan != 0) {{ $permohonan_menunggu / $total_permohonan * 100 }} @else 0 @endif%</span>
                            </span>
                        </div>
                        <div class="status">
                            <div class="status-title"> Persentase </div>
                            <div class="status-number"> @if($total_permohonan != 0) {{ $permohonan_menunggu / $total_permohonan * 100 }} @else 0 @endif% </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{-- <div class="row">
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption ">
                            <span class="caption-subject font-dark bold uppercase">Jumlah Permohonan Tiap Bulan</span>
                            <span class="caption-helper"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <canvas id="ChartOne" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
            <div class="col-lg-6 col-xs-12 col-sm-12">
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption ">
                            <span class="caption-subject font-dark bold uppercase">Jumlah Permohonan Tiap Jenis</span>
                            <span class="caption-helper"></span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <canvas id="ChartTwo" width="400" height="400"></canvas>
                    </div>
                </div>
            </div>
        </div> --}}
        <!-- END CONTENT HEADER -->
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
@endsection
@section('script')
    <script src="{{ asset('pages/scripts/charts-chartjsbundle.min.js') }}"></script>
    <script>
        var ctx = document.getElementById("ChartTwo");
        var chartTwo = new Chart(ctx, {
            type: 'pie',
            data: {
                labels: [
                    'Ruangan', 'Kendaraan', 'Perbaikan', 'Peminjaman Barang', 'Penggandaan', 'Saran Aduan'
                ],
                datasets: [{
                        data: [
                            <?php
                                $ruangan            = \App\PinjamRuangan::where('status_id', '!=', 1)->count();
                                $kendaraan          = \App\PinjamKendaraan::where('status_id', '!=', 1)->count();
                                $perbaikan          = count(\App\PerbaikanBarang::where('status_id', '!=', 1)->groupBy('order_id')->get());
                                $pinjam_barang      = count(\App\PinjamBarang::where('status_id', '!=', 1)->groupBy('order_id')->get());
                                $penggandaan        = count(\App\Penggandaan::where('status_id', '!=', 1)->groupBy('order_id')->get());
                                $saran_aduan        = \App\SaranAduan::where('status_id', '!=', 1)->count();
                                echo "'".$ruangan."'".","."'".$kendaraan."'".","."'".$perbaikan."'".","."'".$pinjam_barang."'".","
                                    ."'".$penggandaan."'".","."'".$saran_aduan."'";
                            ?>
                        ],
                        backgroundColor: [ 
                            <?php
                                for($i=0;$i<6;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderColor: [
                            <?php
                                for($i=0;$i<6;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                            ticks: {
                                beginAtZero: true
                            },
                            gridLines: {
                                display: false
                            },
                            display: false,
                        }]
                }
            }
        });
    </script>
    <script>
        var ctx = document.getElementById("ChartOne");
        var chartOne = new Chart(ctx, {
            type: 'bar',
            data: {
                labels: [
                    <?php
                        $bulan = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
                        foreach($bulan as $b){
                            echo '"'.$b.'"'.","; 
                        }
                    ?>
                ],
                datasets: [{
                        label: 'Jumlah',
                        data: [
                            <?php
                                for($i=1; $i<=12; $i++){
                                    $ruangan            = \App\PinjamRuangan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->count();
                                    $kendaraan          = \App\PinjamKendaraan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->count();
                                    $perbaikan          = count(\App\PerbaikanBarang::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->get());
                                    $pinjam_barang      = count(\App\PinjamBarang::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->get());
                                    $penggandaan        = count(\App\Penggandaan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->get());
                                    $saran_aduan        = \App\SaranAduan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->count();
                                    $jml_permohonan     = $ruangan + $kendaraan + $perbaikan + $pinjam_barang + $penggandaan + $saran_aduan;
                                    echo "'".$jml_permohonan."',";
                                }
                            ?>
                        ],
                        backgroundColor: [ 
                            <?php
                                for($i=0;$i<12;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderColor: [
                            <?php
                                for($i=0;$i<12;$i++){
                                    $color  = "'"."rgba(".rand(0,255).", ".rand(0,255).", ".rand(0,255).", 0.2)',";
                                    echo $color;
                                }
                            ?>
                        ],
                        borderWidth: 1
                    }]
            },
            options: {
                responsive: true,
                maintainAspectRatio: false,
                scales: {
                    yAxes: [{
                        ticks: {
                            beginAtZero: true,
                        },
                        gridLines: {
                            display: true
                        },
                        display: true,
                    }],
                    xAxes: [{
                        ticks: {
                            autoSkip: false
                        },
                    }]
                }
            }
        });
    </script>
@endsection
