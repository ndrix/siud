@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-sticky-note font-red-sunglo"></i>
                            <span class="caption-subject font-red-sunglo sbold uppercase">&nbsp; Ubah Akun Pengguna </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        @php
                            use Spatie\Permission\Models\Role;
                            $encrypt_id = \Crypt::encryptString($user->id);
                        @endphp
                        <form method="post" action="{{ route('user.update', ['id' => $encrypt_id]) }}">
                            @csrf @method('put')
                            <table class="table table-striped table-bordered table-hover">
                                <tbody>
                                    <tr>
                                        <td width="40%">Nama</td>
                                        <td>
                                            <input type="text" class="form-control" name="name" value="{{ $user->name }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">Email</td>
                                        <td>
                                            <input type="text" class="form-control" name="email" value="{{ $user->email }}">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">Password</td>
                                        <td>
                                            <input type="password" class="form-control" name="password" placeholder="Hanya diisi ketika ingin mengubah password">
                                        </td>
                                    </tr>
                                    <tr>
                                        <td width="40%">Hak Akses</td>
                                        <td>    
                                            <select name="hak_akses" class="form-control">
                                                @foreach(Role::all() as $n)
                                                    @if($n->name == $user->getRoleNames()->first())
                                                        <option value="{{ $n->name }}" selected>{{ $n->name }}</option>
                                                    @else
                                                        <option value="{{ $n->name }}">{{ $n->name }}</option>
                                                    @endif
                                                @endforeach
                                                <option value="">User</option>
                                            </select>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <center>
                                <button type="submit" class="btn blue">Simpan</button>
                                <a href="{{ route('user.index') }}" class="btn default">Kembali</a>
                            </center>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
