@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <!-- BEGIN PAGE BASE CONTENT -->
        <!-- BEGIN CONTENT HEADER -->
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        <div class="row margin-bottom-30 about-header">
            <div class="col-md-12">
                <h1>Selamat Datang</h1><h2>Sistem Informasi Urusan Dalam 2.0</h2>
            </div>
        </div>
        <!-- END CONTENT HEADER -->
        <!-- BEGIN CARDS -->
        <div class="row margin-bottom-10">
            <div class="col-lg-3 col-md-6">
                <div class="portlet light">
                    <div class="card-icon">
                        <i class="icon-user-follow font-red-sunglo theme-font"></i>
                    </div>
                    <div class="card-title">
                        <span> UX + </span>
                    </div>
                    <div class="card-desc">
                        <span> SIUD 2.0 tampil dengan desain baru yang memudahkan anda berinteraksi dengan aplikasi. </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="portlet light">
                    <div class="card-icon">
                        <i class="icon-trophy font-green-haze theme-font"></i>
                    </div>
                    <div class="card-title">
                        <span> Kepuasan </span>
                    </div>
                    <div class="card-desc">
                        <span> Mengutamakan kepuasan Anda terhadap layanan yang diberikan. <br><br><br> </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="portlet light">
                    <div class="card-icon">
                        <i class="icon-basket font-purple-wisteria theme-font"></i>
                    </div>
                    <div class="card-title">
                        <span> Pelayanan </span>
                    </div>
                    <div class="card-desc">
                        <span> Selalu berupaya untuk memenuhi kebutuhan Anda terhadap layanan on-demand yang mudah dan cepat. </span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="portlet light">
                    <div class="card-icon">
                        <i class="icon-layers font-blue theme-font"></i>
                    </div>
                    <div class="card-title">
                        <span> Kontinyu </span>
                    </div>
                    <div class="card-desc">
                        <span> Pengembangan secara kontinyu guna memenuhi kebutuhan Anda yang terus berkembang. <br><br></span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END CARDS -->
        <!-- BEGIN PENGUMUMAN -->
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-info font-green-sharp"></i>
                            <span class="caption-subject font-green-sharp bold uppercase">Pengumuman</span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <p class="margin-top-20">
                            <font size="3"> 
                                {{ $pengumuman->pengumuman }}
                            </font>
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <!-- END PENGUMUMAN -->
        {{-- <!-- BEGIN MEMBERS SUCCESS STORIES -->
        <div class="row margin-bottom-10 stories-header" data-auto-height="true">
            <div class="col-md-12">
                <h1>Contact Person</h1>
                <h2>Jangan Ragu Untuk Menghubungi Kami Apabila Terdapat Pertanyaan</h2>
            </div>
        </div>
        <div class="row margin-bottom-10 stories-cont">
            <div class="col-lg-3 col-md-6">
                <div class="portlet light">
                    <div class="photo">
                        <img src="{{ asset('pages/media/users/teambg2.jpg') }}" alt="" class="img-responsive" />
                    </div>
                    <div class="title">
                        <span> Wakingah </span>
                    </div>
                    <div class="desc">
                        <span> Anggota Subbagian Urusan Dalam </span>
                    </div>
                    <div class="desc">
                        <i class="fa fa-phone"></i>
                        <span>&ensp;081 286 272 588</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="portlet light">
                    <div class="photo">
                        <img src="{{ asset('pages/media/users/teambg1.jpg') }}" alt="" class="img-responsive" />
                    </div>
                    <div class="title">
                        <span> Nanda </span>
                    </div>
                    <div class="desc">
                        <span> Anggota Subbagian Pemeliharaan Sarana dan Prasarana </span>
                    </div>
                    <div class="desc">
                        <i class="fa fa-phone"></i>
                        <span>&ensp;082 112 612 279</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="portlet light">
                    <div class="photo">
                        <img src="{{ asset('pages/media/users/teambg5.jpg') }}" alt="" class="img-responsive" />
                    </div>
                    <div class="title">
                        <span> Wisnu Hadi N </span>
                    </div>
                    <div class="desc">
                        <span> Kepala Subbagian Urusan Dalam </span>
                    </div>
                    <div class="desc">
                        <i class="fa fa-phone"></i>
                        <span>&ensp;081 318 101 860</span>
                    </div>
                </div>
            </div>
            <div class="col-lg-3 col-md-6">
                <div class="portlet light">
                    <div class="photo">
                        <img src="{{ asset('pages/media/users/teambg8.jpg') }}" alt="" class="img-responsive" />
                    </div>
                    <div class="title">
                        <span> Nanang Indarjo </span>
                    </div>
                    <div class="desc">
                        <span> Kepala Subbagian Pemeliharaan Sarana dan Prasarana </span>
                    </div>
                    <div class="desc">
                        <i class="fa fa-phone"></i>
                        <span>&ensp;081 311 083 752</span>
                    </div>
                </div>
            </div>
        </div>
        <!-- END MEMBERS SUCCESS STORIES --> --}}
        <!-- END PAGE BASE CONTENT -->
    </div>
    <!-- END CONTENT BODY -->
@endsection
