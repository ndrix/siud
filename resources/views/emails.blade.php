<!doctype html>
<html>
<head>
    <meta name="viewport" content="width=device-width" />
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
    <title>Boardicle Email</title>
    <style>
        /* -------------------------------------
            GLOBAL RESETS
        ------------------------------------- */
        img {
            border: none;
            -ms-interpolation-mode: bicubic;
            max-width: 100%; }
        body {
            background-color: #f6f6f6;
            font-family: sans-serif;
            -webkit-font-smoothing: antialiased;
            font-size: 14px;
            line-height: 1.4;
            margin: 0;
            padding: 0;
            -ms-text-size-adjust: 100%;
            -webkit-text-size-adjust: 100%; }
        table {
            border-collapse: separate;
            mso-table-lspace: 0pt;
            mso-table-rspace: 0pt;
            width: 100%; }
        table td {
            font-family: sans-serif;
            font-size: 14px;
            vertical-align: top; }
        /* -------------------------------------
            BODY & CONTAINER
        ------------------------------------- */
        .body {
            background-color: #f6f6f6;
            width: 100%; }
        /* Set a max-width, and make it display as block so it will automatically stretch to that width, but will also shrink down on a phone or something */
        .container {
            display: block;
            Margin: 0 auto !important;
            /* makes it centered */
            max-width: 580px;
            padding: 10px;
            width: 580px; }
        /* This should also be a block element, so that it will fill 100% of the .container */
        .content {
            box-sizing: border-box;
            display: block;
            Margin: 0 auto;
            max-width: 580px;
            padding: 10px; }
        /* -------------------------------------
            HEADER, FOOTER, MAIN
        ------------------------------------- */
        .main {
            background: #ffffff;
            border-radius: 3px;
            width: 100%; }
        .wrapper {
            box-sizing: border-box;
            padding: 20px; }
        .content-block {
            padding-bottom: 10px;
            padding-top: 10px;
        }
        .footer {
            clear: both;
            Margin-top: 10px;
            text-align: center;
            width: 100%; }
        .footer td,
        .footer p,
        .footer span,
        .footer a {
            color: #999999;
            font-size: 12px;
            text-align: center; }
        /* -------------------------------------
            TYPOGRAPHY
        ------------------------------------- */
        h1,
        h2,
        h3,
        h4 {
            color: #000000;
            font-family: sans-serif;
            font-weight: 400;
            line-height: 1.4;
            margin: 0;
            Margin-bottom: 30px; }
        h1 {
            font-size: 35px;
            font-weight: 300;
            text-align: center;
            text-transform: capitalize; }
        p,
        ul,
        ol {
            font-family: sans-serif;
            font-size: 14px;
            font-weight: normal;
            margin: 0;
            Margin-bottom: 15px; }
        p li,
        ul li,
        ol li {
            list-style-position: inside;
            margin-left: 5px; }
        a {
            color: #3498db;
            text-decoration: underline; }
        /* -------------------------------------
            BUTTONS
        ------------------------------------- */
        .btn {
            box-sizing: border-box;
            width: 100%; }
        .btn > tbody > tr > td {
            padding-bottom: 15px; }
        .btn table {
            width: auto; }
        .btn table td {
            background-color: #ffffff;
            border-radius: 5px;
            text-align: center; }
        .btn a {
            background-color: #ffffff;
            border: solid 1px #3498db;
            border-radius: 5px;
            box-sizing: border-box;
            color: #3498db;
            cursor: pointer;
            display: inline-block;
            font-size: 14px;
            font-weight: bold;
            margin: 0;
            padding: 12px 25px;
            text-decoration: none;
            text-transform: capitalize; }
        .btn-primary table td {
            background-color: #3498db; }
        .btn-primary a {
            background-color: #3498db;
            border-color: #3498db;
            color: #ffffff; }
        /* -------------------------------------
            OTHER STYLES THAT MIGHT BE USEFUL
        ------------------------------------- */
        .last {
            margin-bottom: 0; }
        .first {
            margin-top: 0; }
        .align-center {
            text-align: center; }
        .align-right {
            text-align: right; }
        .align-left {
            text-align: left; }
        .clear {
            clear: both; }
        .mt0 {
            margin-top: 0; }
        .mb0 {
            margin-bottom: 0; }
        .preheader {
            color: transparent;
            display: none;
            height: 0;
            max-height: 0;
            max-width: 0;
            opacity: 0;
            overflow: hidden;
            mso-hide: all;
            visibility: hidden;
            width: 0; }
        .powered-by a {
            text-decoration: none; }
        hr {
            border: 1;
            border-bottom: 1px solid #f6f6f6;
            Margin: 20px 0; }
        /* -------------------------------------
            RESPONSIVE AND MOBILE FRIENDLY STYLES
        ------------------------------------- */
        @media only screen and (max-width: 620px) {
            table[class=body] h1 {
                font-size: 28px !important;
                margin-bottom: 10px !important; }
            table[class=body] p,
            table[class=body] ul,
            table[class=body] ol,
            table[class=body] td,
            table[class=body] span,
            table[class=body] a {
                font-size: 16px !important; }
            table[class=body] .wrapper,
            table[class=body] .article {
                padding: 10px !important; }
            table[class=body] .content {
                padding: 0 !important; }
            table[class=body] .container {
                padding: 0 !important;
                width: 100% !important; }
            table[class=body] .main {
                border-left-width: 0 !important;
                border-radius: 0 !important;
                border-right-width: 0 !important; }
            table[class=body] .btn table {
                width: 100% !important; }
            table[class=body] .btn a {
                width: 100% !important; }
            table[class=body] .img-responsive {
                height: auto !important;
                max-width: 100% !important;
                width: auto !important; }}
        /* -------------------------------------
            PRESERVE THESE STYLES IN THE HEAD
        ------------------------------------- */
        @media all {
            .ExternalClass {
                width: 100%; }
            .ExternalClass,
            .ExternalClass p,
            .ExternalClass span,
            .ExternalClass font,
            .ExternalClass td,
            .ExternalClass div {
                line-height: 100%; }
            .apple-link a {
                color: inherit !important;
                font-family: inherit !important;
                font-size: inherit !important;
                font-weight: inherit !important;
                line-height: inherit !important;
                text-decoration: none !important; }
            .btn-primary table td:hover {
                background-color: #34495e !important; }
            .btn-primary a:hover {
                background-color: #34495e !important;
                border-color: #34495e !important; } }
    </style>
</head>
<body class="">
<table border="0" cellpadding="0" cellspacing="0" class="body">
    <tr>
        <td>&nbsp;</td>
        <td class="container">
            <div class="content">

                <!-- START CENTERED WHITE CONTAINER -->
                {{-- <span class="preheader">Email Untuk Anda!</span> --}}
                <table class="main">

                    <!-- START MAIN CONTENT AREA -->
                    <tr>
                        <td class="wrapper">
                            @php 
                                if($permohonan->count() > 1){
                                    $substr = substr($permohonan->first()->order_id, strpos($permohonan->first()->order_id,"SIUD.")+strlen("SIUD."),strlen($permohonan->first()->order_id));
                                } else{
                                    $substr = substr($permohonan->first()->order_id, strpos($permohonan->first()->order_id,"SIUD.")+strlen("SIUD."),strlen($permohonan->first()->order_id));
                                } 
                                $jenis  = substr($substr,0,strpos($substr,"/"));
                                $bulan = array (1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                            @endphp
                            <table border="0" cellpadding="0" cellspacing="0">
                                <tr>
                                    <td>
                                        <p>Hai, 
                                            @if($jenis == 3 OR $jenis == 4 OR $jenis == 5)
                                                {{ ucwords($permohonan->first()->user->name) }}
                                            @else
                                                {{ ucwords($permohonan->user->name) }}
                                            @endif
                                        </p>

                                        <table border="0" cellpadding="0" cellspacing="0">
                                            <tbody>
                                            <tr>
                                                <td align="left">
                                                    <table border="0" cellpadding="0" cellspacing="0">
                                                        <tbody>
                                                        <tr>
                                                            <td> <div class="container">
                                                                    <center><p>Permohonan Anda :</p></center>
                                                                    <hr>
                                                                    <center>
                                                                        @switch($jenis)
                                                                            @case(1)
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Nomor</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->order_id }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Jenis</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>Peminjaman Ruangan</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Tanggal</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ date('d ', strtotime($permohonan->waktu_mulai)).$bulan[date('n', strtotime($permohonan->waktu_mulai))].date(' Y', strtotime($permohonan->waktu_mulai)).date(' (H:i)', strtotime($permohonan->waktu_mulai))." s.d. ".date('d ', strtotime($permohonan->waktu_selesai)).$bulan[date('n', strtotime($permohonan->waktu_selesai))].date(' Y', strtotime($permohonan->waktu_selesai)).date(' (H:i)', strtotime($permohonan->waktu_selesai)) }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Item</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->ruangan->nama }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Kegiatan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->kegiatan }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Jml Pengguna</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->jml_pengguna }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Layout</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ ucwords($permohonan->layout) }}</td>
                                                                                    </tr>
                                                                                    @if($permohonan->status_id == 4)
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Alasan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->alasan }}</td>
                                                                                    </tr>
                                                                                    @endif
                                                                                </table>
                                                                                @break
                                                                            @case(2)
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Nomor</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->order_id }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Jenis</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>Peminjaman Kendaraan</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Tanggal</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ date('d ', strtotime($permohonan->waktu_mulai)).$bulan[date('n', strtotime($permohonan->waktu_mulai))].date(' Y', strtotime($permohonan->waktu_mulai)).date(' (H:i)', strtotime($permohonan->waktu_mulai))." s.d. ".date('d ', strtotime($permohonan->waktu_selesai)).$bulan[date('n', strtotime($permohonan->waktu_selesai))].date(' Y', strtotime($permohonan->waktu_selesai)).date(' (H:i)', strtotime($permohonan->waktu_selesai)) }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Item</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ ucwords($permohonan->kendaraan->nama) }} ( {{ $permohonan->kendaraan->kapasitas }} ) - {{ $permohonan->kendaraan->nomor_polisi }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Kegiatan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->kegiatan }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Tujuan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->tujuan }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Jml Penumpang</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->jml_penumpang }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Pengemudi</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ ucwords($permohonan->dukungan_pengemudi) }}</td>
                                                                                    </tr>
                                                                                    @if($permohonan->status_id == 4)
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Alasan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->alasan }}</td>
                                                                                    </tr>
                                                                                    @endif
                                                                                </table>
                                                                                @break
                                                                            @case(3)
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Nomor</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->order_id }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Jenis</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>Permohonan Dinas KTLN</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Tanggal</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ date('d ', strtotime($permohonan->first()->created_at)).$bulan[date('n', strtotime($permohonan->first()->created_at))].date(' Y', strtotime($permohonan->first()->created_at)) }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Kegiatan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->kegiatan }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Lokasi</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->lokasi }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Peserta</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>
                                                                                            -&ensp;{{ ucwords($permohonan->first()->peserta) }}<br>
                                                                                        </td>
                                                                                    </tr>
                                                                                    @if($permohonan->first()->status_id == 4)
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Alasan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->alasan }}</td>
                                                                                    </tr>
                                                                                    @endif
                                                                                </table>
                                                                                @break
                                                                            @case(4)
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Nomor</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->order_id }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Jenis</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>Peminjaman Barang</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Tanggal</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ date('d ', strtotime($permohonan->first()->waktu_mulai)).$bulan[date('n', strtotime($permohonan->first()->waktu_mulai))].date(' Y', strtotime($permohonan->first()->waktu_mulai)).date(' (H:i)', strtotime($permohonan->first()->waktu_mulai))." s.d. ".date('d ', strtotime($permohonan->first()->waktu_selesai)).$bulan[date('n', strtotime($permohonan->first()->waktu_selesai))].date(' Y', strtotime($permohonan->first()->waktu_selesai)).date(' (H:i)', strtotime($permohonan->first()->waktu_selesai)) }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Item</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>
                                                                                            @foreach($permohonan as $p)
                                                                                                -&ensp;{{ ucwords($p->barang->barang) }}<br>
                                                                                            @endforeach
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Satker</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->satker->satker }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Kegiatan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->kegiatan }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Keterangan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->keterangan }}</td>
                                                                                    </tr>
                                                                                    @if($permohonan->first()->status_id == 4)
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Alasan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->alasan }}</td>
                                                                                    </tr>
                                                                                    @endif
                                                                                </table>
                                                                                @break
                                                                            @case(5)
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Nomor</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->order_id }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Jenis</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>Penggandaan</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Tanggal</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ date('d ', strtotime($permohonan->first()->created_at)).$bulan[date('n', strtotime($permohonan->first()->created_at))].date(' Y', strtotime($permohonan->first()->created_at)) }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Item</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>
                                                                                            @foreach($permohonan as $p)
                                                                                                -&ensp;{{ ucwords($p->judul) }} ({{ $p->kertas }}) | Warna : {{ $p->hlm_warna }}, Rangkap : {{ $p->jml_warna }} | Hitam : {{ $p->hlm_hitam_putih }}, Rangkap : {{ $p->jml_hitam_putih }}<br>
                                                                                            @endforeach
                                                                                        </td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Keterangan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>
                                                                                            @foreach($permohonan as $p)
                                                                                                {{ $p->keterangan }}&nbsp;|&nbsp;
                                                                                            @endforeach
                                                                                        </td>
                                                                                    </tr>
                                                                                    @if($permohonan->first()->status_id == 4)
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Alasan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->first()->alasan }}</td>
                                                                                    </tr>
                                                                                    @endif
                                                                                </table>
                                                                                @break
                                                                            @case(6)
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Nomor</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->order_id }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Jenis</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>Bensin</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Tanggal</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ date('d ', strtotime($permohonan->created_at)).$bulan[date('n', strtotime($permohonan->created_at))].date(' Y', strtotime($permohonan->created_at)) }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Satker</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->satker->satker }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Jabatan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->jabatan }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Total Penggunaan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->total_penggunaan }}</td>
                                                                                    </tr>
                                                                                    @if($permohonan->status_id == 4)
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Alasan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->alasan }}</td>
                                                                                    </tr>
                                                                                    @endif
                                                                                </table>
                                                                                @break
                                                                            @case(7)
                                                                                <table border="0">
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Nomor</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->order_id }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Jenis</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ ucwords($permohonan->jenis) }}</td>
                                                                                    </tr>
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Saran</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->isian }}</td>
                                                                                    </tr>
                                                                                    @if($permohonan->status_id == 4)
                                                                                    <tr>
                                                                                        <td width="25%"></td>
                                                                                        <td width="25%">Alasan</td>
                                                                                        <td width="2%">:</td>
                                                                                        <td>{{ $permohonan->alasan }}</td>
                                                                                    </tr>
                                                                                    @endif
                                                                                </table>
                                                                                @break
                                                                        @endswitch
                                                                    </center>
                                                                    <br>
                                                                    <center>
                                                                        <b>
                                                                            @if($jenis == 3 OR $jenis == 4 OR $jenis == 5)
                                                                                {{ strtoupper($permohonan->first()->status->status) }}
                                                                            @else
                                                                                {{ strtoupper($permohonan->status->status) }}
                                                                            @endif
                                                                        </b>
                                                                    </center>
                                                                    <hr>
                                                                    <center>
                                                                        <p>
                                                                            @if($jenis == 3 OR $jenis == 4 OR $jenis == 5)
                                                                                @if($permohonan->first()->status_id == 1)
                                                                                    Mohon Menunggu Administrator untuk Memverifikasi Permohonan Anda. 
                                                                                @else
                                                                                    @if($permohonan->first()->status_id == 2)
                                                                                        Selamat! Permohonan Anda telah disetujui!
                                                                                    @else
                                                                                        @if($permohonan->first()->status_id == 3)
                                                                                            Mohon maaf! Permohonan Anda ditolak!
                                                                                        @endif
                                                                                    @endif
                                                                                @endif
                                                                            @else
                                                                                @if($permohonan->status_id == 1)
                                                                                    Mohon Menunggu Administrator untuk Memverifikasi Permohonan Anda. 
                                                                                @else
                                                                                    @if($permohonan->status_id == 2)
                                                                                        Selamat! Permohonan Anda telah disetujui!
                                                                                    @else
                                                                                        @if($permohonan->status_id == 3)
                                                                                            Mohon maaf! Permohonan Anda ditolak!
                                                                                        @endif
                                                                                    @endif
                                                                                @endif
                                                                            @endif
                                                                            <br> Terimakasih telah menggunakan aplikasi SIUD 2.0!
                                                                        </p>
                                                                    </center>
                                                                </div>
                                                            </td>
                                                        </tr>
                                                        </tbody>
                                                    </table>
                                                </td>
                                            </tr>
                                            </tbody>
                                        </table>
                                        {{--<p>This is a really simple email template. Its sole purpose is to get the recipient to click the button with no distractions.</p>--}}
                                        {{--<p>Good luck! Hope it works.</p>--}}
                                    </td>
                                </tr>
                            </table>
                        </td>
                    </tr>

                    <!-- END MAIN CONTENT AREA -->
                </table>

                <!-- END CENTERED WHITE CONTAINER -->
            </div>
        </td>
        <td>&nbsp;</td>
    </tr>
</table>
</body>
</html>