@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="icon-calendar font-green"></i>
                            <span class="caption-subject font-green sbold uppercase">&nbsp; Jadwal Piket Pengemudi </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        @php
                            $bulan = array (1 => 'Januari','Februari','Maret','April','Mei','Juni',
                                        'Juli','Agustus','September','Oktober','November','Desember'
                                    );
                        @endphp
                        <form method="post" action="{{ route('piket_pengemudi.filter') }}" class="form-horizontal">
                            @csrf
                            <div class="row">
                                @php
                                    if(app('request')->input() != null){
                                        $bulan_input = app('request')->input('bulan_input'); 
                                    }
                                @endphp
                                <div class="col-md-1">
                                    <h5>Bulan : </h5>
                                </div>
                                <div class="col-md-2">
                                    <select class="form-control" name="bulan_input">
                                        @php
                                            for($n=1; $n<13; $n++){
                                                if($n == $bulan_input){
                                                    echo '
                                                        <option value="'.$n.'" selected>'.$bulan[$n].'</option>
                                                    ';    
                                                } else{
                                                    echo '
                                                        <option value="'.$n.'">'.$bulan[$n].'</option>
                                                    ';
                                                }
                                            }      
                                        @endphp
                                    </select>
                                </div>
                                <div class="col-md-1">
                                    <button class="btn blue" type="submit" name="submit_bulan">Submit</button>
                                </div>
                            </div>
                        </form><br>
                        <form action="{{ route('piket_pengemudi.store') }}" method="post">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <td width="8%" align="center"><b>No</b></td>
                                    <td width="15%" align="center"><b>Tanggal</b></td>
                                    <td width="47%" align="center"><b>Petugas</b></td>
                                    <td width="30%" align="center"><b>Nomor Kontak</b></td>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $sekarang           = \Carbon\Carbon::now();
                                    $tahun              = date('Y', strtotime($sekarang));
                                    $awal_tanggal       = $tahun."-".$bulan_input."-"."01"." "."00:00:00";
                                    $awal_waktu         = strtotime($awal_tanggal);
                                    $akhir_waktu        = strtotime("+1 month", $awal_waktu);
                                    for($i=$awal_waktu; $i<$akhir_waktu; $i+=86400)
                                    {
                                        $tanggal[] = date('d-m-Y', $i);
                                    }
                                @endphp
                                @csrf
                                @foreach($tanggal as $n)
                                    @php
                                        $piket          = \App\PiketPengemudi::where('tanggal', date('Y-m-d', strtotime($n)))->first();
                                    @endphp
                                    <tr>
                                        <td align="center">{{ $loop->iteration }}</td>
                                        {{-- <td>{{ date('d', strtotime($n->tanggal)).' '.$bulan[date('n', strtotime($n->tanggal))].' '.date('Y', strtotime($n->tanggal)) }}</td> --}}
                                        <td align="center">{{ $n }}<input type="hidden" value="{{ $n }}" name="tanggal[]"></td>
                                        <td>
                                            @hasanyrole('Administrator|Pengelola Kendaraan')
                                            <input type="text" class="form-control" @if($piket != null) value="{{ ucwords($piket->nama) }}" @endif name="nama[]">
                                            @else
                                            <center>{{ ucwords($piket['nama']) }}</center>
                                            @endhasanyrole
                                        </td>
                                        <td>
                                            @hasanyrole('Administrator|Pengelola Kendaraan')
                                            <input type="text" class="form-control" @if($piket != null) value="{{ ucwords($piket->kontak) }}" @endif name="kontak[]">
                                            @else
                                            <center>{{ ucwords($piket['kontak']) }}</center>
                                            @endhasanyrole
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                        @role('Pengelola Kendaraan')
                            <center><button type="submit" class="btn green">&emsp;&emsp; Simpan &emsp;&emsp;</button></center>
                        @endrole
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection