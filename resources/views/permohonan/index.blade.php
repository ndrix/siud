@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar font-purple-plum"></i>
                            <span class="caption-subject font-purple-plum sbold uppercase">&nbsp; Permohonan Saya </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-scrollable" id="sample_3">
                            <thead>
                                <tr>
                                    <th width="8%">No</th>
                                    <th width="20%">Nomor</th>
                                    <th width="25%">Tanggal Dibuat</th>
                                    <th width="17%">Jenis</th>
                                    <th width="20%">Status</th>
                                    <th width="10%"></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $bulan = array (1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                                    $i = 1;
                                @endphp
                                @foreach($permohonan as $n)
                                    <tr>
                                        <td style="font-size:12px" align="center">{{ $i }}</td>
                                        <td style="font-size:12px">{{ $n->order_id }}</td>
                                        <td style="font-size:12px">{{ date('d ', strtotime($n->created_at)).$bulan[date('n', strtotime($n->created_at))].date(' Y', strtotime($n->created_at)) }}</td>
                                        <td style="font-size:12px">
                                            @php 
                                                $substr = substr($n->order_id, strpos($n->order_id,"SIUD.")+strlen("SIUD."),strlen($n->order_id)); 
                                                $jenis  = substr($substr,0,strpos($substr,"/"));
                                            @endphp
                                            @switch($jenis)
                                                @case(1)
                                                    Peminjaman Ruangan
                                                    @break
                                                @case(2)
                                                    Peminjaman Kendaraan
                                                    @break
                                                @case(3)
                                                    Dinas
                                                    @break
                                                @case(4)
                                                    Peminjaman Barang
                                                    @break
                                                @case(5)
                                                    Penggandaan
                                                    @break
                                                @case(6)
                                                    Bensin
                                                    @break
                                                @case(7)
                                                    Saran/Aduan
                                                    @break
                                            @endswitch
                                        </td>
                                        <td style="font-size:12px">
                                            @switch($n->status_id)
                                                @case(1)
                                                    <span class="label label-warning"><font style="font-size:12px"> {{ ucwords($n->status->status) }} </font></span>
                                                    @break
                                                @case(2)
                                                    <span class="label label-success"><font style="font-size:12px"> {{ ucwords($n->status->status) }} </font></span>
                                                    @break
                                                @case(3)
                                                    <span class="label label-danger"><font style="font-size:12px"> {{ ucwords($n->status->status) }} </font></span>
                                                    @break       
                                            @endswitch
                                        </td>
                                        <td>
                                            <center>
                                            @php 
                                                $delete_dot_order_id = str_replace(".", "", $n->order_id); 
                                                $replace_slash_order_id = str_replace("/", "-", $delete_dot_order_id); 
                                                $encrypt_order_id = \Crypt::encryptString($replace_slash_order_id);
                                            @endphp
                                                @switch($jenis)
                                                    @case(1)
                                                        @if($n->status_id == 1)
                                                            <form method="post" action="{{ route('pinjam_ruangan.delete', ['order_id' => $encrypt_order_id]) }}">
                                                                @csrf @method('delete')
                                                                <a href="{{ route('pinjam_ruangan.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>
                                                                <button type="submit" class="btn btn-xs red" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                        @else
                                                            <a href="{{ route('pinjam_ruangan.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-search"></i> Lihat</a>
                                                        @endif
                                                        @break
                                                    @case(2)
                                                        @if($n->status_id == 1)
                                                            <form method="post" action="{{ route('pinjam_kendaraan.delete', ['order_id' => $encrypt_order_id]) }}">
                                                                @csrf @method('delete')
                                                                <a href="{{ route('pinjam_kendaraan.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>
                                                                <button type="submit" class="btn btn-xs red" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                        @else
                                                            <a href="{{ route('pinjam_kendaraan.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-search"></i> Lihat</a>
                                                        @endif
                                                        @break
                                                    @case(3)
                                                        @if($n->status_id == 1)
                                                            <form method="post" action="{{ route('dinas.delete', ['order_id' => $encrypt_order_id]) }}">
                                                                @csrf @method('delete')
                                                                <a href="{{ route('dinas.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>
                                                                <button type="submit" class="btn btn-xs red" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                        @else
                                                            <a href="{{ route('dinas.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-search"></i> Lihat</a>
                                                        @endif
                                                        @break
                                                    @case(4)
                                                        @if($n->status_id == 1)
                                                            <form method="post" action="{{ route('pinjam_barang.delete', ['order_id' => $encrypt_order_id]) }}">
                                                                @csrf @method('delete')
                                                                <a href="{{ route('pinjam_barang.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>
                                                                <button type="submit" class="btn btn-xs red" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                        @else
                                                            <a href="{{ route('pinjam_barang.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-search"></i> Lihat</a>
                                                        @endif
                                                        @break
                                                    @case(5)
                                                        @if($n->status_id == 1)
                                                            <form method="post" action="{{ route('penggandaan.delete', ['order_id' => $encrypt_order_id]) }}">
                                                                @csrf @method('delete')
                                                                <a href="{{ route('penggandaan.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>
                                                                <button type="submit" class="btn btn-xs red" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                        @else
                                                            <a href="{{ route('penggandaan.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-search"></i> Lihat</a>
                                                        @endif
                                                        @break
                                                    @case(6)
                                                        @if($n->status_id == 1)
                                                            <form method="post" action="{{ route('bensin.delete', ['order_id' => $encrypt_order_id]) }}">
                                                                @csrf @method('delete')
                                                                <a href="{{ route('bensin.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>
                                                                <button type="submit" class="btn btn-xs red" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                        @else
                                                            <a href="{{ route('bensin.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-search"></i> Lihat</a>
                                                        @endif
                                                        @break
                                                    @case(7)
                                                        @if($n->status_id == 1)
                                                            <form method="post" action="{{ route('saranaduan.delete', ['order_id' => $encrypt_order_id]) }}">
                                                                @csrf @method('delete')
                                                                <a href="{{ route('saranaduan.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>
                                                                <button type="submit" class="btn btn-xs red" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></button>
                                                            </form>
                                                        @else
                                                            <a href="{{ route('saranaduan.show', ['order_id' => $encrypt_order_id]) }}" class="btn btn-xs blue"><i class="fa fa-search"></i> Lihat</a>
                                                        @endif
                                                        @break
                                                @endswitch
                                            </center>
                                        </td>
                                    </tr>    
                                    @php $i++; @endphp    
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection