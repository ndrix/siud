@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-taxi font-green"></i>
                            <span class="caption-subject font-green sbold uppercase">&nbsp; Data Kendaraan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <td width="8%"><b>No</b></td>
                                    <td width="47%"><b>Nama</b></td>
                                    <td width="30%"><b>Nomor Polisi</b></td>
                                    <td width="15%"><b>Kapasitas</b></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($kendaraan as $n)
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $n->nama }}</td>
                                        <td>{{ $n->nomor_polisi }}</td>
                                        <td>{{ $n->kapasitas }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-calendar font-purple-plum"></i>
                            <span class="caption-subject font-purple-plum sbold uppercase">&nbsp; Jadwal Penggunaan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_1">
                            <thead>
                                <tr>
                                    <th width="8%">No</th>
                                    <th width="17%">Tanggal Mulai</th>
                                    <th width="17%">Tanggal Selesai</th>
                                    <th width="21%">Kendaraan</th>
                                    <th width="25%">Kegiatan</th>
                                    <th width="12%">Satker</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($pinjam_kendaraan as $n)
                                    <tr>
                                        <td style="font-size:12px" align="center">{{ $loop->iteration }}</td>
                                        <td style="font-size:12px">{{ date('d-m-Y (h:i)', strtotime($n->waktu_mulai)) }}</td>
                                        <td style="font-size:12px">{{ date('d-m-Y (h:i)', strtotime($n->waktu_selesai)) }}</td>
                                        <td style="font-size:12px">{{ $n->kendaraan->nama.' - '.$n->kendaraan->nomor_polisi }}</td>
                                        <td style="font-size:12px">{{ $n->kegiatan }}</td>
                                        <td style="font-size:12px">{{ $n->satker->satker }}</td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection