@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gear font-red-sunglo"></i>
                            <span class="caption-subject font-red-sunglo sbold uppercase">&nbsp; Kelola Kendaraan </span>
                        </div>
                        <div class="actions">
                            <a href="{{ route('kendaraan.create') }}" class="btn green"><i class="fa fa-plus"></i> Tambah Kendaraan</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-checkable order-column" id="sample_3">
                            <thead>
                                <tr>
                                    <td width="8%"><b>No</b></td>
                                    <td width="47%"><b>Nama</b></td>
                                    <td width="20%"><b>Nomor Polisi</b></td>
                                    <td width="15%"><b>Kapasitas</b></td>
                                    <td width="10%"></td>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($kendaraan as $n)
                                    @php 
                                        $encrypt_id = \Crypt::encryptString($n->id);
                                    @endphp
                                    <tr>
                                        <td>{{ $loop->iteration }}</td>
                                        <td>{{ $n->nama }}</td>
                                        <td>{{ $n->nomor_polisi }}</td>
                                        <td>{{ $n->kapasitas }}</td>
                                        <td>
                                            <center>
                                            <form method="post" action="{{ route('kendaraan.delete', ['id' => $encrypt_id]) }}">
                                                @csrf @method('delete')
                                                <a href="{{ route('kendaraan.edit', ['id' => $encrypt_id]) }}" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>
                                                <button type="submit" class="btn btn-xs red" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></button>
                                            </form>
                                            </center>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection