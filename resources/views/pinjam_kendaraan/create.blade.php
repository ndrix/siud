@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file font-green"></i>
                            <span class="caption-subject font-green sbold uppercase">&nbsp; Form Peminjaman Kendaraan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <form action="{{ route('pinjam_kendaraan.store') }}" method="post" class="form-horizontal" enctype="multipart/form-data">
                            @csrf
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Tanggal</label>
                                    <div class="col-md-3">
                                        <input name="waktu_mulai" class="form-control form-control-inline date form_datetime form_datetime bs-datetime" size="16" type="text" value="{{ old('waktu_mulai') }}" required/>
                                    </div>
                                    <div class="col-md-1">
                                        <center><label class="control-label">s.d.</label></center>
                                    </div>
                                    <div class="col-md-3">
                                        <input name="waktu_selesai" class="form-control form-control-inline date form_datetime form_datetime bs-datetime" size="16" type="text" value="{{ old('waktu_selesai') }}" required/>
                                    </div>
                                    <div class="col-md-1">
                                        <button type="submit" class="btn blue" name="ketersediaan" value="cek">Cek Ketersediaan</button>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label"></label>
                                    <div class="col-md-7">
                                        @if ($message = Session::get('kendaraan_tersedia'))
                                            @foreach($message as $n)
                                                <span class="font font-blue"><i class="fa fa-check"></i> {{ $n }} </span><br>
                                            @endforeach
                                        @endif
                                        @if ($message = Session::get('tidak_tersedia'))
                                            <label class="label label-danger"><i class="fa fa-times"></i> {{ $message }}</label>
                                        @endif
                                        @if ($message = Session::get('input_salah'))
                                            <label class="label label-danger"><i class="fa fa-times"></i> {{ $message }}</label>
                                        @endif
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Kendaraan</label>
                                    <div class="col-md-7">
                                        <select class="form-control" name="kendaraan_id">
                                            @foreach($kendaraan as $n)
                                                @if(old('kendaraan_id') == $n->id)
                                                    <option value="{{ $n->id }}" selected>{{ ucwords($n->nama).' - '.$n->nomor_polisi }}</option>
                                                @else
                                                    <option value="{{ $n->id }}">{{ ucwords($n->nama).' - '.$n->nomor_polisi }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Satker</label>
                                    <div class="col-md-2">
                                        <select class="form-control select2" name="satker_id">
                                            @foreach($satker as $n)
                                                @if(old('satker_id') == $n->id)
                                                    <option value="{{ $n->id }}" selected>{{ ucwords($n->satker) }}</option>
                                                @else
                                                    <option value="{{ $n->id }}">{{ ucwords($n->satker) }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Kegiatan</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="kegiatan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Lokasi Tujuan</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="tujuan">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Dukungan Pengemudi</label>
                                    <div class="col-md-7">
                                        <div class="mt-radio-inline">
                                            <label class="mt-radio mt-radio-outline"> Ya
                                                <input type="radio" value="ya" name="dukungan_pengemudi" checked/>
                                                <span></span>
                                            </label>
                                            <label class="mt-radio mt-radio-outline"> Tidak
                                                <input type="radio" value="tidak" name="dukungan_pengemudi"/>
                                                <span></span>
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Jml Penumpang</label>
                                    <div class="col-md-2">
                                        <input type="text" class="form-control" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="2" name="jml_penumpang">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Scan Nodin</label>
                                    <div class="col-md-2">
                                        <div class="fileinput fileinput-new" data-provides="fileinput">
                                            <div class="input-group input-large">
                                                <div class="form-control uneditable-input input-fixed input-medium" data-trigger="fileinput">
                                                    <i class="fa fa-file fileinput-exists"></i>&nbsp;
                                                    <span class="fileinput-filename"> </span>
                                                </div>
                                                <span class="input-group-addon btn default btn-file">
                                                    <span class="fileinput-new"> Select file </span>
                                                    <span class="fileinput-exists"> Change </span>
                                                    <input type="file" name="nodin"> 
                                                </span>
                                                <a href="javascript:;" class="input-group-addon btn red fileinput-exists" data-dismiss="fileinput"> Remove </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <hr>
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-9">
                                        <button type="submit" class="btn green">&emsp;&emsp;Submit&emsp;&emsp;</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection