<html lang="en">
    <!--<![endif]-->
    <!-- BEGIN HEAD -->

    <head>
        <meta charset="utf-8" />
        <title>SIUD 2.0</title>
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta content="width=device-width, initial-scale=1" name="viewport" />
        <meta content="SIUD versi terbaru." name="description" />
        <meta content="" name="author" />
        <!-- mobil -->
        <style>
        .barchart {
          width: 100%;
          align-content: center;
          display: flex;
          justify-content: center;
        }
        @media screen and (min-width: 200px){
          .mobil1{
            visibility: hidden;}
          .mobil2{
            visibility: hidden;}
          .mobil3{
            visibility: hidden;}
          .mobil4{
            visibility: hidden;}
          .mobil5{
            visibility: hidden;}
          .mobil6{
            visibility: hidden;}
          .sepeda{
            visibility: hidden;}
        }
        @media screen and (min-width: 418px){
          .mobil1{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 9s linear infinite;
            position: absolute;
            top: 250px;}
          .mobil2{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak2 11s linear infinite;
            position: absolute;
            top: 250px;
            right: 0px;}
          .mobil3{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak3 9s linear infinite;
            position: absolute;
            top: 485px;}
          .mobil4{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak4 8s linear infinite;
            position: absolute;
            top: 225px;
            left: 0px;}
          .mobil5{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 8s linear infinite;
            position: absolute;
            top: 517px;}
          .mobil6{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 10s linear infinite;
            position: absolute;
            top: 523px;}
          .sepeda{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: sepedagerak 17s ease infinite;
            position: absolute;
            top: 455px;
            left: 0px;}
        }
        @media screen and (min-width: 1100px){
          .mobil1{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 9s linear infinite;
            position: absolute;
            top: 250px;}
          .mobil2{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak2 11s linear infinite;
            position: absolute;
            top: 250px;
            right: 0px;}
          .mobil3{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak3 9s linear infinite;
            position: absolute;
            top: 485px;}
          .mobil4{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak4 8s linear infinite;
            position: absolute;
            top: 225px;
            left: 0px;}
          .mobil5{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 8s linear infinite;
            position: absolute;
            top: 517px;}
          .mobil6{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 10s linear infinite;
            position: absolute;
            top: 523px;}
          .sepeda{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: sepedagerak 17s ease infinite;
            position: absolute;
            top: 455px;
            left: 0px;}
        }
        @media screen and (min-width: 1200px){
          .mobil1{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 9s linear infinite;
            position: absolute;
            top: 250px;}
          .mobil2{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak2 11s linear infinite;
            position: absolute;
            top: 250px;
            right: 0px;}
          .mobil3{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak3 9s linear infinite;
            position: absolute;
            top: 490px;}
          .mobil4{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak4 8s linear infinite;
            position: absolute;
            top: 225px;
            left: 0px;}
          .mobil5{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 8s linear infinite;
            position: absolute;
            top: 532px;}
          .mobil6{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 10s linear infinite;
            position: absolute;
            top: 538px;}
          .sepeda{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: sepedagerak 17s ease infinite;
            position: absolute;
            top: 468px;
            left: 0px;}
        }
        @media screen and (min-width: 1300px){
          .mobil1{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 9s linear infinite;
            position: absolute;
            top: 250px;}
          .mobil2{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak2 11s linear infinite;
            position: absolute;
            top: 250px;
            right: 0px;}
          .mobil3{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak3 9s linear infinite;
            position: absolute;
            top: 518px;}
          .mobil4{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak4 8s linear infinite;
            position: absolute;
            top: 225px;
            left: 0px;}
          .mobil5{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 8s linear infinite;
            position: absolute;
            top: 555px;}
          .mobil6{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 10s linear infinite;
            position: absolute;
            top: 560px;}
          .sepeda{
            background-repeat: no-repeat;
            background-size: 80px;
            animation: sepedagerak 17s ease infinite;
            position: absolute;
            top: 488px;
            left: 0px;}
        }
        @media screen and (max-height: 625px){
          .mobil1{
            top:235px;}
          .mobil2{
            top:236px;}
          .mobil3{
            top:500px;}
          .mobil4{
            top:200px;}
          .sepeda{
            top:470px;}
          .mobil5{
            top:535px;}
          .mobil6{
            top:542px;}
          .sepeda{
            top:
          }
        }
        @media screen and (max-height: 611px){
          .mobil1{
            top:225px;}
          .mobil2{
            top:221px;}
          .mobil3{
            top:500px;}
          .mobil4{
            top:200px;}
          .sepeda{
            top:470px;}
          .mobil5{
            top:535px;}
          .mobil6{
            top:542px;}
        }
        @media screen and (max-height: 595px){
          .mobil1{
            }
          .mobil2{
            }
          .mobil3{
            top: 490px;}
          .mobil4{
            }
          .sepeda{
            top: 460px;}
          .mobil5{
            }
          .mobil6{
            }
        }
        @media screen and (max-height: 575px){
          .mobil1{
            top:210px;}
          .mobil2{
            top:212px;}
          .mobil3{
            top: 470px;}
          .mobil4{
            top:180px;}
          .sepeda{
            top: 440px;}
          .mobil5{
            visibility: hidden;}
          .mobil6{
            visibility: hidden;}
        }
        @media screen and (max-height: 560px){
          .mobil1{
            top:200px;}
          .mobil2{
            top:202px;}
          .mobil3{
            top: 470px;}
          .mobil4{
            top:180px;}
          .sepeda{
            top: 440px;}
          .mobil5{
            visibility: hidden;}
          .mobil6{
            visibility: hidden;}
        }
        @media screen and (max-height: 539px){
          .mobil1{
            top:185px;}
          .mobil2{
            top:189px;}
          .mobil3{
            top: 455px;}
          .mobil4{
            top:160px;}
          .sepeda{
            top: 423px;}
          .mobil5{
            visibility: hidden;}
          .mobil6{
            visibility: hidden;}
        }
        @media screen and (min-width: 1400px){
          .mobil1{
            visibility: hidden;}
          .mobil2{
            visibility: hidden;}
          .mobil3{
            visibility: hidden;}
          .mobil4{
            visibility: hidden;}
          .mobil5{
            visibility: hidden;}
          .mobil6{
            visibility: hidden;}
          .sepeda{
            visibility: hidden;}
        }
        @keyframes mobilgerak1 {
          0%{ transform: translateX(1360px);}
          100%{transform: translateX(0px);}
        }
        @keyframes mobilgerak2 {
          0%{ transform: translateX(100px);}
          100%{transform: translateX(-1360px);}
        }
        @keyframes mobilgerak3 {
          0%{ transform: translateX(-100px);}
          100%{transform: translateX(1360px);}
        }

        @keyframes mobilgerak4 {
          0%{ transform: translateX(-100px);}
          100%{transform: translateX(1360px);}
        }

        @keyframes sepedagerak {
          0%{ transform: translateX(-100px);}
          100%{transform: translateX(1360px);}
        }

        @media screen and (min-width: 1900px){
          .mobil1{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 9s linear infinite;
            position: absolute;
            top: 450px;}
          .mobil2{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak2 11s linear infinite;
            position: absolute;
            top: 455px;
            right: 0px;}
          .mobil3{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak3 9s linear infinite;
            position: absolute;
            top: 78%}
          .mobil4{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak4 8s linear infinite;
            position: absolute;
            top: 425px;
            left: 0px;}
          .mobil5{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 8s linear infinite;
            position: absolute;
            top: 83%;}
          .mobil6{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: mobilgerak1 10s linear infinite;
            position: absolute;
            top: 84%;}
          .sepeda{
            visibility: visible;
            background-repeat: no-repeat;
            background-size: 80px;
            animation: sepedagerak 17s ease infinite;
            position: absolute;
            top: 75%;
            left: 0px;}

          @keyframes mobilgerak1 {
            0%{ transform: translateX(1920px);}
            100%{transform: translateX(0px);}
          }
          @keyframes mobilgerak2 {
            0%{ transform: translateX(100px);}
            100%{transform: translateX(-1920px);}
          }
          @keyframes mobilgerak3 {
            0%{ transform: translateX(-100px);}
            100%{transform: translateX(1920px);}
          }

          @keyframes mobilgerak4 {
            0%{ transform: translateX(-100px);}
            100%{transform: translateX(1920px);}
          }

          @keyframes sepedagerak {
            0%{ transform: translateX(-100px);}
            100%{transform: translateX(1920px);}
          }
        }
        @media screen and (min-width: 2000px){
          .mobil1{
            visibility: hidden;}
          .mobil2{
            visibility: hidden;}
          .mobil3{
            visibility: hidden;}
          .mobil4{
            visibility: hidden;}
          .mobil5{
            visibility: hidden;}
          .mobil6{
            visibility: hidden;}
          .sepeda{
            visibility: hidden;}
        }

        @media screen and (max-height: 500px){
          .mobil1{
            visibility: hidden;}
          .mobil2{
            visibility: hidden;}
          .mobil3{
            visibility: hidden;}
          .mobil4{
            visibility: hidden;}
          .sepeda{
            visibility: hidden;}
          .mobil5{
            visibility: hidden;}
          .mobil6{
            visibility: hidden;}
        }
        .btnmodal{
          position: absolute;
          right: 30px;
          top: 10px;}
        </style>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/font-awesome/css/font-awesome.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/simple-line-icons/simple-line-icons.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet" type="text/css" />
        <link href="{{ asset('global/plugins/bootstrap-switch/css/bootstrap-switch.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="{{ asset('global/css/components.min.css') }}" rel="stylesheet" id="style_components" type="text/css" />
        <link href="{{ asset('global/css/plugins.min.css') }}" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <!-- BEGIN PAGE LEVEL STYLES -->
        <link href="{{ asset('pages/css/login-4.css') }}" rel="stylesheet" type="text/css" />
        <!-- END PAGE LEVEL STYLES -->
        <!-- END THEME LAYOUT STYLES -->
        <link rel="shortcut icon" href="{{ asset('global/img/favicon.ico') }}" />
        <script src="https://code.jquery.com/jquery-1.10.2.js"></script> </head>

    <body class="login" style="overflow: hidden">
      <!-- Script Direct Page
      <script type="text/javascript">
      var widthdoc = $( document ).width();
      var heightdoc = $( document ).height();
      var total = widthdoc + heightdoc;
      document.write(widthdoc, 'x' ,heightdoc, '=', total);

      if (widthdoc>=200) {
       document.write('<br>kondisi 1')
      }
      else {
        window.location.replace('page_user_login_3.html');
      }
      </script> -->

        <!-- Mobil -->
        <div class="sepeda"><img src="{{ asset('pages/media/bg/siud/bikerider.gif') }}" alt="" height="66"></div>
        <div class="mobil3"><img src="{{ asset('pages/media/bg/siud/mobil2.png') }}" alt="" height="45"></div>
        <div class="mobil4"><img src="{{ asset('pages/media/bg/siud/mobil4.png') }}" alt="" height="42"></div>
        <div class="mobil1"><img src="{{ asset('pages/media/bg/siud/mobil1.png') }}" alt="" height="42"></div>
        <div class="mobil2"><img src="{{ asset('pages/media/bg/siud/mobil3.png') }}" alt="" height="45"></div>
        <div class="mobil5"><img src="{{ asset('pages/media/bg/siud/mobil5.png') }}" alt="" height="42"></div>
        <div class="mobil6"><img src="{{ asset('pages/media/bg/siud/mobil6.png') }}" alt="" height="42"></div>
        <!-- End Mobil -->

        <!-- Tombol untuk menampilkan modal-->
        <div class="btnmodal">
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myKinerja" onClick="init()">Kinerja</button>
        <button type="button" class="btn btn-info" data-toggle="modal" data-target="#myModal">Login</button>
        </div>

      	<!-- Modal Login -->
      	<div id="myModal" class="modal fade" role="dialog">
      		<div class="">
      			<!-- konten modal-->
      			<div class="">
      				<!-- heading modal -->
              <!-- BEGIN LOGO -->
              <div class="logo">
                  <a href="index.html">
                      <img src="../assets/layouts/layout4/img/logosiud.png" alt="" /> </a>
              </div>
              <!-- END LOGO -->
      				<!-- body modal -->
              <!-- BEGIN LOGIN -->
              <div class="content">
                  <!-- BEGIN LOGIN FORM -->
                  <form class="login-form" action="{{ route('login') }}" method="post">
                    @csrf
                      <h3 class="form-title">Login to your account</h3>
                      <div class="alert alert-danger display-hide">
                          <button class="close" data-close="alert"></button>
                          <span> Enter any username and password. </span>
                      </div>
                      <div class="form-group">
                          <!--ie8, ie9 does not support html5 placeholder, so we just show field title for that-->
                          <label class="control-label visible-ie8 visible-ie9">Username</label>
                          <div class="input-icon">
                            <i class="fa fa-user"></i>
                            <input class="form-control{{ $errors->has('username') ? ' is-invalid' : '' }}" type="text" autocomplete="off" placeholder="Username" name="username" />
                          </div>
                      </div>
                      <div class="form-group">
                          <label class="control-label visible-ie8 visible-ie9">Password</label>
                          <div class="input-icon">
                            <i class="fa fa-lock"></i>
                            <input class="form-control placeholder-no-fix" type="password" autocomplete="off" placeholder="Password" name="password" />
                          </div>
                      </div>
                      <div class="form-group">
                          <div class="gcaptcha">
                            {!! Captcha::display() !!}
                          </div><br>
                          <button type="submit" class="btn btn-lg green pull-right"> Login </button>
                      </div>
                  </form>
                  <!-- END LOGIN FORM -->
              </div>
              <!-- END LOGIN -->
      				<!-- footer modal -->
              <!-- BEGIN COPYRIGHT -->
              <br>
              <div class="copyright"> 2018 &copy; Urdal BSSN </div>
              <!-- END COPYRIGHT -->
      			</div>
      		</div>
      	</div>

        <!-- Modal Kinerja-->
        <div id="myKinerja" class="modal fade" role="dialog">
      		<div class="modal-dialog">
      			<!-- konten modal-->
      			<div class="modal-content">
      				<!-- heading modal -->
      				<div class="modal-header">
      					<button type="button" class="close" data-dismiss="modal">&times;</button>
      					<h4 class="modal-title">Kinerja SIUD</h4>
      				</div>
              <!-- body modal -->
              <?php
                $total_permohonan     = count(\App\Penggandaan::groupBy('order_id')->get()) + 
                                        count(\App\PinjamBarang::groupBy('order_id')->get()) + 
                                        \App\Dinas::count() + 
                                        \App\PinjamRuangan::count() + 
                                        \App\PinjamKendaraan::count() + 
                                        \App\Bensin::count() + 
                                        \App\SaranAduan::count();
                $permohonan_diproses  = count(\App\Penggandaan::groupBy('order_id')->where('status_id', '!=', 1)->get()) + 
                                        count(\App\PinjamBarang::groupBy('order_id')->where('status_id', '!=', 1)->get()) + 
                                        \App\Dinas::where('status_id', '!=', 1)->count() + 
                                        \App\PinjamRuangan::where('status_id', '!=', 1)->count() + 
                                        \App\PinjamKendaraan::where('status_id', '!=', 1)->count() + 
                                        \App\Bensin::where('status_id', '!=', 1)->count() + 
                                        \App\SaranAduan::where('status_id', '!=', 1)->count();
                $permohonan_ruangan   = \App\PinjamRuangan::where('status_id', '!=', 1)->count();
                $permohonan_kendaraan = \App\PinjamKendaraan::where('status_id', '!=', 1)->count();
                $permohonan_dinas     = \App\Dinas::where('status_id', '!=', 1)->count();
                $permohonan_bensin    = \App\Bensin::where('status_id', '!=', 1)->count();
                $tahun                = \Carbon\Carbon::now()->year;
                for($i=1; $i<=12; $i++){
                  $ruangan            = \App\PinjamRuangan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->whereYear('updated_at', $tahun)->count();
                  $kendaraan          = \App\PinjamKendaraan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->whereYear('updated_at', $tahun)->count();
                  $perbaikan          = count(\App\PerbaikanBarang::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
                  $pinjam_barang      = count(\App\PinjamBarang::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
                  $penggandaan        = count(\App\Penggandaan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->groupBy('order_id')->whereYear('updated_at', $tahun)->get());
                  $saran_aduan        = \App\SaranAduan::where('status_id', '!=', 1)->whereMonth('updated_at', $i)->whereYear('updated_at', $tahun)->count();
                  $jml_permohonan     = $ruangan + $kendaraan + $perbaikan + $pinjam_barang + $penggandaan + $saran_aduan;
                  $arr_jml_permohonan[$i] = $jml_permohonan;
                };
              ?>
      				<div class="modal-body">
                <div class="row widget-row">
                    <div class="col-md-6">
                        <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                            <h4 class="widget-thumb-heading">Total Permohonan</h4>
                            <div class="widget-thumb-wrap">
                                <i class="widget-thumb-icon bg-green icon-layers"></i>
                                <div class="widget-thumb-body">
                                    <span class="widget-thumb-subtitle">&nbsp;</span>
                                    <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $total_permohonan }}">{{ $total_permohonan }}</span>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                      <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                          <h4 class="widget-thumb-heading">Permohonan Diproses</h4>
                          <div class="widget-thumb-wrap">
                              <i class="widget-thumb-icon bg-blue icon-check"></i>
                              <div class="widget-thumb-body">
                                  <span class="widget-thumb-subtitle">&nbsp;</span>
                                  <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $permohonan_diproses }}">{{ $permohonan_diproses }}</span>
                              </div>
                          </div>
                      </div>
                    </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                        <h4 class="widget-thumb-heading">Permohonan Ruangan</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-blue-hoki fa fa-building"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle">&nbsp;</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $permohonan_ruangan }}">{{ $permohonan_ruangan }}</span>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                        <h4 class="widget-thumb-heading">Permohonan Kendaraan</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-green-seagreen fa fa-car"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle">&nbsp;</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $permohonan_kendaraan }}">{{ $permohonan_kendaraan }}</span>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-6">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                        <h4 class="widget-thumb-heading">Permohonan KTLN</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-purple-plum fa fa-plane"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle">&nbsp;</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $permohonan_dinas }}">{{ $permohonan_dinas }}</span>
                            </div>
                        </div>
                    </div>
                  </div>
                  <div class="col-md-6">
                    <div class="widget-thumb widget-bg-color-white text-uppercase margin-bottom-20 bordered">
                        <h4 class="widget-thumb-heading">Permohonan Bensin</h4>
                        <div class="widget-thumb-wrap">
                            <i class="widget-thumb-icon bg-yellow-mint fa fa-flask"></i>
                            <div class="widget-thumb-body">
                                <span class="widget-thumb-subtitle">&nbsp;</span>
                                <span class="widget-thumb-body-stat" data-counter="counterup" data-value="{{ $permohonan_bensin }}">{{ $permohonan_bensin }}</span>
                            </div>
                        </div>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12">
                    <div id="barchart" class="barchart"></div>
                  </div>
                </div>
      				</div>
      				<!-- footer modal -->
      				<div class="modal-footer">
      					<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
      				</div>
      			</div>
      		</div>
      	</div>

        <!--[if lt IE 9]>
        <script src="../assets/global/plugins/respond.min.js"></script>
        <script src="../assets/global/plugins/excanvas.min.js"></script>
        <script src="../assets/global/plugins/ie8.fix.min.js"></script>
        <![endif]-->
        <!-- BEGIN CORE PLUGINS -->
        <script src="{{ asset('global/plugins/jquery.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap/js/bootstrap.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/js.cookie.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery-slimscroll/jquery.slimscroll.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery.blockui.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/bootstrap-switch/js/bootstrap-switch.min.js') }}" type="text/javascript"></script>
        <!-- END CORE PLUGINS -->
        <!-- BEGIN PAGE LEVEL PLUGINS -->
        <script src="{{ asset('global/plugins/jquery-validation/js/jquery.validate.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/jquery-validation/js/additional-methods.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/select2/js/select2.full.min.js') }}" type="text/javascript"></script>
        <script src="{{ asset('global/plugins/backstretch/jquery.backstretch.min.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL PLUGINS -->
        <!-- BEGIN THEME GLOBAL SCRIPTS -->
        <script src="{{ asset('global/scripts/app.min.js') }}" type="text/javascript"></script>
        <!-- END THEME GLOBAL SCRIPTS -->
        <!-- BEGIN PAGE LEVEL SCRIPTS -->
        <script src="{{ asset('pages/scripts/login-4siud.js') }}" type="text/javascript"></script>
        <!-- END PAGE LEVEL SCRIPTS -->
        <!-- BEGIN THEME LAYOUT SCRIPTS -->
        <!-- END THEME LAYOUT SCRIPTS -->
        <script type="text/javascript" src="https://www.google.com/jsapi"></script>
        <script type="text/javascript">
          function init() {
              google.load("visualization", "1.1", {
                  packages: ["corechart"],
                  callback: 'drawCharts'
              });
          }
          function drawCharts() {
              var data = google.visualization.arrayToDataTable([
                  ['Bulan', 'Jml', { role: 'style' }],
                  ['Jan', <?php echo $arr_jml_permohonan[1]; ?>, '#99ccff'],
                  ['Feb', <?php echo $arr_jml_permohonan[2]; ?>, '#9999ff'],
                  ['Mar', <?php echo $arr_jml_permohonan[3]; ?>, '#cc99ff'],
                  ['Apr', <?php echo $arr_jml_permohonan[4]; ?>, '#ff99ff'],
                  ['Mei', <?php echo $arr_jml_permohonan[5]; ?>, '#ff99cc'],
                  ['Jun', <?php echo $arr_jml_permohonan[6]; ?>, '#66ffb2'],
                  ['Jul', <?php echo $arr_jml_permohonan[7]; ?>, '#e5ccff'],
                  ['Ags', <?php echo $arr_jml_permohonan[8]; ?>, '#ffcce5'],
                  ['Sep', <?php echo $arr_jml_permohonan[9]; ?>, '#ffccff'],
                  ['Okt', <?php echo $arr_jml_permohonan[10]; ?>, '#ccccff'],
                  ['Nov', <?php echo $arr_jml_permohonan[11]; ?>, '#66b2ff'],
                  ['Des', <?php echo $arr_jml_permohonan[12]; ?>, '#ff66b2']
              ]);
              var options = {
                  title: 'Jumlah Permohonan Tiap Bulan',
              };
              var chart = new google.visualization.ColumnChart(document.getElementById('barchart'));
              chart.draw(data, options);
          }
        </script>
        <script>
                  $(window).resize(function(){
              init();
          });
            var assetBaseUrl = "{{ asset('') }}";
            $(document).ready(function()
            {
                $('#clickmewow').click(function()
                {
                    $('#radio1003').attr('checked', 'checked');
                });
            })
        </script>
    </body>
</html>
