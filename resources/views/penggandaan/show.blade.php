@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-sticky-note @role('Administrator') font-red-sunglo @else font-purple-plum @endrole"></i>
                            <span class="caption-subject @role('Administrator') font-red-sunglo @else font-purple-plum @endrole sbold uppercase">&nbsp; Rincian Permohonan </span>
                        </div>
                        <div class="actions">
                            @hasanyrole('Administrator|Pengelola Urdal|Pengelola Kendaraan|Pengelola Penggandaan')
                                <a href="{{ route('permohonan.kelola') }}" class="btn default"><i class="fa  fa-chevron-circle-left"></i> Kembali</a>
                            @else
                                <a href="{{ route('permohonan.index') }}" class="btn default"><i class="fa  fa-chevron-circle-left"></i> Kembali</a>
                            @endhasanyrole
                        </div>
                    </div>
                    <div class="portlet-body">
                        @php 
                            $delete_dot_order_id = str_replace(".", "", $penggandaan->first()['order_id']); 
                            $replace_slash_order_id = str_replace("/", "-", $delete_dot_order_id); 
                            $encrypt_order_id = \Crypt::encryptString($replace_slash_order_id);
                        @endphp
                        @hasanyrole('Administrator|Pengelola Urdal|Pengelola Pengadaan|Pengelola Penggandaan')
                        @else    
                            @php    
                                foreach(Auth::user()->unreadnotifications as $n){
                                    if($n->data['order_id'] == $replace_slash_order_id){
                                        $n->markasread();
                                    };
                                };
                            @endphp
                        @endhasanyrole
                        <table class="table table-striped table-bordered table-hover">
                            <tbody> 
                                <tr>
                                    <td width="40%">Nomor</td>
                                    <td>{{ $penggandaan->first()['order_id'] }}</td>
                                </tr>
                                <tr>
                                    <td width="40%">Jenis</td>
                                    <td>
                                        Penggandaan
                                    </td>
                                </tr>
                                <tr>
                                    @php
                                        $bulan = array (1 => 'Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember');
                                    @endphp
                                    <td width="40%">Tanggal</td>
                                    <td>{{ date('d ', strtotime($penggandaan->first()['created_at'])).$bulan[date('n', strtotime($penggandaan->first()['created_at']))].date(' Y', strtotime($penggandaan->first()['created_at'])) }}</td>
                                </tr>
                                <tr>
                                    <td width="40%">Satker</td>
                                    <td>    
                                        {{ $penggandaan->first()->satker->satker }}
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%">Item</td>
                                    <td>
                                        @foreach($penggandaan as $p)
                                            -&ensp;{{ ucwords($p->judul) }} ({{ $p->kertas }}) | Warna : {{ $p->hlm_warna }}, Rangkap : {{ $p->jml_warna }} | Hitam : {{ $p->hlm_hitam_putih }}, Rangkap : {{ $p->jml_hitam_putih }}<br>
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%">Keterangan</td>
                                    <td>
                                        @foreach($penggandaan as $p)
                                            - {{ $p->keterangan }}
                                        @endforeach
                                    </td>
                                </tr>
                                <tr>
                                    <td width="40%">Status</td>
                                    <td>
                                        @switch($penggandaan->first()['status_id'])
                                            @case(1)
                                                <span class="label label-danger"> {{ ucwords($penggandaan->first()->status->status) }} </span>
                                                @break
                                            @case(2)
                                                <span class="label label-warning"> {{ ucwords($penggandaan->first()->status->status) }} </span>
                                                @break
                                            @case(3)
                                                <span class="label label-success"> {{ ucwords($penggandaan->first()->status->status) }} </span>
                                                @break
                                            @case(4)
                                                <span class="label label-default"> {{ ucwords($penggandaan->first()->status->status) }} </span>
                                                @break        
                                        @endswitch
                                    </td>
                                </tr>
                                @if($penggandaan->first()['alasan'] != null)
                                    <tr>
                                        <td width="40%">Alasan</td>
                                        <td>{{ $penggandaan->first()['alasan'] }}</td>
                                    </tr>
                                @endif
                            </tbody>
                        </table>
                        <center>
                            @if($kelola == "no")
                                @if($penggandaan->first()['status_id'] != 2 AND $penggandaan->first()['status_id'] != 3)
                                    <a href="{{ route('penggandaan.edit', ['order_id' => $encrypt_order_id]) }}" class="btn green">Ubah Data</a>
                                @endif
                            @else
                                <form method="post" action="{{ route('permohonan.setuju', ['order_id' => $encrypt_order_id]) }}">
                                    @csrf @method('put')
                                    <button class="btn blue">Setuju</button>
                                    <a href="{{ route('permohonan.tolak', ['order_id' => $encrypt_order_id]) }}" class="btn red">Tolak</a>
                                </form>
                            @endif
                        </center>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection