@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <li>{{ $message }}</li>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-pencil font-purple-plum"></i>
                            <span class="caption-subject font-purple-plum sbold uppercase">&nbsp; Ubah Permohonan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        @php 
                            $delete_dot_order_id = str_replace(".", "", $penggandaan->first()['order_id']); 
                            $replace_slash_order_id = str_replace("/", "-", $delete_dot_order_id); 
                            $encrypt_order_id = \Crypt::encryptString($replace_slash_order_id);
                        @endphp
                        <form action="{{ route('penggandaan.update', ['order_id' => $encrypt_order_id]) }}" class="mt-repeater form-horizontal" method="post">
                            @csrf @method('put')
                            <div class="form-body"> 
                                <div class="form-group">
                                    <label class="col-md-1 control-label">Satker</label>
                                    <div class="col-md-2">
                                        <select class="form-control" name="satker_id">
                                            @foreach($satker as $n)
                                                <option value="{{ $n->id }}">{{ ucwords($n->satker) }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div data-repeater-list="barang"><hr>
                                    @foreach($penggandaan as $p)
                                        <div data-repeater-item class="mt-repeater-item">
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Nama/Judul</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" name="judul" value="{{ $p->judul }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Kertas</label>
                                                <div class="col-md-2">
                                                    <input type="text" class="form-control" name="kertas" value="{{ $p->kertas }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Halaman Warna</label>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" name="hlm_warna" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="3" value="{{ $p->hlm_warna }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Rangkap Warna</label>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" name="jml_warna" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="2" value="{{ $p->jml_warna }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Halaman Hitam</label>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" name="hlm_hitam_putih" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="3" value="{{ $p->hlm_hitam_putih }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Rangkap Hitam</label>
                                                <div class="col-md-1">
                                                    <input type="text" class="form-control" name="jml_hitam_putih" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="2" value="{{ $p->jml_hitam_putih }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <label class="col-md-2 control-label">Keterangan</label>
                                                <div class="col-md-7">
                                                    <input type="text" class="form-control" name="keterangan" value="{{ $p->keterangan }}">
                                                </div>
                                            </div>
                                            <div class="form-group">
                                                <div class="col-md-5"></div>
                                                <div class="col-md-2">
                                                    <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete form-control">
                                                        <i class="fa fa-close"></i></a>
                                                </div>
                                                <div class="col-md-5"></div>
                                            </div>
                                        </div>
                                    @endforeach
                                </div>
                            </div>
                            <div align="right"><a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                <i class="fa fa-plus"></i> Add</a>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="submit" class="btn green">Simpan</button>
                                        <a href="{{ route('penggandaan.show', ['order_id' => $encrypt_order_id]) }}" class="btn default">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection