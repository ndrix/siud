@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <li>{{ $message }}</li>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file font-green"></i>
                            <span class="caption-subject font-green sbold uppercase">&nbsp; Form Penggandaan </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="form-body">
                            <div class="form-group">
                                <form action="{{ route('penggandaan.store') }}" class="mt-repeater form-horizontal" method="post">
                                    @csrf
                                    <div class="col-md-1">
                                        <label class="control-label">Satker</label>
                                    </div>
                                    <div class="col-md-2">
                                        <select class="form-control select2" name="satker_id">
                                            @foreach($satker as $n)
                                                @if(old('satker_id') == $n->id)
                                                    <option value="{{ $n->id }}" selected>{{ ucwords($n->satker) }}</option>
                                                @else
                                                    <option value="{{ $n->id }}">{{ ucwords($n->satker) }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                    <div class="col-md-9" align="right">
                                        <a href="javascript:;" data-repeater-create class="btn btn blue mt-repeater-add">
                                        <i class="fa fa-plus"></i> Add</a>
                                    </div>
                                    <div class="col-md-12">
                                        <!-- jQuery Repeater Container -->
                                        <div data-repeater-list="barang">
                                            <div data-repeater-item class="mt-repeater-item">
                                                <!-- BEGIN SAMPLE TABLE PORTLET-->
                                                <div class="table-scrollable">
                                                    <table class="table table-striped table-bordered table-hover">
                                                        <thead>
                                                            <tr>
                                                                <th scope="col" rowspan="2" style="width:230px;text-align:center;vertical-align: middle; !important "> Nama / Judul </th>
                                                                <th scope="col" rowspan="2"style="text-align:center;vertical-align: middle;"> Kertas </th>
                                                                <th scope="col" colspan="2" style="text-align:center;"> Warna </th>
                                                                <th scope="col" colspan="2" style="text-align:center;"> Hitam Putih </th>
                                                                <th scope="col" rowspan="2"style="text-align:center;vertical-align: middle;"> Keterangan </th>
                                                            </tr>
                                                            <tr>
                                                                <th scope="col" style="text-align:center;"> Page </th>
                                                                <th scope="col" style="text-align:center;"> Qty </th>
                                                                <th scope="col" style="text-align:center;"> Page </th>
                                                                <th scope="col" style="text-align:center;"> Qty </th>
                                                            </tr>
                                                        </thead>
                                                        <tbody>
                                                            <div class="mt-repeater">
                                                                <div class="mt-repeater-input mt-repeater-textarea">
                                                                <tr>
                                                                    <td><input type="text" name="judul" class="form-control text" align="center"/></td>
                                                                    <td><input type="text" name="kertas" class="form-control text" align="center"/></td>
                                                                    <td><input type="number" name="hlm_warna" class="form-control text" align="center"/></td>
                                                                    <td><input type="number" name="jml_warna" class="form-control text" align="center"/></td>
                                                                    <td><input type="number" name="hlm_hitam_putih" class="form-control text" align="center"/></td>
                                                                    <td><input type="number" name="jml_hitam_putih" class="form-control text" align="center"/></td>
                                                                    <td><input type="text" name="keterangan" class="form-control text" align="center"/></td>
                                                                    <td><a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete">
                                                                        <i class="fa fa-close"></i></a></td>
                                                                    </tr>
                                                                </div>
                                                            </div>
                                                        </tbody>
                                                    </table>
                                                </div>
                                            </div>
                                            <!-- END SAMPLE TABLE PORTLET-->
                                        </div>
                                    </div>
                                    <center><button type="submit" class="btn green">Submit</button></center>
                                </form>
                            </div>
                        </div>
                        {{-- <form action="{{ route('penggandaan.store') }}" class="mt-repeater form-horizontal" method="post">
                            @csrf
                            <div class="form-body"> 
                                <div class="form-group">
                                    <label class="col-md-1 control-label">Satker</label>
                                    <div class="col-md-2">
                                        <select class="form-control" name="satker_id">
                                            @foreach($satker as $n)
                                                @if(old('satker_id') == $n->id)
                                                    <option value="{{ $n->id }}" selected>{{ ucwords($n->satker) }}</option>
                                                @else
                                                    <option value="{{ $n->id }}">{{ ucwords($n->satker) }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div data-repeater-list="barang"><hr>
                                    <div data-repeater-item class="mt-repeater-item">
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Nama/Judul</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" name="judul">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Kertas</label>
                                            <div class="col-md-2">
                                                <input type="text" class="form-control" name="kertas">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Halaman Warna</label>
                                            <div class="col-md-1">
                                                <input type="text" class="form-control" name="hlm_warna" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="3">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Rangkap Warna</label>
                                            <div class="col-md-1">
                                                <input type="text" class="form-control" name="jml_warna" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="2">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Halaman Hitam</label>
                                            <div class="col-md-1">
                                                <input type="text" class="form-control" name="hlm_hitam_putih" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="3">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Rangkap Hitam</label>
                                            <div class="col-md-1">
                                                <input type="text" class="form-control" name="jml_hitam_putih" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="2">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-2 control-label">Keterangan</label>
                                            <div class="col-md-7">
                                                <input type="text" class="form-control" name="keterangan">
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <div class="col-md-5"></div>
                                            <div class="col-md-1">
                                                <a href="javascript:;" data-repeater-delete class="btn btn-danger mt-repeater-delete form-control">
                                                    <i class="fa fa-close"></i></a>
                                            </div>
                                            <div class="col-md-5"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div align="left"><a href="javascript:;" data-repeater-create class="btn btn-info mt-repeater-add">
                                <i class="fa fa-plus"></i> Add</a>
                            </div>
                            <div class="form-actions">
                                <hr>
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-9">
                                        <button type="submit" class="btn green">Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form> --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection