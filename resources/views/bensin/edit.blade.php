@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <li>{{ $message }}</li>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-file font-green"></i>
                            <span class="caption-subject font-green sbold uppercase">&nbsp; Form Bensin </span>
                        </div>
                    </div>
                    <div class="portlet-body">
                        @php 
                            $delete_dot_order_id = str_replace(".", "", $bensin->first()['order_id']); 
                            $replace_slash_order_id = str_replace("/", "-", $delete_dot_order_id); 
                            $encrypt_order_id = \Crypt::encryptString($replace_slash_order_id);
                        @endphp
                        <form action="{{ route('bensin.update', ['order_id' => $encrypt_order_id]) }}" class="form-horizontal" method="post">
                            @csrf @method('put')
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Waktu</label>
                                    <div class="col-md-3">
                                        <input class="form-control form-control-inline" size="16" type="date" name="waktu" value="{{ date('Y-d-m', strtotime($bensin->waktu)) }}" />
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Satker</label>
                                    <div class="col-md-2">
                                        <select class="form-control" name="satker_id">
                                            @foreach($satker as $n)
                                                @if($bensin->satker_id == $n->id)
                                                    <option value="{{ $n->id }}" selected>{{ ucwords($n->satker) }}</option>
                                                @else
                                                    <option value="{{ $n->id }}">{{ ucwords($n->satker) }}</option>
                                                @endif
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Jabatan</label>
                                    <div class="col-md-7">
                                        <input type="text" name="jabatan" class="form-control" value="{{ $bensin->jabatan }}">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-2 control-label">Total Penggunaan (Rp)</label>
                                    <div class="col-md-7">
                                        <input type="text" class="form-control" name="total_penggunaan" oninput="this.value = this.value.replace(/[^0-9]/g, '').replace(/(\..*)\./g, '$1');" maxlength="6" value="{{ $bensin->total_penggunaan }}">
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <hr>
                                <div class="row">
                                    <div class="col-md-offset-4 col-md-9">
                                        <button type="submit" class="btn green">Simpan</button>
                                        <a href="{{ route('bensin.show', ['order_id' => $encrypt_order_id]) }}" class="btn default">Batal</a>
                                    </div>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection