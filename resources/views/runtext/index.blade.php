@extends('template')
@section('content')
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content" style="padding-top: 0">
        <div class="row margin-bottom-10">
            <div class="col-md-12">
                <marquee bgcolor="#acb5c3" height="25">
                    <font size="3" color="white">
                        @foreach($runtext as $n)
                            ({{ date('d-F-Y', strtotime($n->created_at)) }}) - {{ $n->isian }} &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                            &emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;&emsp;
                        @endforeach
                    </font>
                </marquee>
            </div>
        </div>
        @if ($message = Session::get('success'))
            <div class="alert alert-success">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($message = Session::get('error'))
            <div class="alert alert-danger">
                <p>{{ $message }}</p>
            </div>
        @endif
        @if ($errors->any())
            <div class="alert alert-danger">
                <strong>Maaf!</strong> Terdapat kesalahan pada input Anda.<br><br>
                <ul>
                    @foreach ($errors->all() as $error)
                        <li>{{ $error }}</li>
                    @endforeach
                </ul>
            </div>
        @endif
        <div class="row">
            <div class="col-md-12">
                <div class="portlet light portlet-fit">
                    <div class="portlet-title">
                        <div class="caption">
                            <i class="fa fa-gear font-red-sunglo"></i>
                            <span class="caption-subject font-red-sunglo sbold uppercase">&nbsp; Kelola Teks Berjalan </span>
                        </div>
                        <div class="actions">
                            <a href="{{ route('runtext.create') }}" class="btn green"><i class="fa fa-plus"></i> Tambah Teks</a>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover table-scrollable" id="sample_3">
                            <thead>
                                <tr>
                                    <th width="5%">No</th>
                                    <th width="15%">Tanggal</th>
                                    <th width="60%">Teks</th>
                                    <th width="10%">Tampil</th>
                                    <th width="10%">Aksi</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($runtext as $n)
                                    @php
                                        $encrypt_id = \Crypt::encryptString($n->id);
                                    @endphp
                                    <tr>
                                        <td style="font-size:12px;" align="center">{{ $loop->iteration }}</td>
                                        <td style="font-size:12px;">{{ date('d F Y' ,strtotime($n->created_at)) }}</td>
                                        <td style="font-size:12px;">{{ $n->isian }}</td>
                                        <td style="font-size:12px;"><center>{{ ucwords($n->tampil) }}</center></td>
                                        <td><center>
                                            <form method="post" action="{{ route('runtext.delete', ['id' => $encrypt_id]) }}">
                                                @csrf @method('delete')
                                                <a href="{{ route('runtext.edit', ['id' => $encrypt_id]) }}" class="btn btn-xs blue"><i class="fa fa-pencil"></i></a>
                                                <button type="submit" class="btn btn-xs red" onclick="return confirm('Are you sure you want to delete this item?');"><i class="fa fa-trash"></i></button>
                                            </form></center>
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
